package net.tetrakoopa.bankoid.backend;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.webkit.WebView;

import net.tetrakoopa.bankoid.backend.ui.model.AccountWebViewModel;
import net.tetrakoopa.bankoid.backend.util.WebClientUtil;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;
import net.tetrakoopa.bankoid.backend.util.ClientViewJSExecutor;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

@SuppressLint("NewApi")
public abstract class BackendHandler<M extends AccountWebViewModel> implements ResourceProvider {

	protected final M accountModel;

	private WebView webView;
	private AssetManager assetManager;
	protected BackendPreferences preferences;
	private ClientViewJSExecutor clientViewJSExecutor;

	private final String resourceDirectory;
	private final String resourcesFolderName;

	protected final String name;

	protected final PreferencesKey preferencesKey;

	private MessageLogger logger;
	private LoginFailureDialogProvider loginFailureDialogProvider;
	private Context context;

	public enum UserAction {
		LOGIN, LOGOUT
	}

	protected BackendHandler(String name, M accountModel) {
		this.name = name;
		this.resourcesFolderName = name;
		this.accountModel = accountModel;
		this.resourceDirectory = Resources.getResourcesDirectory(resourcesFolderName);
		this.preferencesKey = new PreferencesKey();
	}

	public void setContextRelatedObjects(Context context, AssetManager assetManager, WebView webView, BackendPreferences preferences, ClientViewJSExecutor clientViewJSExecutor) {
		this.context = context;
		this.webView = webView;
		this.assetManager = assetManager;
		this.preferences = preferences;
		this.clientViewJSExecutor = clientViewJSExecutor;
	}

	@Override
	public String getResourceFolderName() {
		return resourcesFolderName;
	}

	public String getResourceDirectory() {
		return resourceDirectory;
	}

	protected InputStream getJavascriptResource(String relativePath) throws IOException {
		if (relativePath.startsWith("/")) relativePath = relativePath.substring(1);
		return assetManager.open(resourceDirectory+"/web-resources/javascript/"+relativePath);
	}

	protected void injectJS(InputStream inputStream) throws IOException {
		WebClientUtil.injectJS(webView, inputStream);
	}

	/**
	 * @param condition @Nullable a javascript condition : if evaluates to false then no injection is done
	 */
	protected void injectJSIf(InputStream inputStream, String condition) throws IOException {
		WebClientUtil.injectJSIf(webView, inputStream, condition);
	}

	protected  Object executeJSFunctionForThis(String thisObject, String functionName, Object... args) {
		return clientViewJSExecutor.executeForThis(thisObject, functionName, args);
	}
	protected  Object executeJSFunction(String functionName, Object... args) {
		return clientViewJSExecutor.execute(functionName, args);
	}

	protected boolean injectMainJSIfNeeded() {
		return injectMainJSIf("typeof AccountHelper == 'undefined'");
	}
	private boolean injectMainJSIf(String condition) {
		try (final InputStream backendMainJsInputStream = getJavascriptResource("main.js")) {
			injectJSIf(backendMainJsInputStream, condition);
		} catch (IOException ioex) {
			logger.log(MessageLogger.LogMessage.Severity.FATAL, BackendHandler.class, "Failed to open or read backend main javascript", ioex);
			return false;
		}
		try (final InputStream commonMainJsInputStream = assetManager.open("web-resources/javascript/main.js")) {
			injectJSIf(commonMainJsInputStream, condition);
		} catch (IOException ioex) {
			logger.log(MessageLogger.LogMessage.Severity.FATAL, BackendHandler.class, "Failed to open or read common main javascript", ioex);
			return false;
		}
		clientViewJSExecutor.execute("Net_TetraKoopa_AndroidOverWeb_Common._doInternalSetup");
		return true;
	}
	protected boolean injectMainJS() {
		return injectMainJSIf(null);
	}

	public abstract URL getEntry();

	/**
	 * @return <code>true</code> if handled
	 */
	protected abstract boolean onURLChanged(String string, URL url);

	public void urlChanged(String string) {
		final URL url;
		try {
			url = new URL(string);
		} catch (MalformedURLException e) {
			throw new InternalError("");
		}
		onURLChanged(string, url);
	}

	protected abstract void onUserAction(UserAction action);

	public void perform(UserAction action) {
		onUserAction(action);
	}


	protected void navigateTo(String url) {
		webView.loadUrl(url);
	}

	private static String removeStartingAndTrailingBackslashes(String string) {
		return string
			.replaceAll("/+$", "")
			.replaceAll("^/+", "");
	}

	protected MessageLogger getLogger() {
		return logger;
	}

	public void setLogger(MessageLogger logger) {
		this.logger = logger;
	}

	protected void showLoginFailure() {
		loginFailureDialogProvider.showDialog(context);
	}

	public void setLoginFailureDialogProvider(LoginFailureDialogProvider provider) {
		this.loginFailureDialogProvider = provider;
	}
}
