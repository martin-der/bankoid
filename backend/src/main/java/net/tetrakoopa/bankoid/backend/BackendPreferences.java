package net.tetrakoopa.bankoid.backend;

import java.util.Set;

public interface BackendPreferences {

	interface OnPreferenceChangeListener {
		void onPreferenceChanged(BackendPreferences preferences, String... keys);
	}

	void registerOnPreferenceChangeListener(OnPreferenceChangeListener listener);

	void unregisterOnPreferenceChangeListener(OnPreferenceChangeListener listener);


	boolean getBoolean(String key);

	float getFloat(String key);

	int getInt(String key);

	long getLong(String key);

	String getString(String key);

	Set<String> getStringSet(String key);
}
