package net.tetrakoopa.bankoid.backend.util;

import android.util.Base64;
import android.webkit.WebView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class WebClientUtil {

	/**
	 * @param condition @Nullable a javascript condition : if evaluates to false then no injection is done
	 */
	public static void injectJSIf(WebView webView, InputStream inputStream, String condition) throws IOException {
		final byte[] buffer = readStream(inputStream);
		final String encoded = Base64.encodeToString(buffer, Base64.NO_WRAP);
		webView.loadUrl("javascript:(function() {" +
				( condition != null ? "if ("+condition+") {" : "" ) +
				"var parent = document.getElementsByTagName('head').item(0);" +
				"var script = document.createElement('script');" +
				"script.type = 'text/javascript';" +
				"script.innerHTML = window.atob('" + encoded + "');" +
				"parent.appendChild(script)" +
				( condition != null ? "}" : "" ) +
				"})()");
	}

	public static void injectJS(WebView webView, InputStream inputStream) throws IOException {
		injectJSIf(webView, inputStream, null);
	}

	private static byte[] readStream(InputStream stream) throws IOException {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int nRead;
		byte[] data = new byte[1024];
		while ((nRead = stream.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
		}

		buffer.flush();
		return buffer.toByteArray();
	}

}
