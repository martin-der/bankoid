package net.tetrakoopa.bankoid.backend;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public abstract class BackendService extends Service {

	private final IWebBackend.Stub binder;

	protected BackendService(IWebBackend.Stub webBackend) {
		binder = webBackend;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}
}