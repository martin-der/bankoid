package net.tetrakoopa.bankoid.backend;

public class PreferencesKey {

	public static String getForBackend(String backend, String key) {
		return "backend."+backend+"."+key;
	}

	public final class CleanUI {
		public static final String HIDE_SAVE_ACCOUNT_IDENTIFIER = "hide-button-save-identifier";
	}

	public final CleanUI cleanUI = new CleanUI();

	public PreferencesKey() {
	}
}
