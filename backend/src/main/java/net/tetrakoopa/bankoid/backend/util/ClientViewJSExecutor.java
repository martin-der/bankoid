package net.tetrakoopa.bankoid.backend.util;

public interface ClientViewJSExecutor {

	/**
	 * @param thisObject @Nullable object used as <code>this</code> when using function this way : <code>function.call(thosObject, parameters...)</code>
	 */
	Object executeForThis(String thisObject, String functionName, Object... parameters);

	Object execute(String functionName, Object... args);
}
