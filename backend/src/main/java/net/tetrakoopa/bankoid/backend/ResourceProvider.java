package net.tetrakoopa.bankoid.backend;

public interface ResourceProvider {

	String getResourceFolderName();

	String getResourceDirectory();
}
