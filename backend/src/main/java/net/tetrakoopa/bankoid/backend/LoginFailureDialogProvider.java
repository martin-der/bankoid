package net.tetrakoopa.bankoid.backend;

import android.content.Context;

public interface LoginFailureDialogProvider {
	void showDialog(Context context);
}
