package net.tetrakoopa.bankoid.backend.ui.model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public abstract class AccountWebViewModel extends ViewModel {

	private final MutableLiveData<String> url;
	private final MutableLiveData<Boolean> logged;

	public AccountWebViewModel() {
		url = new MutableLiveData<>();
		logged = new MutableLiveData<>();
	}

	public MutableLiveData<String> getUrl() {
		return url;
	}

	public MutableLiveData<Boolean> getLogged() {
		return logged;
	}

}