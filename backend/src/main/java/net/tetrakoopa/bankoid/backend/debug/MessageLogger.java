package net.tetrakoopa.bankoid.backend.debug;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public interface MessageLogger {

	long log(LogMessage.Severity severity, LogMessage.Type type, LogMessage.Location location, String content, String details, Exception cause);
	long log(LogMessage.Severity severity, LogMessage.Type type, LogMessage.Location location, String content, String details);
	long log(LogMessage.Severity severity, LogMessage.Type type, String location, String content, String details, Exception cause);
	long log(LogMessage.Severity severity, LogMessage.Type type, String location, String content, String details);

	long log(LogMessage.Severity severity, LogMessage.Type type, int location, String content, String details, Exception cause);
	long log(LogMessage.Severity severity, LogMessage.Type type, int location, String content, String details);

	long log(LogMessage.Severity severity, Class<?> fragmentOrActivity, String content, String details, Exception cause);
	long log(LogMessage.Severity severity, Class<?> fragmentOrActivity, String content, String details);
	long log(LogMessage.Severity severity, Class<?> fragmentOrActivity, String content, Exception cause);
	long log(LogMessage.Severity severity, Class<?> fragmentOrActivity, String content);


	class LogMessage {

		public enum Severity { TRACE, DEBUG, INFO, WARNING, ERROR, FATAL }
		public enum Type { ACTIVITY, WEB_JS }

		public static class Location {

			public final int index;
			public final String where;

			public Location(String where, int index) {
				this.where = where;
				this.index = index;
			}
			public Location(String where) {
				this.where = where;
				this.index = -1;
			}
			public Location(int index) {
				this.where = null;
				this.index = index;
			}
		}

		private static final AtomicLong sequenceId = new AtomicLong();

		public final long id;
		public final Date date;
		public final Severity severity;
		public final Type type;
		public final Location location;
		public final String content;
		public final Exception cause;
		public final String details;

		private final Map<String, String> extras = new HashMap<>();

		public LogMessage(Severity severity, Type type, Location location, String content, String details, Exception cause) {
			this.id = sequenceId.getAndIncrement();
			this.date = new Date();
			this.severity = severity;
			this.type = type;
			this.location = location;
			this.content = content;
			this.cause = cause;
			this.details = details;
		}
		public LogMessage(Severity severity, Type type, Location location, String content, String details) {
			this(severity, type, location, content, details, null);
		}

		public LogMessage(Severity severity, Type type, String location, String content, String details, Exception cause) {
			this(severity, type, new Location(location), content, details, cause);
		}
		public LogMessage(Severity severity, Type type, String location, String content, String details) {
			this(severity, type, new Location(location), content, details, null);
		}

		public LogMessage(Severity severity, Type type, int location, String content, String details, Exception cause) {
			this(severity, type, new Location(location), content, details, cause);
		}
		public LogMessage(Severity severity, Type type, int location, String content, String details) {
			this(severity, type, location, content, details, null);
		}

		public LogMessage(Severity severity, Class<?> fragmentOrActivity, String content, String details, Exception cause) {
			this(severity, Type.ACTIVITY, bestLocation(fragmentOrActivity, cause), content, details, cause);
		}
		public LogMessage(Severity severity, Class<?> fragmentOrActivity, String content, String details) {
			this(severity, fragmentOrActivity, content, details, null);
		}
		public LogMessage(Severity severity, Class<?> fragmentOrActivity, String content, Exception cause) {
			this(severity, fragmentOrActivity, content, cause == null ? "" : cause.getMessage(), cause);
		}
		public LogMessage(Severity severity, Class<?> fragmentOrActivity, String content) {
			this(severity, fragmentOrActivity, content, "");
		}

		public LogMessage withExtra(String key, String content) {
			extras.put(key, content);
			return this;
		}

		public Map<String, String> extras() {
			return Collections.unmodifiableMap(extras);
		}

		@Override
		public String toString() {
			return content;
		}

		public void setUpperClasses(Class<?>... classes) {
			final Set<String> names = new HashSet<>();
			for (Class<?> clazz : classes) {
				names.add(clazz.getName());
			}
		}

		private static String[] UPPER_CLASSES_NAMES = new String[] { "net.tetrakoopa.bankoid.debug.FullLogger", "net.tetrakoopa.bankoid.Oops" };
		private static Location bestLocation(Class<?> clazz, Exception exception) {
			return bestLocation(clazz, exception, UPPER_CLASSES_NAMES);
		}
		private static Location bestLocation(Class<?> clazz, Exception exception, String[] upperClassesNames) {
			if (exception != null) {

				final StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
				int index = 0;
				boolean reachedMessageLogClasses = false;
				while (index < stackTraceElements.length) {
					final StackTraceElement stackTraceElement = stackTraceElements[index];
					final String className = stackTraceElement.getClassName();
					if (className != null) {
						boolean isInsideMessageLogClasses = false;
						for (final String upperClassName : upperClassesNames) {
							if (className.equals(upperClassName) || className.startsWith(upperClassName + "$"))
								isInsideMessageLogClasses = true;
						}
						if (!reachedMessageLogClasses) {
							if (isInsideMessageLogClasses) {
								reachedMessageLogClasses = true;
							}

						} else {
							if (!isInsideMessageLogClasses) {
								StackTraceElement callingElement = stackTraceElements[index];
								return new Location(callingElement.getClassName() + "." + callingElement.getMethodName(), callingElement.getLineNumber());
							}
						}
					}
					index ++;
				}
			}
			return new Location(clazz.getName());
		}
	}

}
