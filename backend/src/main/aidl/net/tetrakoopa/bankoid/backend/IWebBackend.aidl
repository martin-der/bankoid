package net.tetrakoopa.bankoid.backend;

import net.tetrakoopa.bankoid.backend.Info;

interface IWebBackend {

	String name();

	Info info();

}
