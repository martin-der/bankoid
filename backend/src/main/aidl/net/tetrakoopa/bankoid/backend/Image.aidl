package net.tetrakoopa.bankoid.backend;

// Declare any non-default types here with import statements

interface Image {

	String type();

	byte[] content();
}
