package net.tetrakoopa.bankoid.backend;

import net.tetrakoopa.bankoid.backend.Image;

interface Info {

	String name();

	String identifier();

	String description();

	Image logo();
}
