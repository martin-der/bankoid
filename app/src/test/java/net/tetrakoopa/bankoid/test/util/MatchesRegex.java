package net.tetrakoopa.bankoid.test.util;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

import java.lang.reflect.Array;
import java.util.regex.Pattern;

public class MatchesRegex extends BaseMatcher<String> {

	private final Pattern regex;

	public MatchesRegex(String regex) {
		this.regex = Pattern.compile(regex);
	}

	@Override
	public boolean matches(Object actualValue) {
		return match(actualValue, regex);
	}

	@Override
	public void describeTo(Description description) {
		description.appendValue(regex);
	}

	private static boolean match(Object actual, Pattern regex) {
		if (actual == null) {
			return false;
		}

		if (actual.getClass().isArray()) {
			return arraysMatch(actual, regex);
		}

		return regex.matcher(actual.toString()).matches();
	}

	private static boolean arraysMatch(Object actualArray, Pattern regex) {
		return areArrayElementsEqual(actualArray, regex);
	}

	private static boolean areArrayElementsEqual(Object actualArray, Pattern regex) {
		for (int i = 0; i < Array.getLength(actualArray); i++) {
			if (!match(Array.get(actualArray, i), regex)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Creates a matcher that matches when the examined object is logically equal to the specified
	 * <code>operand</code>, as determined by calling the {@link java.lang.Object#equals} method on
	 * the <b>examined</b> object.
	 *
	 * <p>If the specified operand is <code>null</code> then the created matcher will only match if
	 * the examined object's <code>equals</code> method returns <code>true</code> when passed a
	 * <code>null</code> (which would be a violation of the <code>equals</code> contract), unless the
	 * examined object itself is <code>null</code>, in which case the matcher will return a positive
	 * match.</p>
	 *
	 * <p>The created matcher provides a special behaviour when examining <code>Array</code>s, whereby
	 * it will match if both the operand and the examined object are arrays of the same length and
	 * contain items that are equal to each other (according to the above rules) <b>in the same
	 * indexes</b>.</p>
	 * <p/>
	 * For example:
	 * <pre>
	 * assertThat("foo", equalTo("foo"));
	 * assertThat(new String[] {"foo", "bar"}, equalTo(new String[] {"foo", "bar"}));
	 * </pre>
	 *
	 */
	@Factory
	public static Matcher match(String operand) {
		return new MatchesRegex(operand);
	}
}
