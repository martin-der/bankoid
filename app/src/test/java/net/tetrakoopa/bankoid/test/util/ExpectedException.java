package net.tetrakoopa.bankoid.test.util;

import org.hamcrest.Matcher;
import org.hamcrest.StringDescription;
import org.hamcrest.core.IsEqual;
import org.hamcrest.core.StringContains;
import org.hamcrest.core.StringEndsWith;
import org.hamcrest.core.StringStartsWith;
import org.junit.rules.TestRule;
import org.junit.runners.model.Statement;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.junit.internal.matchers.ThrowableCauseMatcher.hasCause;
import static org.junit.internal.matchers.ThrowableMessageMatcher.hasMessage;
import static org.junit.matchers.JUnitMatchers.isThrowable;

public class ExpectedException implements TestRule {

	private final static String NO_MESSAGE = "net.tetrakoopa.bankoid.test.util.ExpectedException.NO_MESSAGE";

	private static class MatcherBuilder {

		private final List<Matcher<?>> matchers = new ArrayList<Matcher<?>>();

		void add(Matcher<?> matcher) {
			matchers.add(matcher);
		}

		boolean expectsThrowable() {
			return !matchers.isEmpty();
		}

		Matcher<Throwable> build() {
			return isThrowable(allOfTheMatchers());
		}

		private Matcher<Throwable> allOfTheMatchers() {
			if (matchers.size() == 1) {
				return cast(matchers.get(0));
			}
			return allOf(castedMatchers());
		}

		@SuppressWarnings({"unchecked", "rawtypes"})
		private List<Matcher<? super Throwable>> castedMatchers() {
			return new ArrayList<Matcher<? super Throwable>>((List) matchers);
		}

		@SuppressWarnings("unchecked")
		private Matcher<Throwable> cast(Matcher<?> singleMatcher) {
			return (Matcher<Throwable>) singleMatcher;
		}
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ElementType.METHOD})
	public @interface Exception {

		@interface Message {
			enum MatchType { EQUAL, BEGIN, END, CONTAIN, REGEX }
			String value();
			MatchType matchType() default MatchType.EQUAL;
		}

		Class<? extends Throwable> expected();
		boolean noSubClass() default false;

		Message message() default @Message(NO_MESSAGE);
	}

	/**
	 * Returns a {@linkplain TestRule rule} that expects no exception to
	 * be thrown (identical to behavior without this rule).
	 */
	public static ExpectedException none() {
		return new ExpectedException();
	}

	private final MatcherBuilder matcherBuilder = new MatcherBuilder();

	private String missingExceptionMessage= "Expected test to throw %s";

	private ExpectedException() {
	}

	/**
	 * Specifies the failure message for tests that are expected to throw 
	 * an exception but do not throw any. You can use a {@code %s} placeholder for
	 * the description of the expected exception. E.g. "Test doesn't throw %s."
	 * will fail with the error message
	 * "Test doesn't throw an instance of foo.".
	 *
	 * @param message exception detail message
	 * @return the rule itself
	 */
	public ExpectedException reportMissingExceptionWithMessage(String message) {
		missingExceptionMessage = message;
		return this;
	}

	public Statement apply(Statement base,
						   org.junit.runner.Description description) {
		final Exception exception = description.getAnnotation(Exception.class);
		if (exception != null) {
			this.expect(exception.expected());
			final Exception.Message message = exception.message();
			if (!message.value().equals(NO_MESSAGE))
				switch (message.matchType()) {
					case BEGIN:
						this.expectMessage(StringStartsWith.startsWith(message.value()));
					case END:
						this.expectMessage(StringEndsWith.endsWith(message.value()));
					case CONTAIN:
						this.expectMessage(StringContains.containsString(message.value()));
					case REGEX:
						this.expectMessage(MatchesRegex.match(message.value()));
					default:
						this.expectMessage(IsEqual.equalTo(message.value()));
				}
		}
		return new RuleStatement(base);
	}

	/**
	 * Verify that your code throws an exception that is matched by
	 * a Hamcrest matcher.
	 * <pre> &#064;Test
	 * public void throwsExceptionThatCompliesWithMatcher() {
	 *     NullPointerException e = new NullPointerException();
	 *     thrown.expect(is(e));
	 *     throw e;
	 * }</pre>
	 */
	public void expect(Matcher<?> matcher) {
		matcherBuilder.add(matcher);
	}

	/**
	 * Verify that your code throws an exception that is an
	 * instance of specific {@code type}.
	 * <pre> &#064;Test
	 * public void throwsExceptionWithSpecificType() {
	 *     thrown.expect(NullPointerException.class);
	 *     throw new NullPointerException();
	 * }</pre>
	 */
	public void expect(Class<? extends Throwable> type) {
		expect(instanceOf(type));
	}

	/**
	 * Verify that your code throws an exception whose message contains
	 * a specific text.
	 * <pre> &#064;Test
	 * public void throwsExceptionWhoseMessageContainsSpecificText() {
	 *     thrown.expectMessage(&quot;happened&quot;);
	 *     throw new NullPointerException(&quot;What happened?&quot;);
	 * }</pre>
	 */
	public void expectMessage(String substring) {
		expectMessage(containsString(substring));
	}

	/**
	 * Verify that your code throws an exception whose message is matched 
	 * by a Hamcrest matcher.
	 * <pre> &#064;Test
	 * public void throwsExceptionWhoseMessageCompliesWithMatcher() {
	 *     thrown.expectMessage(startsWith(&quot;What&quot;));
	 *     throw new NullPointerException(&quot;What happened?&quot;);
	 * }</pre>
	 */
	public void expectMessage(Matcher<String> matcher) {
		expect(hasMessage(matcher));
	}

	/**
	 * Verify that your code throws an exception whose cause is matched by 
	 * a Hamcrest matcher.
	 * <pre> &#064;Test
	 * public void throwsExceptionWhoseCauseCompliesWithMatcher() {
	 *     NullPointerException expectedCause = new NullPointerException();
	 *     thrown.expectCause(is(expectedCause));
	 *     throw new IllegalArgumentException(&quot;What happened?&quot;, cause);
	 * }</pre>
	 */
	public void expectCause(Matcher<? extends Throwable> expectedCause) {
		expect(hasCause(expectedCause));
	}

	private class RuleStatement extends Statement {
		private final Statement next;

		public RuleStatement(Statement base) {
			next = base;
		}

		@Override
		public void evaluate() throws Throwable {
			try {
				next.evaluate();
			} catch (Throwable e) {
				handleException(e);
				return;
			}
			if (isAnyExceptionExpected()) {
				failDueToMissingException();
			}
		}
	}

	private void handleException(Throwable e) throws Throwable {
		if (isAnyExceptionExpected()) {
			assertThat(e, matcherBuilder.build());
		} else {
			throw e;
		}
	}

	private boolean isAnyExceptionExpected() {
		return matcherBuilder.expectsThrowable();
	}

	private void failDueToMissingException() throws AssertionError {
		fail(missingExceptionMessage());
	}

	private String missingExceptionMessage() {
		String expectation= StringDescription.toString(matcherBuilder.build());
		return format(missingExceptionMessage, expectation);
	}
}
