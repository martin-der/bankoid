package net.tetrakoopa.bankoid.util.xml;

import net.tetrakoopa.bankoid.model.bank.Config;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;

import org.junit.Test;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ConfigParserTest extends AbstractParserTest {

	@Test
	public void readConfigXml() throws IOException, XmlPullParserException, ParseException {
		InputStream stream = getClass().getResourceAsStream("config.xml");
		final ConfigParser parser = new ConfigParser(null);
		parser.setParser(factory.newPullParser());

		Config config = parser.parse(stream);

		assertEquals(config.getAccounts().size(), 1);
		assertNotNull(config.getClazz().getHandler());
		assertNotNull(config.getClazz().getModel());
	}
}
