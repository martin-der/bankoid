package net.tetrakoopa.bankoid.util.xml;

import org.junit.BeforeClass;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public abstract class AbstractParserTest {

	protected static XmlPullParserFactory factory;

	@BeforeClass
	public static void initAll() throws XmlPullParserException {
		factory = XmlPullParserFactory.newInstance();
	}
}
