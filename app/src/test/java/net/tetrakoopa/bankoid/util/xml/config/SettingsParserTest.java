package net.tetrakoopa.bankoid.util.xml.config;

import net.tetrakoopa.bankoid.model.bank.Settings;
import net.tetrakoopa.bankoid.model.common.Primitive;
import net.tetrakoopa.bankoid.util.xml.AbstractParserTest;
import net.tetrakoopa.bankoid.test.util.ExpectedException;
import net.tetrakoopa.bankoid.test.util.ExpectedException.Exception;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.InvalidPrimitiveTypeParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.AttributeMissingParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.InvalidAttributeValueParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;

import org.junit.Rule;
import org.junit.Test;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


public class SettingsParserTest extends AbstractParserTest {

	@Rule
	public ExpectedException exceptionAwareRule = ExpectedException.none();

	private Settings readReferenceSettings() throws XmlPullParserException, IOException, ParseException {
		final InputStream stream = getClass().getResourceAsStream("settings/"+"settings_referenced.xml");
		final SettingsParser.Standalone parser = new SettingsParser.Standalone();
		parser.setParser(factory.newPullParser());

		return parser.parse(stream);
	}
	private Settings readSettingsFrom(String filename, boolean withReference) throws IOException, ParseException, XmlPullParserException {
		final InputStream stream = getClass().getResourceAsStream("settings/"+filename);
		final SettingsParser.Standalone parser = withReference ? new SettingsParser.Standalone(readReferenceSettings()) : new SettingsParser.Standalone();
		parser.setParser(factory.newPullParser());

		return parser.parse(stream);
	}
	private Settings readSettingsFromWithReference(String filename) throws IOException, ParseException, XmlPullParserException {
		return readSettingsFrom(filename, true);
	}
	private Settings readSettingsFrom(String filename) throws IOException, ParseException, XmlPullParserException {
		return readSettingsFrom(filename, false);
	}

	@Test
	public void read_settings_xml() throws IOException, XmlPullParserException, ParseException {
		final Settings settings = readSettingsFrom("settings.xml");

		assertEquals(settings.getEntries().size(), 5);

		Settings.Setting entryAbcDefGhi = settings.getEntries().get("abc-def-ghi");
		assertNotNull(entryAbcDefGhi);
		assertNull(entryAbcDefGhi.dependency);
		assertEquals(entryAbcDefGhi.primitive.type, Primitive.Type.BOOLEAN);
		assertEquals(entryAbcDefGhi.defaultz, Boolean.TRUE);
		assertNotNull(entryAbcDefGhi.text);
		assertEquals(entryAbcDefGhi.text.title, "@string/my_title");
		assertEquals(entryAbcDefGhi.text.summaryOn, "@string/my_summary_on");
		assertEquals(entryAbcDefGhi.text.summaryOff, "@string/my_summary_off");

		Settings.Setting entryFoobar = settings.getEntries().get("foo--bar");
		assertNotNull(entryFoobar);
		assertNull(entryFoobar.dependency);
		assertEquals(entryFoobar.primitive.type, Primitive.Type.STRING);
		assertEquals(entryFoobar.defaultz, "maybe");
		assertNull(entryFoobar.text);

		Settings.Setting entryQwerty = settings.getEntries().get("qwerty");
		assertNotNull(entryQwerty);
		assertNull(entryQwerty.dependency);
		assertEquals(entryQwerty.primitive.type, Primitive.Type.INTEGER);
		assertEquals(entryQwerty.defaultz, 112);
		assertNotNull(entryQwerty.text);
		assertEquals(entryQwerty.text.title, "How many keys do you want?");
		assertEquals(entryQwerty.text.summary, "Let's pretend this is a virtual keyboard");

		Settings.Setting entryRepaintAll = settings.getEntries().get("repaint-all");
		assertNotNull(entryRepaintAll);
		assertNull(entryRepaintAll.dependency);
		assertEquals(entryRepaintAll.primitive.type, Primitive.Type.BOOLEAN);
		assertEquals(entryRepaintAll.defaultz, false);
		assertNotNull(entryRepaintAll.text);
		assertEquals(entryRepaintAll.text.title, "Repaint all");
		assertEquals(entryRepaintAll.text.summary, "Repaint all, are you sure?");

		Settings.Setting entryNewColor = settings.getEntries().get("new-color");
		assertNotNull(entryNewColor);
		assertEquals(entryNewColor.dependency, "repaint-all");
		assertEquals(entryNewColor.primitive.type, Primitive.Type.STRING);
		assertEquals(entryNewColor.defaultz, "green");
		assertNotNull(entryNewColor.text);
		assertEquals(entryNewColor.text.title, "Pick a color");
		assertEquals(entryNewColor.text.summary, "It will be used to repaint all.");
	}

	@Test
	@Exception(
			expected = InvalidPrimitiveTypeParseException.class,
			message = @Exception.Message("l.3:39: 'nonono' is not a valid Primitive type"))
	public void read_settings_xml_bad_primitive_type() throws IOException, XmlPullParserException, ParseException {
		readSettingsFrom("settings_bad_primitive_type.xml");
	}

	@Test
	@Exception(
			expected = AttributeMissingParseException.class,
			message = @Exception.Message("l.3:24: Expected an attribute 'default', found none"))
	public void read_settings_xml_bad_missing_default_value() throws IOException, XmlPullParserException, ParseException {
		readSettingsFrom("settings_bad_missing_default_value.xml");
	}

	@Test
	@Exception(
			expected = InvalidAttributeValueParseException.class,
			message = @Exception.Message("l.4:59: Illegal value 'abc-ghi' for attribute 'dependency': No previously declared setting named 'abc-ghi'"))
	public void read_settings_xml_unknown_dependency() throws IOException, XmlPullParserException, ParseException {
		readSettingsFrom("settings_bad_unknown_dependency.xml");
	}


	@Test
	public void read_settings_xml_with_reference() throws IOException, XmlPullParserException, ParseException {
		final Settings settings = readSettingsFromWithReference("settings_with_reference.xml");

		assertEquals(settings.getEntries().size(), 2);

		Settings.Setting entryLabelingAll = settings.getEntries().get("labeling-all");
		assertNotNull(entryLabelingAll);
		assertNull(entryLabelingAll.dependency);
		assertEquals(entryLabelingAll.primitive.type, Primitive.Type.BOOLEAN);
		assertEquals(entryLabelingAll.defaultz, false);
		assertNotNull(entryLabelingAll.text);
		assertEquals(entryLabelingAll.text.title, "Label everything");
		assertEquals(entryLabelingAll.text.summary, "Label everything, are you sure?");

		Settings.Setting entryLabelingTool = settings.getEntries().get("labeling-tool");
		assertNotNull(entryLabelingTool);
		assertNull(entryLabelingTool.dependency);
		assertEquals(entryLabelingTool.primitive.type, Primitive.Type.STRING);
		assertEquals(entryLabelingTool.defaultz, "pencil");
		assertNotNull(entryLabelingTool.text);
		assertEquals(entryLabelingTool.text.title, "Pick a tool");
		assertEquals(entryLabelingTool.text.summary, "It will be used to label everything.");
	}

	@Test
	public void read_settings_xml_with_named_reference_and_reference_default_override() throws IOException, XmlPullParserException, ParseException {
		final Settings settings = readSettingsFromWithReference("settings_with_named_reference_and_reference_default_override.xml");

		assertEquals(settings.getEntries().size(), 2);

		assertEquals(settings.getEntries().size(), 2);

		Settings.Setting entryTodo = settings.getEntries().get("todo");
		assertNotNull(entryTodo);
		assertNull(entryTodo.dependency);
		assertEquals(entryTodo.primitive.type, Primitive.Type.BOOLEAN);
		assertEquals(entryTodo.defaultz, true);
		assertNotNull(entryTodo.text);
		assertEquals(entryTodo.text.title, "Label everything");
		assertEquals(entryTodo.text.summary, "Label everything, are you sure?");

		Settings.Setting entryTodoHow = settings.getEntries().get("todo-how");
		assertNotNull(entryTodoHow);
		assertNull(entryTodoHow.dependency);
		assertEquals(entryTodoHow.primitive.type, Primitive.Type.STRING);
		assertEquals(entryTodoHow.defaultz, "chalk");
		assertNotNull(entryTodoHow.text);
		assertEquals(entryTodoHow.text.title, "Pick a tool");
		assertEquals(entryTodoHow.text.summary, "It will be used to label everything.");
	}

	@Test
	public void read_settings_xml_with_reference_and_reference_default_override() throws IOException, XmlPullParserException, ParseException {
		final Settings settings = readSettingsFromWithReference("settings_with_reference_and_reference_default_override.xml");

		assertEquals(settings.getEntries().size(), 2);

		assertEquals(settings.getEntries().size(), 2);

		Settings.Setting entryLabelingAll = settings.getEntries().get("labeling-all");
		assertNotNull(entryLabelingAll);
		assertNull(entryLabelingAll.dependency);
		assertEquals(entryLabelingAll.primitive.type, Primitive.Type.BOOLEAN);
		assertEquals(entryLabelingAll.defaultz, true);
		assertNotNull(entryLabelingAll.text);
		assertEquals(entryLabelingAll.text.title, "Label everything");
		assertEquals(entryLabelingAll.text.summary, "Label everything, are you sure?");

		Settings.Setting entryLabelingTool = settings.getEntries().get("labeling-tool");
		assertNotNull(entryLabelingTool);
		assertNull(entryLabelingTool.dependency);
		assertEquals(entryLabelingTool.primitive.type, Primitive.Type.STRING);
		assertEquals(entryLabelingTool.defaultz, "chalk");
		assertNotNull(entryLabelingTool.text);
		assertEquals(entryLabelingTool.text.title, "Pick a tool");
		assertEquals(entryLabelingTool.text.summary, "It will be used to label everything.");
	}

	@Test
	public void read_settings_xml_with_reference_and_reference_content_override() throws IOException, XmlPullParserException, ParseException {
		final Settings settings = readSettingsFromWithReference("settings_with_named_reference_and_reference_content_override.xml");

		assertEquals(settings.getEntries().size(), 2);

		Settings.Setting entryTodo = settings.getEntries().get("todo");
		assertNotNull(entryTodo);
		assertNull(entryTodo.dependency);
		assertEquals(entryTodo.primitive.type, Primitive.Type.BOOLEAN);
		assertEquals(entryTodo.defaultz, false);
		assertNotNull(entryTodo.text);
		assertEquals(entryTodo.text.title, "Label everything");
		assertEquals(entryTodo.text.summary, "Label everything, are you sure?");

		Settings.Setting entryTodoHow = settings.getEntries().get("todo-how");
		assertNotNull(entryTodoHow);
		assertNull(entryTodoHow.dependency);
		assertEquals(entryTodoHow.primitive.type, Primitive.Type.STRING);
		assertEquals(entryTodoHow.defaultz, "pencil");
		assertNotNull(entryTodoHow.text);
		assertEquals(entryTodoHow.text.title, "Pick a tool");
		assertEquals(entryTodoHow.text.summary, "I won't do it any way... please stop writing summaries");
	}

	@Test
	public void read_settings_xml_with_named_reference() throws IOException, XmlPullParserException, ParseException {
		final Settings settings = readSettingsFromWithReference("settings_with_named_reference.xml");

		assertEquals(settings.getEntries().size(), 2);

		Settings.Setting entryTodo = settings.getEntries().get("todo");
		assertNotNull(entryTodo);
		assertNull(entryTodo.dependency);
		assertEquals(entryTodo.primitive.type, Primitive.Type.BOOLEAN);
		assertEquals(entryTodo.defaultz, false);
		assertNotNull(entryTodo.text);
		assertEquals(entryTodo.text.title, "Label everything");
		assertEquals(entryTodo.text.summary, "Label everything, are you sure?");

		Settings.Setting entryTodoHow = settings.getEntries().get("todo-how");
		assertNotNull(entryTodoHow);
		assertNull(entryTodoHow.dependency);
		assertEquals(entryTodoHow.primitive.type, Primitive.Type.STRING);
		assertEquals(entryTodoHow.defaultz, "pencil");
		assertNotNull(entryTodoHow.text);
		assertEquals(entryTodoHow.text.title, "Pick a tool");
		assertEquals(entryTodoHow.text.summary, "It will be used to label everything.");
	}

	@Test
	public void read_settings_xml_with_named_reference_and_reference_content_override() throws IOException, XmlPullParserException, ParseException {
		final Settings settings = readSettingsFromWithReference("settings_with_named_reference_and_reference_content_override.xml");

		assertEquals(settings.getEntries().size(), 2);

		Settings.Setting entryTodo = settings.getEntries().get("todo");
		assertNotNull(entryTodo);
		assertNull(entryTodo.dependency);
		assertEquals(entryTodo.primitive.type, Primitive.Type.BOOLEAN);
		assertEquals(entryTodo.defaultz, false);
		assertNotNull(entryTodo.text);
		assertEquals(entryTodo.text.title, "Label everything");
		assertEquals(entryTodo.text.summary, "Label everything, are you sure?");

		Settings.Setting entryTodoHow = settings.getEntries().get("todo-how");
		assertNotNull(entryTodoHow);
		assertNull(entryTodoHow.dependency);
		assertEquals(entryTodoHow.primitive.type, Primitive.Type.STRING);
		assertEquals(entryTodoHow.defaultz, "pencil");
		assertNotNull(entryTodoHow.text);
		assertEquals(entryTodoHow.text.title, "Pick a tool");
		assertEquals(entryTodoHow.text.summary, "I won't do it any way... please stop writing summaries");
	}

	@Test
	@Exception(
			expected = InvalidAttributeValueParseException.class,
			message = @Exception.Message("l.4:23: Illegal value 'labeling-tool-zzz' for attribute 'reference': Unknown reference 'labeling-tool-zzz'"))
	public void read_settings_xml_unknown_reference() throws IOException, XmlPullParserException, ParseException {
		readSettingsFromWithReference("settings_bad_unknown_reference.xml");
	}

	@Test
	@Exception(
			expected = InvalidAttributeValueParseException.class,
			message = @Exception.Message("l.4:49: Illegal value 'labeling-tool-zzz' for attribute 'reference': Unknown reference 'labeling-tool-zzz'"))
	public void read_settings_xml_unknown_named_reference() throws IOException, XmlPullParserException, ParseException {
		readSettingsFromWithReference("settings_bad_unknown_named_reference.xml");
	}

}