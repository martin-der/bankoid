package net.tetrakoopa.bankoid.util.xml;

import net.tetrakoopa.bankoid.model.bank.About;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;

import org.junit.Test;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class AboutParserTest extends AbstractParserTest {

	@Test
	public void readAboutXml() throws IOException, XmlPullParserException, ParseException {
		InputStream stream = getClass().getResourceAsStream("about.xml");
		final AboutParser parser = new AboutParser();
		parser.setParser(factory.newPullParser());


		About about = parser.parse(stream);

		assertEquals(about.name, "Some Bank Cartel");
		assertEquals(about.description, "All your money are belong to us.");
	}
}
