package net.tetrakoopa.bankoid;

public class Help {

	public static final class Key {
		private Key(){};

		public static final String ABOUT_CREATE_ACCOUNT_SHOWN = "tutorial.about.create_account";
	}
}
