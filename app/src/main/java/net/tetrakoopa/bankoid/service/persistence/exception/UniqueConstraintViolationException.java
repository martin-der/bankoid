package net.tetrakoopa.bankoid.service.persistence.exception;

import android.database.SQLException;

public class UniqueConstraintViolationException extends PersistenceException {

	public UniqueConstraintViolationException(Class<?> entity, SQLException ex) {
		super("Duplicate field(s) in "+entity.getName()+": "+ex.getMessage(), ex);
	}
}
