package net.tetrakoopa.bankoid.service.persistence.exception;

public class PersistenceException extends Exception {

	public PersistenceException(String message) {
		super(message);
	}

	public PersistenceException(String message, Throwable cause) {
		super(message, cause);
	}

	public PersistenceException(Throwable cause) {
		super(cause);
	}

	protected static String selectionAndSelectionArgsToString(String selection, String[] selectionArgs) {
		final StringBuilder builder = new StringBuilder();
		builder.append('\'');
		if (selection != null)
			builder.append(selection);
		builder.append("' ");
		if (selectionArgs != null) {
			boolean first = true;
			builder.append("with [");
			for (String arg : selectionArgs) {
				if (first) first = false;
				else builder.append(",");
				builder.append('\'').append(arg).append('\'');
			}
			builder.append(']');
		}

		return builder.toString();
	}
}
