package net.tetrakoopa.bankoid.service.persistence.schema;

public class AccountDBSchema {

	public static final String SQL_CREATE_ENTRIES =
			"CREATE TABLE " + AccountDBContract.Entry.TABLE_NAME + " (" +
					AccountDBContract.Entry._ID + " INTEGER PRIMARY KEY," +
					AccountDBContract.Entry.COLUMN_NAME_NAME + " TEXT not null unique," +
					AccountDBContract.Entry.COLUMN_NAME_BANK + " TEXT," +
					AccountDBContract.Entry.COLUMN_NAME_TYPE + " TEXT," +
					AccountDBContract.Entry.COLUMN_NAME_IDENTIFIER + " TEXT)";

	public static final String SQL_DELETE_ENTRIES =
			"DROP TABLE IF EXISTS " + AccountDBContract.Entry.TABLE_NAME;
}
