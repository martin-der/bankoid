package net.tetrakoopa.bankoid.service.persistence;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import net.tetrakoopa.bankoid.model.AccountContent;
import net.tetrakoopa.bankoid.service.persistence.exception.NoSuchElementException;
import net.tetrakoopa.bankoid.service.persistence.exception.NonUniqueResultException;
import net.tetrakoopa.bankoid.service.persistence.exception.UniqueConstraintViolationException;
import net.tetrakoopa.bankoid.service.persistence.schema.AccountDBContract.Entry;

public class AccountPersistenceService extends AbstractPersistenceService<AccountContent.Account> implements CRUD<AccountContent.Account> {

	private final AccountDBHelper dbHelper;

	public AccountPersistenceService(Context context) {
		dbHelper = new AccountDBHelper(context);
	}

	public long insert(String name, String bank, String type, String identifier) throws UniqueConstraintViolationException {

		final SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Entry.COLUMN_NAME_NAME, name);
		values.put(Entry.COLUMN_NAME_BANK, bank);
		values.put(Entry.COLUMN_NAME_TYPE, nullableToDB(type));
		values.put(Entry.COLUMN_NAME_IDENTIFIER, identifier);

		try {
			return db.insertOrThrow(Entry.TABLE_NAME, null, values);
		} catch (SQLiteConstraintException cex) {
			throw new UniqueConstraintViolationException(AccountContent.Account.class, cex);
		}
	}

	@Override
	public void insert(AccountContent.Account account) throws UniqueConstraintViolationException  {
		final long id = insert(account.name, account.bank, account.type, account.identifier);
		account.id = id;
	}

	public AccountContent.Account retrieve(long id) throws NoSuchElementException {

		final SQLiteDatabase db = dbHelper.getReadableDatabase();

		final String[] projection = {
				Entry._ID,
				Entry.COLUMN_NAME_NAME,
				Entry.COLUMN_NAME_BANK,
				Entry.COLUMN_NAME_TYPE,
				Entry.COLUMN_NAME_IDENTIFIER
		};

		String selection = Entry._ID + " = ?";
		String[] selectionArgs = { String.valueOf(id) };


		try (final Cursor cursor = db.query(
				Entry.TABLE_NAME,   // The table to query
				projection,             // The array of columns to return (pass null to get all)
				selection,              // The columns for the WHERE clause
				selectionArgs,          // The values for the WHERE clause
				null,                   // don't group the rows
				null,                   // don't filter by row groups
				null               // The sort order
		)) {

			if (!cursor.moveToNext()) {
				throw new NoSuchElementException(AccountContent.Account.class, id);
			}

			return build(cursor);
		}
	}

	@Override
	public void delete(long id) throws NoSuchElementException {
		final SQLiteDatabase db = dbHelper.getReadableDatabase();

		String selection = Entry._ID + " = ?";
		String[] selectionArgs = { String.valueOf(id) };

		if (db.delete(Entry.TABLE_NAME, selection, selectionArgs) < 1) throw new NoSuchElementException(AccountContent.Account.class, id);
	}

	@SuppressLint("NewApi")
	public AccountContent.Account findByName(String name) throws NonUniqueResultException, NoSuchElementException {
		final SQLiteDatabase db = dbHelper.getReadableDatabase();

		final String[] projection = {
				Entry._ID,
				Entry.COLUMN_NAME_NAME,
				Entry.COLUMN_NAME_BANK,
				Entry.COLUMN_NAME_TYPE,
				Entry.COLUMN_NAME_IDENTIFIER
		};

		final String selection = Entry.COLUMN_NAME_NAME + " = ?";
		final String[] selectionArgs = { name };

		try (final Cursor cursor = db.query(
				Entry.TABLE_NAME,
				projection,
				selection,
				selectionArgs,
				null,
				null,
				null
		)) {
			return getUniqueOrThrow(cursor, AccountPersistenceService::build, selection, selectionArgs);
		}
	}

	public DBStream<AccountContent.Account> find() {
		final SQLiteDatabase db = dbHelper.getReadableDatabase();

		final String[] projection = {
				Entry._ID,
				Entry.COLUMN_NAME_NAME,
				Entry.COLUMN_NAME_BANK,
				Entry.COLUMN_NAME_TYPE,
				Entry.COLUMN_NAME_IDENTIFIER
		};

		final String sortOrder = Entry.COLUMN_NAME_NAME + " DESC";

		final Cursor cursor = db.query(
				Entry.TABLE_NAME,
				projection,
				null,
				null,
				null,
				null,
				sortOrder
		);


		return cursorStream(cursor, AccountPersistenceService::build);
	}

	private static AccountContent.Account build(Cursor cursor) {
		final String name = cursor.getString(cursor.getColumnIndexOrThrow(Entry.COLUMN_NAME_NAME));
		final String bank = cursor.getString(cursor.getColumnIndexOrThrow(Entry.COLUMN_NAME_BANK));
		final String type = cursor.getString(cursor.getColumnIndexOrThrow(Entry.COLUMN_NAME_TYPE));
		final String identifier = cursor.getString(cursor.getColumnIndexOrThrow(Entry.COLUMN_NAME_IDENTIFIER));

		return new AccountContent.Account(cursor.getLong(cursor.getColumnIndexOrThrow(Entry._ID)), name, bank, nullableFromDB(type), identifier);
	}

}
