package net.tetrakoopa.bankoid.service.business;

import android.content.Context;
import android.content.SharedPreferences;

import net.tetrakoopa.bankoid.ApplicationDataHelper;
import net.tetrakoopa.bankoid.backend.PreferencesKey;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;
import net.tetrakoopa.bankoid.debug.LogCapable;
import net.tetrakoopa.bankoid.model.AccountContent;
import net.tetrakoopa.bankoid.model.bank.Config;
import net.tetrakoopa.bankoid.model.bank.Settings;
import net.tetrakoopa.bankoid.model.common.Primitive;
import net.tetrakoopa.bankoid.util.PreferenceHelper;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;

import java.io.IOException;
import java.util.Map;

import androidx.preference.PreferenceManager;

public class AccountService implements LogCapable {

	private final Context context;

	public AccountService(Context context) {
		this.context = context;
	}

	public void performPostAccountCreationInitialization(AccountContent.Account account) {

		final Config config;
		try {
			config = ApplicationDataHelper.loadConfig(account.bank, context);
		} catch (IOException|ParseException ex) {
			logger.log(MessageLogger.LogMessage.Severity.ERROR, AccountService.class, "Failed to perform post account initialization: "+ex.getMessage(), ex);
			return;
		}

		initPreferences(account, config);
	}

	private void initPreferences(AccountContent.Account account, Config config) {

		final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

		final String backend = account.bank;

		final String mainKey = PreferencesKey.getForBackend(backend, "~initialization");

		if (sharedPreferences.contains(mainKey)) return;

		final SharedPreferences.Editor editor = sharedPreferences.edit();

		try {

			editor.putInt(mainKey, 1);

			for (Map.Entry<String, Settings.Setting> settingEntry : config.getSettings().getEntries().entrySet()) {
				final String key = PreferencesKey.getForBackend(backend, settingEntry.getKey());
				final Settings.Setting setting = settingEntry.getValue();

				if (!sharedPreferences.contains(key)) {
					PreferenceHelper.putValue(editor, key, setting.primitive.type, setting.defaultz);
				}
			}

		} finally {
			editor.commit();
		}

	}

}
