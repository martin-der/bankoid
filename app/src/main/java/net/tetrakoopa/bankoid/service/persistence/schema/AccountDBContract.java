package net.tetrakoopa.bankoid.service.persistence.schema;

import android.provider.BaseColumns;

public final class AccountDBContract {

	private AccountDBContract() {}

	/* Inner class that defines the table contents */
	public static class Entry implements BaseColumns {
		public static final String TABLE_NAME = "entry";
		public static final String COLUMN_NAME_NAME = "name";
		public static final String COLUMN_NAME_BANK = "bank";
		public static final String COLUMN_NAME_TYPE = "type";
		public static final String COLUMN_NAME_IDENTIFIER = "identifier";
	}
}
