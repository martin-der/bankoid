package net.tetrakoopa.bankoid.service.persistence;

import android.annotation.SuppressLint;
import android.database.Cursor;

import net.tetrakoopa.bankoid.service.persistence.exception.NoSuchElementException;
import net.tetrakoopa.bankoid.service.persistence.exception.NonUniqueResultException;

import org.springframework.core.GenericTypeResolver;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public abstract class AbstractPersistenceService<E> implements CRUD<E> {

	private final Class<E> entityClass;

	protected AbstractPersistenceService() {
		final Class<?>[] genericTypes = GenericTypeResolver.resolveTypeArguments(this.getClass(), AbstractPersistenceService.class);
		entityClass = (Class<E>) genericTypes[0];
	}

	protected static String nullableToDB(String value) {
		return value==null ? "" : value;
	}

	protected static String nullableFromDB(String value) {
		return value.equals("") ? null : value;
	}

	@FunctionalInterface
	protected interface Mapper<E> {
		E convert(Cursor cursor);
	}

	protected static <E> Iterator<E> cursorIterator(final Cursor cursor, final Mapper<E> mapper) {

		return new Iterator<E>() {
			@Override
			public boolean hasNext() {
				return cursor.moveToNext();
			}

			@Override
			public E next() {
				return mapper.convert(cursor);
			}
		};
	}

	protected E getUniqueOrThrow(Cursor cursor, final Mapper<E> mapper, String selection, String[] selectionArgs) throws NoSuchElementException, NonUniqueResultException {
		final E e = getUnique(cursor, mapper);
		if (e == null) {
			throw new NoSuchElementException(entityClass, selection, selectionArgs);
		}
		return e;
	}
	protected E getUnique(Cursor cursor, final Mapper<E> mapper) throws NonUniqueResultException {
		if (!cursor.moveToNext()) {
			return null;
		}

		final E e = mapper.convert(cursor);

		if (cursor.moveToNext()) {
			throw new NonUniqueResultException(entityClass);
		}

		return e;
	}

	@SuppressLint("NewApi")
	protected static <E> DBStream<E> cursorStream(final Cursor cursor, final Mapper<E> mapper) {
		final Stream<E> stream = StreamSupport.stream(Spliterators.spliteratorUnknownSize(cursorIterator(cursor, mapper),Spliterator.ORDERED), false);
		return new DBStream<>(stream, cursor);
	}

}
