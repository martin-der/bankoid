package net.tetrakoopa.bankoid.service.persistence.exception;

public class NoSuchElementException extends PersistenceException {

	public NoSuchElementException(Class<?> entity, long id) {
		super("No such "+entity.getName()+" with id "+id);
	}

	public NoSuchElementException(Class<?> entity, String selection, String[] selectionArgs) {
		super("No such "+entity.getName()+" by "+selectionAndSelectionArgsToString(selection, selectionArgs));
	}
}
