package net.tetrakoopa.bankoid.service.persistence.exception;

public class NonUniqueResultException extends PersistenceException {


	public NonUniqueResultException(Class<?> entity) {
		super("Got more than one entity");
	}
}
