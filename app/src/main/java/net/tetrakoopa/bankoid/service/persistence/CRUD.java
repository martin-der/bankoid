package net.tetrakoopa.bankoid.service.persistence;

import android.annotation.SuppressLint;
import android.database.Cursor;

import net.tetrakoopa.bankoid.service.persistence.exception.NoSuchElementException;
import net.tetrakoopa.bankoid.service.persistence.exception.UniqueConstraintViolationException;

import java.util.stream.Stream;

public interface CRUD<E> {

	@SuppressLint("NewApi")
	class DBStream<E> implements AutoCloseable {

		public final Stream<E> data;
		private final Cursor cursor;

		public DBStream(Stream<E> data, Cursor cursor) {
			this.data = data;
			this.cursor = cursor;
		}

		@Override
		public void close() {
			cursor.close();
		}
	}

	E retrieve(long id) throws NoSuchElementException;

	void insert(E e) throws UniqueConstraintViolationException;

	void delete(long id) throws NoSuchElementException;

	DBStream<E> find();
}
