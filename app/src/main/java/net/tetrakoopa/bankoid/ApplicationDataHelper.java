package net.tetrakoopa.bankoid;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.RemoteException;
import android.widget.Toast;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.backend.WebBackendProviderConnection;
import net.tetrakoopa.bankoid.debug.LogCapable;
import net.tetrakoopa.bankoid.model.AccountContent;
import net.tetrakoopa.bankoid.model.bank.About;
import net.tetrakoopa.bankoid.model.bank.Config;
import net.tetrakoopa.bankoid.model.bank.Settings;
import net.tetrakoopa.bankoid.service.persistence.AccountPersistenceService;
import net.tetrakoopa.bankoid.service.persistence.CRUD;
import net.tetrakoopa.bankoid.service.persistence.exception.NoSuchElementException;
import net.tetrakoopa.bankoid.service.persistence.exception.NonUniqueResultException;
import net.tetrakoopa.bankoid.util.AssetsReader;
import net.tetrakoopa.bankoid.util.DeclaredValue;
import net.tetrakoopa.bankoid.util.PreferenceHelper;
import net.tetrakoopa.bankoid.util.ResourceHelper;
import net.tetrakoopa.bankoid.util.xml.AboutParser;
import net.tetrakoopa.bankoid.util.xml.ConfigParser;
import net.tetrakoopa.bankoid.util.xml.common.DrawableParser;
import net.tetrakoopa.bankoid.util.xml.common.PrimitiveParser;
import net.tetrakoopa.bankoid.util.xml.config.SettingsParser;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;
import net.tetrakoopa.bankoid.backend.IWebBackend;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;

import org.springframework.util.ReflectionUtils;

import java.io.IOException;
import java.io.InputStream;

public class ApplicationDataHelper implements LogCapable {

	private static int bankLogoWidthDP = 80;
	private static int bankLogoHeightDP = 80;

	public static void doFistInitialization(SharedPreferences sharedPreferences) {
		initializePreferences(sharedPreferences);
	}
	public static void initializePreferences(SharedPreferences sharedPreferences) {
		final SharedPreferences.Editor editor = sharedPreferences.edit();
		ReflectionUtils.doWithLocalFields(SettingsActivity.Key.class, field -> {
			if (field.getType().equals(String.class)) {
				final String key = (String) field.get(null);
				final DeclaredValue.Default defaultAnnotation = field.getAnnotation(DeclaredValue.Default.class);
				if (defaultAnnotation != null) {
					if (!sharedPreferences.contains(key)) {
						PreferenceHelper.putValueOrLog(editor, key, defaultAnnotation);
					}

				}
			}
		});
		editor.commit();
	}

	public static void loadStartingAccount(Context context, SharedPreferences sharedPreferences, ApplicationData.InitializationUIMessage.Pusher messagesPusher) {
		String accountName = null;

		final SettingsActivity.Key.AccountSelectionWhenStarting accountSelection;
		try {
			accountSelection = SettingsActivity.Key.AccountSelectionWhenStarting.fromKey(sharedPreferences.getString(SettingsActivity.Key.ACCOUNT_SELECTION_WHEN_STARTING, null));
		} catch (DeclaredValue.DeclaredEnum.InvalidEnumKeyException ex) {
			logger.log(MessageLogger.LogMessage.Severity.ERROR, ApplicationDataHelper.class,"Failed to restore account at start", ex);
			return;
		}
		if (accountSelection == SettingsActivity.Key.AccountSelectionWhenStarting.PICK) {
			accountName = sharedPreferences.getString(SettingsActivity.Key.ACCOUNT_SELECTED_BY_PICKING, null);
		} else if (accountSelection == SettingsActivity.Key.AccountSelectionWhenStarting.PREVIOUS) {
			accountName = sharedPreferences.getString(SettingsActivity.Key.ACCOUNT_SELECTED_PREVIOUSLY, null);
		} else {
			return;
		}

		final String resolvedAccountName = accountName;

		if (resolvedAccountName == null) return;

		final AccountContent.Account account;
		try {
			account = new AccountPersistenceService(context).findByName(accountName);
		} catch (NonUniqueResultException ex) {
			messagesPusher.push((c) -> Oops.ERROR.logAndNotify(c, ApplicationDataHelper.class, ResourceHelper.text(c, R.string.message_error_failed_to_restore_account_while_starting, resolvedAccountName), ex));
			return;
		} catch (NoSuchElementException ex) {
			messagesPusher.push((c) -> Oops.ERROR.logAndNotify(c, () -> {
				if (accountSelection == SettingsActivity.Key.AccountSelectionWhenStarting.PICK) {
					logger.log(MessageLogger.LogMessage.Severity.INFO, ApplicationDataHelper.class, "Removed non-existent account '"+resolvedAccountName+"' selected by picking for auto-loading at start");
					sharedPreferences.edit().putString(SettingsActivity.Key.ACCOUNT_SELECTED_BY_PICKING, null).commit();
				} else if (accountSelection == SettingsActivity.Key.AccountSelectionWhenStarting.PREVIOUS) {
					logger.log(MessageLogger.LogMessage.Severity.INFO, ApplicationDataHelper.class, "Removed non-existent account '"+resolvedAccountName+"' selected previously for auto-loading at start");
					sharedPreferences.edit().putString(SettingsActivity.Key.ACCOUNT_SELECTED_PREVIOUSLY, null).commit();
				}
			}, ApplicationDataHelper.class, ResourceHelper.text(c, R.string.message_error_failed_to_restore_account_while_starting, resolvedAccountName), ex));
			return;
		}

		try {
			ApplicationData.instance().setCurrentAccount(context, account);
		} catch (Exception ex) {
			messagesPusher.push((c) -> Oops.ERROR.logAndNotify(c, ApplicationDataHelper.class, ResourceHelper.text(c, R.string.message_error_failed_to_restore_account_while_starting, resolvedAccountName), ex));
		}
	}

	@SuppressLint("NewApi")
	public static void loadAccounts(Context context) {
		AccountContent.clear();
		try (final CRUD.DBStream<AccountContent.Account> stream = new AccountPersistenceService(context).find()) {
			stream.data.forEach(account -> AccountContent.add(account));
		}
	}

	public static About loadAbout(String name, Context context) throws IOException, ParseException {
		return loadAbout(name, context, new AboutParser());
	}
	public static About loadAbout(String name, Context context, AboutParser parser) throws IOException, ParseException {
		try (final InputStream aboutStream = context.getAssets().open("backend/"+name+"/about.xml")) {
			final About about = parser.parse(aboutStream);
			if (about.logo != null) {
				about.logo.loadDrawable(description -> loadBackendLogoDrawable(context, name, description));
			}
			return about;
		}
	}

	public static Settings loadReferenceSettings(Context context) throws IOException, ParseException {
		try (final InputStream configStream = context.getAssets().open("common/settings.xml")) {
			return new SettingsParser.Standalone().parse(configStream);
		}
	}

	public static Config loadConfig(String name, Context context) throws IOException, ParseException {
		return loadConfig(name, context, new ConfigParser(ApplicationData.instance().referenceSettings()));
	}
	private static Config loadConfig(String name, Context context, ConfigParser parser) throws IOException, ParseException {
		try (final InputStream configStream = context.getAssets().open("backend/"+name+"/config.xml")) {
			final Config config = parser.parse(configStream);
			return config;
		}
	}

	private static Drawable loadBackendLogoDrawable(Context context, String backend, DrawableParser.DrawableDescription description) {
		String iconType = description.type;
		if (iconType ==null) iconType = "png";
		iconType = iconType.toLowerCase();
		try {
			if (iconType.equals("svg")) {
				return AssetsReader.getDrawableFromSVGAssets(context, "backend/" + backend + "/logo." + iconType, AssetsReader.DisplayMetricsHelper.dpToPx(bankLogoWidthDP), AssetsReader.DisplayMetricsHelper.dpToPx(bankLogoHeightDP), null);
			} else {
				return AssetsReader.getDrawableFromAssets(context, "backend/" + backend + "/logo." + iconType);
			}
		} catch (Exception ex) {
			logger.log(MessageLogger.LogMessage.Severity.ERROR, ApplicationData.class, "Failed to get '"+backend+"' backend logo", ex);
		}
		return null;
	}

	public static void startDiscoveringWebBackends(Context context) throws RemoteException {
		final WebBackendProviderConnection.Helper<WebBackendProviderConnection> helper = new WebBackendProviderConnection.Helper<>(context);
		final WebBackendProviderConnection connection = helper.initService(new WebBackendProviderConnection.ConnectionStatusListener() {
			@Override
			public void onConnected(ComponentName name, IWebBackend backend) {
				Toast.makeText(context,"Service Connected!",Toast.LENGTH_LONG).show();
			}

			@Override
			public void onDisconnected(ComponentName name) {
				Toast.makeText(context,"Service Disconnected!",Toast.LENGTH_LONG).show();
			}
		});
	}
}
