package net.tetrakoopa.bankoid.debug;

import net.tetrakoopa.bankoid.backend.debug.MessageLogger;

public interface LogCapable {

	MessageLogger logger = FullLogger.instance();
}
