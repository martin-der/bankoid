package net.tetrakoopa.bankoid.debug;

import net.tetrakoopa.bankoid.model.LogMessageContent;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;

public class FullLogger implements MessageLogger {

	private final static FullLogger instance = new FullLogger();
	public static FullLogger instance() {
		return instance;
	}

	@Override
	public long log(LogMessage.Severity severity, LogMessage.Type type, LogMessage.Location location, String content, String details, Exception cause) {
		return LogMessageContent.addMessage(new LogMessage(severity, type, location, content, details, cause));
	}

	@Override
	public long log(LogMessage.Severity severity, LogMessage.Type type, LogMessage.Location location, String content, String details) {
		return LogMessageContent.addMessage(new LogMessage(severity, type, location, content, details));
	}

	@Override
	public long log(LogMessage.Severity severity, LogMessage.Type type, String location, String content, String details, Exception cause) {
		return LogMessageContent.addMessage(new LogMessage(severity, type, location, content, details, cause));
	}

	@Override
	public long log(LogMessage.Severity severity, LogMessage.Type type, String location, String content, String details) {
		return LogMessageContent.addMessage(new LogMessage(severity, type, location, content, details));
	}

	@Override
	public long log(LogMessage.Severity severity, LogMessage.Type type, int location, String content, String details, Exception cause) {
		return LogMessageContent.addMessage(new LogMessage(severity, type, location, content, details, cause));
	}

	@Override
	public long log(LogMessage.Severity severity, LogMessage.Type type, int location, String content, String details) {
		return LogMessageContent.addMessage(new LogMessage(severity, type, location, content, details));
	}

	@Override
	public long log(LogMessage.Severity severity, Class<?> fragmentOrActivity, String content, String details, Exception cause) {
		return LogMessageContent.addMessage(new LogMessage(severity, fragmentOrActivity, content, details, cause));
	}

	@Override
	public long log(LogMessage.Severity severity, Class<?> fragmentOrActivity, String content, String details) {
		return LogMessageContent.addMessage(new LogMessage(severity, fragmentOrActivity, content, details));
	}

	@Override
	public long log(LogMessage.Severity severity, Class<?> fragmentOrActivity, String content, Exception cause) {
		return LogMessageContent.addMessage(new LogMessage(severity, fragmentOrActivity, content, cause));
	}

	@Override
	public long log(LogMessage.Severity severity, Class<?> fragmentOrActivity, String content) {
		return LogMessageContent.addMessage(new LogMessage(severity, fragmentOrActivity, content));
	}
}
