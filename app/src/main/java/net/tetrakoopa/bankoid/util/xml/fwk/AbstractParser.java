package net.tetrakoopa.bankoid.util.xml.fwk;

import android.util.Xml;

import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.XmlParseException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

public abstract class AbstractParser<E> extends CommonParser<E> {

	private static final String NO_NAMESPACE = null;

	protected XmlPullParser parser;

	public AbstractParser() {
		parser = Xml.newPullParser();
	}

	/**
	 * Used by tests. The constructor provides a parser.
	 * @param parser
	 */
	public void setParser(XmlPullParser parser) {
		this.parser = parser;
	}

	/** @return if non null <code>parse(InputStream)</code> will assert that the first tag is this one */
	public abstract String requiredTag();

	public final E parse(InputStream inputStream) throws IOException, ParseException {
		if (inputStream == null) throw new IllegalArgumentException("input stream must not be null");
		try {
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(inputStream, null);
			parser.nextTag();

			final String requiredTag = requiredTag();
			if (requiredTag != null) {
				parser.require(XmlPullParser.START_TAG, NO_NAMESPACE, requiredTag);
			}
			return parse(parser);
		} catch (XmlPullParserException xpex) {
			throw new XmlParseException(xpex);
		}
	}

}
