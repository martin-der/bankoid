package net.tetrakoopa.bankoid.util.xml.fwk.exception;

import net.tetrakoopa.bankoid.util.xml.common.PrimitiveParser;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ContractViolationParseException;

public class InvalidPrimitiveTypeParseException extends ContractViolationParseException {

	public InvalidPrimitiveTypeParseException(PrimitiveParser.InvalidPrimitiveTypeException iptex, int lineNumber, int columnNumber) {
		super(iptex.getMessage(), lineNumber, columnNumber);
	}

	public InvalidPrimitiveTypeParseException(String value, int lineNumber, int columnNumber) {
		super(buildMessage(value), lineNumber, columnNumber);
	}

	private static String buildMessage(String string) {
		return "'"+string+"' is not a valid Primitive type";
	}

}
