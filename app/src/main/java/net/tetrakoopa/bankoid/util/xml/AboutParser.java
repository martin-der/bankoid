package net.tetrakoopa.bankoid.util.xml;

import net.tetrakoopa.bankoid.model.bank.About;
import net.tetrakoopa.bankoid.util.xml.common.DrawableParser;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.TagBadCardinalityParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.AbstractParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class AboutParser extends AbstractParser<About> {

	public static final String TAG_ABOUT = "about";

	public static final String TAG_ABOUT_NAME = "name";
	public static final String TAG_ABOUT_IDENTIFIER = "identifier";
	public static final String TAG_ABOUT_DESCRIPTION = "description";
	public static final String TAG_ABOUT_LOGO = "logo";

	@Override
	public String requiredTag() {
		return TAG_ABOUT ;
	}

	final DrawableParser drawableParser = new DrawableParser();

	public About parse(XmlPullParser parser) throws ParseException, IOException, XmlPullParserException {

		String name = null;
		String identifier = null;
		String description = null;
		About.Logo logo = null;


		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String tagName = parser.getName();

			if (tagName.equals(TAG_ABOUT_NAME)) {
				if (name != null)
					throw new TagBadCardinalityParseException(TAG_ABOUT_NAME, TagBadCardinalityParseException.Type.EXPECTED_ONE_FOUND_MANY, parser.getLineNumber(), parser.getColumnNumber());
				name = readText(parser);
			} else if (tagName.equals(TAG_ABOUT_IDENTIFIER)) {
				if (identifier != null)
					throw new TagBadCardinalityParseException(TAG_ABOUT_IDENTIFIER, TagBadCardinalityParseException.Type.EXPECTED_ONE_FOUND_MANY, parser.getLineNumber(), parser.getColumnNumber());
				identifier = readText(parser);
			} else if (tagName.equals(TAG_ABOUT_DESCRIPTION)) {
				if (description != null)
					throw new TagBadCardinalityParseException(TAG_ABOUT_DESCRIPTION, TagBadCardinalityParseException.Type.EXPECTED_ONE_FOUND_MANY, parser.getLineNumber(), parser.getColumnNumber());
				description = readText(parser);
			} else if (tagName.equals(TAG_ABOUT_LOGO)) {
				if (logo != null)
					throw new TagBadCardinalityParseException(TAG_ABOUT_LOGO, TagBadCardinalityParseException.Type.EXPECTED_ONE_FOUND_MANY, parser.getLineNumber(), parser.getColumnNumber());
				logo = parseLogo(parser);
			} else {
				skip(parser);
			}
		}

		return new About(name, identifier, description, logo);
	}

	private About.Logo parseLogo(XmlPullParser parser) {
		return new About.Logo(drawableParser.parse(parser));
	}


}
