package net.tetrakoopa.bankoid.util.xml.common;

import android.graphics.drawable.Drawable;

import net.tetrakoopa.bankoid.util.xml.fwk.CommonParser;

import org.xmlpull.v1.XmlPullParser;

public class DrawableParser extends CommonParser<DrawableParser.DrawableDescription> {

	public interface DrawableLoader {
		Drawable load(DrawableDescription description);
	}

	public static class DrawableDescription {
		public final String name;

		public final String type;

		public final String background;

		public DrawableDescription(String name, String type, String background) {
			this.name = name;
			this.type = type;
			this.background = background;
		}
	}
	public static class DrawableHelper {
		private final DrawableParser.DrawableDescription description;

		public DrawableHelper(DrawableDescription description) {
			this.description = description;
		}

		public Drawable load(DrawableParser.DrawableLoader loader) {
			return loader.load(description);
		}
	}

	public DrawableDescription parse(XmlPullParser parser) {
		final String name = parser.getAttributeValue(null, "name");
		final String type = parser.getAttributeValue(null, "type");
		final String background = parser.getAttributeValue(null, "background");
		return new DrawableDescription(name, type, background);
	}

	public static abstract class DeserializedDrawable {

		private DrawableParser.DrawableHelper helper;

		private Drawable drawable;

		public DeserializedDrawable(DrawableParser.DrawableDescription description) {
			this.helper = new DrawableParser.DrawableHelper(description);
		}

		public Drawable getDrawable() {
			return drawable;
		}

		public void loadDrawable(DrawableParser.DrawableLoader loader) {
			this.drawable = helper.load(loader);
		}
	}
}
