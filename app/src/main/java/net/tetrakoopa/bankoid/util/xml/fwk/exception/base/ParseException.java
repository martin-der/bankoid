package net.tetrakoopa.bankoid.util.xml.fwk.exception.base;

public abstract class ParseException extends Exception {

	public ParseException(String message) {
		super(message);
	}
	public ParseException(Exception cause) {
		super(cause);
	}
	public ParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public final String getMessage() {
		return prefixLocation(getLineNumber(), getColumnNumber())+super.getMessage();
	}

	public abstract int getLineNumber();

	public abstract int getColumnNumber();

	private static String prefixLocation(int line, int column) {
		return "l."+line+":"+column+": ";
	}

}
