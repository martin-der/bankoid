package net.tetrakoopa.bankoid.util.xml;

import net.tetrakoopa.bankoid.model.common.Constraint;
import net.tetrakoopa.bankoid.util.EnumUtil;
import net.tetrakoopa.bankoid.util.xml.fwk.CommonParser;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.AttributeMissingParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.InvalidAttributeValueParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.InvalidTextContentParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class ConstraintParser extends CommonParser<Constraint>  {

	public static final String ATTRIBUTE_TYPE = "type";
	public static final String ATTRIBUTE_DESCRIPTION = "description";

	@Override
	public Constraint parse(XmlPullParser parser) throws ParseException, IOException, XmlPullParserException {
		final String typeString = parser.getAttributeValue(null, ATTRIBUTE_TYPE);
		final String description = parser.getAttributeValue(null, ATTRIBUTE_DESCRIPTION);

		final Constraint.Type type;
		try {
			type = Constraint.Type.fromNameIgnoreCase(typeString);
		} catch (EnumUtil.InvalidEnumCriteriaException.Name iaex) {
			throw new InvalidAttributeValueParseException(ATTRIBUTE_TYPE, typeString, parser.getLineNumber(), parser.getColumnNumber(), iaex);
		}
		if (type == null) throw new AttributeMissingParseException(ATTRIBUTE_TYPE, parser.getLineNumber(), parser.getColumnNumber());
		final String value = readText(parser);
		skipRemaining(parser);
		switch (type) {
			case REGEX: return new Constraint(value, description);
			case MIN: case MAX:
				final int limit;
				try {
					limit = Integer.parseInt(value);
				} catch (NumberFormatException nfex) {
					throw new InvalidTextContentParseException(ATTRIBUTE_TYPE, parser.getLineNumber(), parser.getColumnNumber(), nfex);
				}
				return new Constraint(type, limit, description);
			default:
				throw new InternalError("Case "+type.name()+" not implemented");
		}
	}

}
