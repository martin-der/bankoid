package net.tetrakoopa.bankoid.util.xml.fwk.exception.base;

public class InvalidTagException extends ContractViolationParseException {

	public InvalidTagException(String message, int lineNumber, int columnNumber) {
		super(message, lineNumber, columnNumber);
	}

	public InvalidTagException(String message, int lineNumber, int columnNumber, Throwable ex) {
		super(message, lineNumber, columnNumber, ex);
	}
}
