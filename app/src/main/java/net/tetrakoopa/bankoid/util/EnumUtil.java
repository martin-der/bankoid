package net.tetrakoopa.bankoid.util;

import android.annotation.SuppressLint;

import java.lang.reflect.Field;

public class EnumUtil {

	public static class InvalidEnumCriteriaException extends Exception {

		protected InvalidEnumCriteriaException(Class<? extends Enum<?>> enumClass, String criteria, String value) {
			super("No such "+ enumClass.getName()+" with "+criteria+" '"+value+"'");
		}

		public static class Name extends InvalidEnumCriteriaException {
			public Name(Class<? extends Enum<?>> enumClass, String name) {
				super(enumClass, "name", name);
			}
		}
	}

	public static <E extends Enum<E>> E fromNameIgnoreCase(Class<E> enumClass, String name, E valueIfInvalid) throws InvalidEnumCriteriaException.Name {
		try {
			return fromNameIgnoreCase(enumClass, name);
		} catch (InvalidEnumCriteriaException.Name iecex) {
			return valueIfInvalid;
		}
	}

	/**
	 * @throws InvalidEnumCriteriaException.Name if no <code>Type</code> named <code>name</code> exist
	 */
	public static <E extends Enum<E>> E fromNameIgnoreCase(Class<E> enumClass, String name) throws InvalidEnumCriteriaException.Name {
		if (name==null) return null;
		name = name.toUpperCase();
		return fromName(enumClass, name.toUpperCase());
	}

	public static <E extends Enum<E>> E fromName(Class<E> enumClass, String name, E valueIfInvalid) {
		try {
			return fromName(enumClass, name);
		} catch (InvalidEnumCriteriaException.Name iecex) {
			return valueIfInvalid;
		}
	}
	/**
	 * @throws InvalidEnumCriteriaException.Name if no <code>Type</code> named <code>name</code> exist
	 */
	public static <E extends Enum<E>> E fromName(Class<E> enumClass, String name) throws InvalidEnumCriteriaException.Name {
		if (name==null) return null;
		for (E value : enumClass.getEnumConstants()) {
			if (value.name().equals(name)) return value;
		}
		throw new InvalidEnumCriteriaException.Name(enumClass, name);

	}

	@SuppressLint("NewApi")
	public static <E extends Enum<E>> Field getField(E e) {
		final Class<E> enumClass = (Class<E>) e.getClass();
		try {
			return enumClass.getField(e.name());
		} catch (NoSuchFieldException noSuchFieldException) {
			throw new InternalError(noSuchFieldException);
		}
	}


}
