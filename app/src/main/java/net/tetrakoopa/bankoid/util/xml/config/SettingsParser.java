package net.tetrakoopa.bankoid.util.xml.config;

import net.tetrakoopa.bankoid.model.bank.Settings;
import net.tetrakoopa.bankoid.model.common.AndroidPreferenceI18NText;
import net.tetrakoopa.bankoid.model.common.Constraint;
import net.tetrakoopa.bankoid.model.common.Primitive;
import net.tetrakoopa.bankoid.util.xml.ConstraintParser;
import net.tetrakoopa.bankoid.util.xml.common.PrimitiveParser;
import net.tetrakoopa.bankoid.util.xml.fwk.AbstractParser;
import net.tetrakoopa.bankoid.util.xml.fwk.CommonParser;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.InvalidPrimitiveTypeParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.InvalidPrimitiveValueParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.AttributeMissingParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.InvalidAttributeValueParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.InvalidTagException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class SettingsParser extends CommonParser<Settings> {

	private final static String TAG_SETTINGS = "settings";

	private final static String TAG_SETTING_REFERENCE = "reference";
	private final static String TAG_SETTING_DEPENDENCY = "dependency";
	private final static String TAG_SETTING_TYPE = "type";
	private final static String TAG_SETTING_DEFAULT = "default";
	private final static String TAG_SETTING_GROUP = "group";
	private final static String TAG_SETTING_GROUP_NAME = "name";

	private final static String TAG_CONSTRAINT = "constraint";


	private final static String TAG_TEXT = "text";
	private final static String TAG_TEXT_TITLE = "title";
	private final static String TAG_TEXT_SUMMARY = "summary";
	private final static String TAG_TEXT_SUMMARY_ON = "summaryOn";
	private final static String TAG_TEXT_SUMMARY_OFF = "summaryOff";

	private final ConstraintParser constraintParser;

	public static class Standalone extends AbstractParser<Settings> {

		private final SettingsParser delegateParser;

		public Standalone() {
			this.delegateParser = new SettingsParser();
		}
		public Standalone(Settings reference) {
			this.delegateParser = new SettingsParser(reference);
		}

		@Override
		public String requiredTag() {
			return TAG_SETTINGS;
		}

		@Override
		public Settings parse(XmlPullParser parser) throws ParseException, IOException, XmlPullParserException {
			return delegateParser.parse(parser);
		}
	}

	private final Settings reference;

	public SettingsParser() {
		this(null);
	}
	public SettingsParser(Settings reference) {
		this.constraintParser = new ConstraintParser();
		this.reference = reference;
	}

	@Override
	public Settings parse(XmlPullParser parser) throws ParseException, IOException, XmlPullParserException {

		final Settings settings = new Settings();

		final Pattern validName = Pattern.compile(Settings.Setting.VALID_NAME_REGEX);

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			final String tagName = parser.getName();

			if (!validName.matcher(tagName).matches()) {
				throw new InvalidTagException("Setting name '"+tagName+"' does not match '"+Settings.Setting.VALID_NAME_REGEX+"'", parser.getLineNumber(), parser.getColumnNumber());
			}
			if (settings.getEntries().containsKey(tagName)) {
				throw new InvalidTagException("Setting '"+tagName+"' already declared", parser.getLineNumber(), parser.getColumnNumber());
			}

			final Settings.Setting setting = parseSetting(parser, tagName);

			if (setting.dependency != null && !settings.getEntries().containsKey(setting.dependency)) {
				throw new InvalidAttributeValueParseException(TAG_SETTING_DEPENDENCY, setting.dependency, parser.getLineNumber(), parser.getColumnNumber(), "No previously declared setting named '"+setting.dependency+"'");
			}

			settings.getEntries().put(tagName, setting);
		}

		return settings;
	}

	private Settings.Setting parseSetting(XmlPullParser parser, String name) throws IOException, XmlPullParserException, ParseException {

		final String typeString = parser.getAttributeValue(null, TAG_SETTING_TYPE);
		final String defaultValue = parser.getAttributeValue(null, TAG_SETTING_DEFAULT);

		final boolean hasDiscriminantConfiguration = typeString != null;

		String reference = parser.getAttributeValue(null, TAG_SETTING_REFERENCE);
		if (reference == null && !hasDiscriminantConfiguration) reference = name;

		final Primitive primitive;
		final Object defaultz;
		final List<Constraint> constraints = new ArrayList<>();
		final List<String> groups = new ArrayList<>();

		AndroidPreferenceI18NText referenceText = null;

		if (reference != null) {
			if (hasDiscriminantConfiguration) {
				throw new InvalidAttributeValueParseException(TAG_SETTING_REFERENCE, "", parser.getLineNumber(), parser.getColumnNumber(), "Cannot define any of '" + TAG_SETTING_TYPE + "' or '" + TAG_SETTING_DEFAULT + "' with '" + TAG_SETTING_REFERENCE + "'");
			}

			if (!this.reference.getEntries().containsKey(reference)) {
				throw new InvalidAttributeValueParseException(TAG_SETTING_REFERENCE, reference, parser.getLineNumber(), parser.getColumnNumber(), "Unknown reference '" + reference + "'");
			}

			final Settings.Setting referenceSetting = this.reference.getEntries().get(reference);
			referenceText = referenceSetting.text;

			primitive = referenceSetting.primitive;

			if (defaultValue != null) {
				try {
					defaultz = PrimitiveParser.deserializeValueForPrimitive(primitive, defaultValue);
				} catch (PrimitiveParser.InvalidPrimitiveValueException ex) {
					throw new InvalidPrimitiveValueParseException(primitive.type, ex.getMessage(), parser.getLineNumber(), parser.getColumnNumber(), ex);
				}
			} else
				defaultz = referenceSetting.defaultz;

			for (String group : referenceSetting.groups) {
				groups.add(group);
			}
		} else {
			if (typeString == null) throw new AttributeMissingParseException(TAG_SETTING_TYPE, parser.getLineNumber(), parser.getColumnNumber());
			if (defaultValue == null) throw new AttributeMissingParseException(TAG_SETTING_DEFAULT, parser.getLineNumber(), parser.getColumnNumber());
			try {
				primitive = PrimitiveParser.deserializePrimitive(typeString);
			} catch (PrimitiveParser.InvalidPrimitiveTypeException ex) {
				throw new InvalidPrimitiveTypeParseException(ex, parser.getLineNumber(), parser.getColumnNumber());
			}
			try {
				defaultz = PrimitiveParser.deserializeValueForPrimitive(primitive, defaultValue);
			} catch (PrimitiveParser.InvalidPrimitiveValueException ex) {
				throw new InvalidPrimitiveValueParseException(primitive.type, ex.getMessage(), parser.getLineNumber(), parser.getColumnNumber(), ex);
			}
		}

		final String dependencyString = parser.getAttributeValue(null, TAG_SETTING_DEPENDENCY);

		AndroidPreferenceI18NText text = null;

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			final String tagName = parser.getName();

			if (tagName.equals(TAG_TEXT)) {
				text = parseText(parser, referenceText);
			} else if (tagName.equals(TAG_CONSTRAINT)) {
				constraints.add(constraintParser.parse(parser));
			} else if (tagName.equals(TAG_SETTING_GROUP)) {
				groups.add(parseGroup(parser));
			} else {
				skip(parser);
			}
		}

		if (text == null && referenceText != null) text = referenceText;

		return new Settings.Setting(primitive, constraints.toArray(new Constraint[constraints.size()]), groups.toArray(new String[groups.size()]), dependencyString, defaultz, text);
	}

	private String parseGroup(XmlPullParser parser) throws IOException, XmlPullParserException, AttributeMissingParseException {
		final String group = parser.getAttributeValue(null, TAG_SETTING_GROUP_NAME);
		if (group == null) throw new AttributeMissingParseException(TAG_SETTING_GROUP_NAME, parser.getLineNumber(), parser.getColumnNumber());
		skipRemaining(parser);
		return group;
	}

	private AndroidPreferenceI18NText parseText(XmlPullParser parser, AndroidPreferenceI18NText referenceText) throws IOException, XmlPullParserException {
		final String title = parser.getAttributeValue(null, TAG_TEXT_TITLE);
		final String summary = parser.getAttributeValue(null, TAG_TEXT_SUMMARY);
		final String summaryOn = parser.getAttributeValue(null, TAG_TEXT_SUMMARY_ON);
		final String summaryOff = parser.getAttributeValue(null, TAG_TEXT_SUMMARY_OFF);
		skipRemaining(parser);
		return new AndroidPreferenceI18NText(
				title == null && referenceText!=null
						? referenceText.title
						: title,
				summary == null && referenceText!=null
						? referenceText.summary
						: summary,
				summaryOn == null && referenceText!=null
						? referenceText.summaryOn
						: summaryOn,
				summaryOff == null && referenceText!=null
						? referenceText.summaryOff
						: summaryOff);
	}
}
