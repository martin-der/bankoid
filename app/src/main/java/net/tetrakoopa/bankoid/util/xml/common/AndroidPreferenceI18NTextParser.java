package net.tetrakoopa.bankoid.util.xml.common;

import net.tetrakoopa.bankoid.model.common.AndroidPreferenceI18NText;
import net.tetrakoopa.bankoid.util.xml.fwk.CommonParser;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class AndroidPreferenceI18NTextParser extends CommonParser<AndroidPreferenceI18NText> {

	@Override
	public AndroidPreferenceI18NText parse(XmlPullParser parser) throws ParseException, IOException, XmlPullParserException {
		final String title = parser.getAttributeValue(null, "title");
		final String summary = parser.getAttributeValue(null, "summary");
		final String summaryOn = parser.getAttributeValue(null, "summaryOn");
		final String summaryOff = parser.getAttributeValue(null, "summaryOff");
		return new AndroidPreferenceI18NText(title, summary, summaryOn, summaryOff);
	}
}
