package net.tetrakoopa.bankoid.util.xml.fwk.exception.base;

public abstract class ContractViolationParseException extends ParseException {

	private final int lineNumber;
	private final int columnNumber;

	public ContractViolationParseException(String message, int lineNumber, int columnNumber) {
		super(message);
		this.lineNumber = lineNumber;
		this.columnNumber = columnNumber;
	}

	public ContractViolationParseException(String message, int lineNumber, int columnNumber, Throwable cause) {
		super(message, cause);
		this.lineNumber = lineNumber;
		this.columnNumber = columnNumber;
	}

	@Override
	public int getLineNumber() {
		return lineNumber;
	}

	@Override
	public int getColumnNumber() {
		return columnNumber;
	}
}
