package net.tetrakoopa.bankoid.util.xml.fwk.exception.base;

public class AttributeMissingParseException extends ContractViolationParseException {

	public AttributeMissingParseException(String what, int lineNumber, int columnNumber) {
		super(explanation(what), lineNumber, columnNumber);
	}

	private static String explanation(String what) {
		return "Expected an attribute '"+what+"', found none";
	}
}
