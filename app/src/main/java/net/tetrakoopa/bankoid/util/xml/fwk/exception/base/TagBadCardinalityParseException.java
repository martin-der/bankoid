package net.tetrakoopa.bankoid.util.xml.fwk.exception.base;

public class TagBadCardinalityParseException extends ContractViolationParseException {

	public enum Type {
		EXPECTED_ONE_FOUND_MANY,
		EXPECTED_ONE_FOUND_NONE,
		EXPECTED_ONE_OR_MORE_FOUND_NONE
	}

	public TagBadCardinalityParseException(String what, Type type, int lineNumber, int columnNumber) {
		super(explanation(what, type), lineNumber, columnNumber);
	}

	private static String explanation(String what, Type type) {
		switch (type) {
			case EXPECTED_ONE_FOUND_NONE:
				return "Expected one '"+what+"' found none";
			case EXPECTED_ONE_FOUND_MANY:
			return "Expected one '"+what+"' found many";
			case EXPECTED_ONE_OR_MORE_FOUND_NONE:
				return "Expected one or more '"+what+"' found many";
			default: return "???"+type.name()+"???";
		}
	}
}
