package net.tetrakoopa.bankoid.util;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class SingletonViewModelFactory<M extends ViewModel> extends ViewModelProvider.NewInstanceFactory {

	private final M myViewModel;
	private final Class<M> myViewModelClass;

	private final Map<Class<? extends ViewModel>, ViewModel> mFactory = new HashMap<>();

	public SingletonViewModelFactory(@NonNull M myViewModel, Class<M> vmClass) {
		this.myViewModel = myViewModel;
		this.myViewModelClass = vmClass;
	}

	@NonNull
	@Override
	public <T extends ViewModel> T create(final @NonNull Class<T> modelClass) {
		mFactory.put(modelClass, myViewModel);

		if (myViewModelClass.isAssignableFrom(modelClass)) {
			M shareVM = null;

			if (mFactory.containsKey(modelClass)) {
				shareVM = (M)mFactory.get(modelClass);
			} else {
				try {
					shareVM = (M) modelClass.getConstructor(Runnable.class).newInstance(new Runnable() {
						@Override
						public void run() {
							mFactory.remove(modelClass);
						}
					});
				} catch (NoSuchMethodException e) {
					throw new RuntimeException("Cannot create an instance of " + modelClass, e);
				} catch (IllegalAccessException e) {
					throw new RuntimeException("Cannot create an instance of " + modelClass, e);
				} catch (InstantiationException e) {
					throw new RuntimeException("Cannot create an instance of " + modelClass, e);
				} catch (InvocationTargetException e) {
					throw new RuntimeException("Cannot create an instance of " + modelClass, e);
				}
				mFactory.put(modelClass, shareVM);
			}

			return (T) shareVM;
		}
		return super.create(modelClass);
	}

}