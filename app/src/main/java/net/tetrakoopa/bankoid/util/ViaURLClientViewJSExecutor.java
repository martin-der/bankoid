package net.tetrakoopa.bankoid.util;

import android.webkit.WebView;

import net.tetrakoopa.bankoid.backend.util.ClientViewJSExecutor;

public class ViaURLClientViewJSExecutor implements ClientViewJSExecutor {

	private final WebView webView;

	public ViaURLClientViewJSExecutor(WebView webView) {
		this.webView = webView;
	}

	@Override
	public Object executeForThis(String thisObject, String functionName, Object... parameters) {
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("javascript:try{");
		stringBuilder.append(functionName);
		stringBuilder.append(".call(");
		stringBuilder.append(thisObject==null?"null":thisObject).append(',');
		for (int i = 0; i < parameters.length; i++) {
			final Object parameter = parameters[i];
			if(parameter instanceof String){
				stringBuilder.append("'");
				stringBuilder.append(parameter.toString().replace("'", "\\'"));
				stringBuilder.append("'");
			}
			if(i < parameters.length - 1){
				stringBuilder.append(",");
			}
		}
		stringBuilder.append(")}catch(error){Android.onError(error.message, error.fileName != null ? error.fileName : '', error.lineNumber);}");
		webView.post(() -> webView.loadUrl(stringBuilder.toString()));

		return null;
	}

	@Override
	public Object execute(String functionName, Object... parameters) {
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("javascript:try{");
		stringBuilder.append(functionName);
		stringBuilder.append("(");
		for (int i = 0; i < parameters.length; i++) {
			final Object parameter = parameters[i];
			if(parameter instanceof String){
				stringBuilder.append("'");
				stringBuilder.append(parameter.toString().replace("'", "\\'"));
				stringBuilder.append("'");
			}
			if(i < parameters.length - 1){
				stringBuilder.append(",");
			}
		}
		stringBuilder.append(")}catch(error){Android.onError('calling \\'"+functionName+"\\': '+error.message, error.fileName != null ? error.fileName : '', error.lineNumber);}");
		webView.post(() -> webView.loadUrl(stringBuilder.toString()));

		return null;
	}
}
