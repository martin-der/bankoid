package net.tetrakoopa.bankoid.util.xml.fwk.exception.base;

public class InvalidAttributeValueParseException extends ContractViolationParseException {

	public InvalidAttributeValueParseException(String what, String value, int lineNumber, int columnNumber) {
		this(what, value, lineNumber, columnNumber, (String)null);
	}
	public InvalidAttributeValueParseException(String what, String value, int lineNumber, int columnNumber, Throwable cause) {
		super(explanation(what, value, cause), lineNumber, columnNumber);
	}
	public InvalidAttributeValueParseException(String what, String value, int lineNumber, int columnNumber, String detail) {
		super(explanation(what, value, detail), lineNumber, columnNumber);
	}

	private static String explanation(String what, String value, Throwable cause) {
		return (cause == null)
				? explanation(what, value, (String)null)
				: explanation(what, value, cause.getClass().getName()+":"+cause.getMessage());
	}
	private static String explanation(String what, String value, String detail) {
		final String explanation = "Illegal value '"+value+"' for attribute '"+what+"'";
		return detail!=null
				? explanation+": "+detail
				: explanation;
	}
}
