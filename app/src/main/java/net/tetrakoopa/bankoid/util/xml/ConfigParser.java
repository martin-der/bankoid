package net.tetrakoopa.bankoid.util.xml;

import net.tetrakoopa.bankoid.backend.BackendHandler;
import net.tetrakoopa.bankoid.model.bank.Config;
import net.tetrakoopa.bankoid.model.bank.Settings;
import net.tetrakoopa.bankoid.model.common.Constraint;
import net.tetrakoopa.bankoid.backend.ui.model.AccountWebViewModel;
import net.tetrakoopa.bankoid.util.xml.config.SettingsParser;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.TagBadCardinalityParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.InvalidTextContentParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;
import net.tetrakoopa.bankoid.util.xml.fwk.AbstractParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConfigParser extends AbstractParser<Config> {

	private final static String TAG_CONFIG = "config";

	private final static String TAG_CONFIG_CLASS = "class";
	private final static String TAG_CONFIG_SETTINGS = "settings";
	private final static String TAG_CONFIG_ACCOUNT = "account";

	private final static String TAG_IDENTIFIER = "identifier";

	private final static String TAG_IDENTIFIER_CONSTRAINT = "constraint";

	@Override
	public String requiredTag() {
		return TAG_CONFIG;
	}

	private final Settings referenceSettings;

	private final ConstraintParser constraintParser;

	public ConfigParser(Settings referenceSettings) {
		this.constraintParser = new ConstraintParser();
		this.referenceSettings = referenceSettings;
	}

	public Config parse(XmlPullParser parser) throws ParseException, IOException, XmlPullParserException {

		Config.Clazz clazz = null;
		Settings settings = null;
		final List<Config.Account> accounts = new ArrayList<>();

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String tagName = parser.getName();

			if (tagName.equals(TAG_CONFIG_CLASS)) {
				if (clazz != null)
					throw new TagBadCardinalityParseException(TAG_CONFIG_CLASS, TagBadCardinalityParseException.Type.EXPECTED_ONE_FOUND_MANY, parser.getLineNumber(), parser.getColumnNumber());
				clazz = parseClass(parser);
			} else if (tagName.equals(TAG_CONFIG_SETTINGS)) {
				if (settings != null)
					throw new TagBadCardinalityParseException(TAG_CONFIG_SETTINGS, TagBadCardinalityParseException.Type.EXPECTED_ONE_FOUND_MANY, parser.getLineNumber(), parser.getColumnNumber());
				settings = parseSettings(parser);
			} else if (tagName.equals(TAG_CONFIG_ACCOUNT)) {
				Config.Account account = parseAccount(parser);
				accounts.add(account);
			} else {
				skip(parser);
			}
		}

		final Config config = new Config(clazz, settings == null ? new Settings() : settings);
		config.getAccounts().addAll(accounts);

		return config;
	}

	private Settings parseSettings(XmlPullParser parser) throws ParseException, XmlPullParserException, IOException {
		return new SettingsParser(referenceSettings).parse(parser);
	}

	private Config.Account parseAccount(XmlPullParser parser) throws IOException, XmlPullParserException, ParseException {
		Config.Account.Identifier indentifier = null;

		final String type = parser.getAttributeValue(null, "type");

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String tagName = parser.getName();

			if (tagName.equals(TAG_IDENTIFIER)) {
				if (indentifier != null)
					throw new TagBadCardinalityParseException(TAG_IDENTIFIER, TagBadCardinalityParseException.Type.EXPECTED_ONE_FOUND_MANY, parser.getLineNumber(), parser.getColumnNumber());
				indentifier = parseAccountIdentifier(parser);
			} else {
				skip(parser);
			}
		}

		return new Config.Account(type, indentifier);
	}

	private Config.Account.Identifier parseAccountIdentifier(XmlPullParser parser) throws ParseException, IOException, XmlPullParserException {
		final List<Constraint> constraints = new ArrayList<>();

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String tagName = parser.getName();

			if (tagName.equals(TAG_IDENTIFIER_CONSTRAINT)) {
				constraints.add(constraintParser.parse(parser));
			} else {
				skip(parser);
			}
		}

		return new Config.Account.Identifier(constraints.toArray(new Constraint[constraints.size()]));
	}

	private Config.Clazz parseClass(XmlPullParser parser) throws ParseException, IOException, XmlPullParserException {

		String handler = null;
		String model = null;

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			final String tagName = parser.getName();

			if (tagName.equals("handler")) {
				if (handler != null)
					throw new TagBadCardinalityParseException("handler", TagBadCardinalityParseException.Type.EXPECTED_ONE_FOUND_MANY, parser.getLineNumber(), parser.getColumnNumber());
				handler = readText(parser);
			} else if (tagName.equals("model")) {
				if (model != null)
					throw new TagBadCardinalityParseException("model", TagBadCardinalityParseException.Type.EXPECTED_ONE_FOUND_MANY, parser.getLineNumber(), parser.getColumnNumber());
				model = readText(parser);
			} else {
				skip(parser);
			}
		}

		if (model == null) throw new TagBadCardinalityParseException("", TagBadCardinalityParseException.Type.EXPECTED_ONE_FOUND_NONE, parser.getLineNumber(), parser.getColumnNumber());
		if (handler == null) throw new TagBadCardinalityParseException("", TagBadCardinalityParseException.Type.EXPECTED_ONE_FOUND_NONE, parser.getLineNumber(), parser.getColumnNumber());

		final Class<? extends AccountWebViewModel> modelClass;
		try {
			modelClass = (Class<? extends AccountWebViewModel>) Class.forName(model);
		} catch (ClassNotFoundException|ClassCastException ex) {
			throw new InvalidTextContentParseException("Not a valid model class", parser.getLineNumber(), parser.getColumnNumber(), ex);
		}
		final Class<? extends BackendHandler<?>> handlerClass;
		try {
			handlerClass = (Class<? extends BackendHandler<?>>) Class.forName(handler);
		} catch (ClassNotFoundException|ClassCastException ex) {
			throw new InvalidTextContentParseException("Not a valid handler class", parser.getLineNumber(), parser.getColumnNumber(), ex);
		}
		return new Config.Clazz(handlerClass, modelClass);

	}

}
