package net.tetrakoopa.bankoid.util;

import java.util.HashMap;
import java.util.Map;

public class JavaLangUtil {

	private final static Map<Class,Class> primitiveClassesToBoxed = new HashMap<>();
	
	static {
		primitiveClassesToBoxed.put(int.class, Integer.class );
		primitiveClassesToBoxed.put(long.class, Long.class );
		primitiveClassesToBoxed.put(double.class, Double.class );
		primitiveClassesToBoxed.put(float.class, Float.class );
		primitiveClassesToBoxed.put(boolean.class, Boolean.class );
		primitiveClassesToBoxed.put(char.class, Character.class );
		primitiveClassesToBoxed.put(byte.class, Byte.class );
		primitiveClassesToBoxed.put(void.class, Void.class );
		primitiveClassesToBoxed.put(short.class, Short.class );
	}
	
	public static Class<?> boxedClass(Class<?> primitiveClass) {
		if (primitiveClass == null) new NullPointerException("primitiveClass cannot be null");
		final Class<?> boxed = primitiveClassesToBoxed.get(primitiveClass);
		if (boxed == null) throw new IllegalArgumentException("Class "+primitiveClass.getName()+" is not a primitive class");
		return boxed;
	}

	public static String parametersTypeAsString(Class<?>... parameters) {
		final StringBuilder builder = new StringBuilder();
		builder.append("[");
		boolean first = true;
		for (Class<?> parameter : parameters) {
			if (first) first=false;
			else builder.append(',');
			builder.append(parameter.getName());
		}
		builder.append("]");
		return builder.toString();
	}

	public static String exceptionToText(Throwable exception) {
		final StringBuilder message = new StringBuilder();
		boolean first = true;
		do {
			if (first) first = false;
			else message.append("caused by ");

			message.append(exception.getClass().getName());
			if (exception.getMessage()!=null) message.append(": ").append(exception.getMessage());
			message.append('\n');

			for (final StackTraceElement element : exception.getStackTrace()) {
				message.append("\tat ")
						.append(element.getClassName()+":"+element.getLineNumber()+" ")
						.append(element.getMethodName())
						.append('\n');
			}

			exception = exception.getCause();
		} while (exception !=null);
		return message.toString();
	}

}
