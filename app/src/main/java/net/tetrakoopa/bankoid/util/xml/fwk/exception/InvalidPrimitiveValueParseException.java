package net.tetrakoopa.bankoid.util.xml.fwk.exception;

import net.tetrakoopa.bankoid.model.common.Primitive;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ContractViolationParseException;

public class InvalidPrimitiveValueParseException extends ContractViolationParseException {

	public InvalidPrimitiveValueParseException(Primitive.Type type, String value, int lineNumber, int columnNumber) {
		super(buildMessage(type, value), lineNumber, columnNumber);
	}
	public InvalidPrimitiveValueParseException(Primitive.Type type, String value, int lineNumber, int columnNumber, Exception cause) {
		super(buildMessage(type, value), lineNumber, columnNumber, cause);
	}

	private static String buildMessage(Primitive.Type type, String string) {
		return "'"+string+"' is not a valid value for "+type.name();
	}

}
