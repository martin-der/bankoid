package net.tetrakoopa.bankoid.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class AssetsReader {

	/**
	 * @return <code>null</code> if not found
	 * @throws IOException
	 */
	public static Drawable getDrawableFromAssets(Context context, String path) throws IOException {
		InputStream inputStream = context.getAssets().open(path);
		if (inputStream == null) throw new FileNotFoundException("No such asset '"+path+"'");
		try {
			final Drawable drawable = Drawable.createFromStream(inputStream, null);
			if (drawable == null) throw new RuntimeException("Invalid drawable file '"+path+"");
			return drawable;
		} finally {
			inputStream.close();
		}
	}

	public static class DisplayMetricsHelper {
		public static int dpToPx(int dp) {
			return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
		}
	}

	public static Drawable getDrawableFromSVGAssets(Context context, String path, int width, int height, Integer backgroundColor) throws IOException {
		final SVG svg;
		try {
			svg = SVG.getFromAsset(context.getAssets(), path);
		} catch (SVGParseException e) {
			throw new RuntimeException("Invalid SVG file '"+path+"");
		}

		float documentWidth = svg.getDocumentWidth();
		float documentHeight = svg.getDocumentHeight();
		if (documentWidth == -1) {
			documentWidth = width;
			documentHeight = height;
		}
		if (documentWidth != -1) {
			Bitmap newBM = Bitmap.createBitmap((int) Math.ceil(documentWidth),
					(int) Math.ceil(documentHeight),
					Bitmap.Config.ARGB_8888);
			final Canvas canvas = new Canvas(newBM);
			if (backgroundColor != null) {
				canvas.drawRGB((backgroundColor & 0xFF0000) >> 16, (backgroundColor & 0xFF00) >> 8, backgroundColor & 0xFF);
			}
			svg.renderToCanvas(canvas);
			return new BitmapDrawable(newBM);
		}

		throw new RuntimeException("Unsupported SVG file '"+path+"");
	}
}