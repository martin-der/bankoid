package net.tetrakoopa.bankoid.util;

import android.content.SharedPreferences;

import net.tetrakoopa.bankoid.ApplicationDataHelper;
import net.tetrakoopa.bankoid.Oops;
import net.tetrakoopa.bankoid.model.common.Primitive;
import net.tetrakoopa.bankoid.util.xml.common.PrimitiveParser;

public class PreferenceHelper {

	public static void putValue(SharedPreferences.Editor editor, String key, Primitive.Type type, Object value) {
		if (value == null) {
			editor.remove(key);
			return;
		}
		switch (type) {
			case BOOLEAN:
				editor.putBoolean(key, (boolean)value);
				return;
			case INTEGER:
				editor.putInt(key, (int)value);
				return;
			case FLOAT:
				editor.putFloat(key, (float)value);
				return;
			case STRING:
				editor.putString(key, (String)value);
				return;
			case ENUMERATION:
				final Enum enumValue = (Enum)value;
				final String enumKey = DeclaredValue.DeclaredEnum.Util.getEnumKey(enumValue);
				editor.putString(key, enumKey == null ? enumValue.name() : enumKey);
				return;
		}
	}

	public static void putValue(SharedPreferences.Editor editor, String key, DeclaredValue.Default defaultz) throws PrimitiveParser.InvalidPrimitiveValueException {
		putValue(editor, key, defaultz.type(), DeclaredValue.Helper.getValue(defaultz));
	}

	public static boolean putValueOrLog(SharedPreferences.Editor editor, String key, DeclaredValue.Default defaultz) {
		try {
			putValue(editor, key, defaultz);
		} catch (PrimitiveParser.InvalidPrimitiveValueException ex) {
			Oops.ERROR.log(ApplicationDataHelper.class, "Unable to set preference '"+key+"': "+ex.getMessage(), ex);
			return false;
		}
		return true;
	}
}