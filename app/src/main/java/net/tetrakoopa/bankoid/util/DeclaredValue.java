package net.tetrakoopa.bankoid.util;

import android.annotation.SuppressLint;

import net.tetrakoopa.bankoid.model.common.Primitive;
import net.tetrakoopa.bankoid.util.xml.common.PrimitiveParser;

import org.springframework.core.GenericTypeResolver;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;

@SuppressLint("NewApi")
public interface DeclaredValue {

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	@interface Key {

		/**
		 * Enum key naming conventions
		 * If set on the enum then all value will use the specified key convention
		 */
		interface Enum {
			/**
			 * The enum value to lowercase
			 */
			@Retention(RetentionPolicy.RUNTIME)
			@Target({ElementType.FIELD, ElementType.TYPE})
			@interface LowerCase {}

			/** The enum value to uppercase */
			@Retention(RetentionPolicy.RUNTIME)
			@Target({ElementType.FIELD, ElementType.TYPE})
			@interface UpperCase {}
		}

		/** The attribute to lowercase */
		@Retention(RetentionPolicy.RUNTIME)
		@Target(ElementType.FIELD)
		@interface LowerCase {}

		/** The attribute to uppercase */
		@Retention(RetentionPolicy.RUNTIME)
		@Target(ElementType.FIELD)
		@interface UpperCase {}

		String value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	@interface Type {
		Primitive.Type value();
		Class<? extends Enum> enumClass() default Enum.class;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	@interface Default {
		String value();
		Primitive.Type type();
		Class<? extends Enum> enumClass() default Enum.class;
	}


	class Helper {

		public static <V> V getValue(Default defaultz) throws PrimitiveParser.InvalidPrimitiveValueException {
			if (defaultz == null) return null;
			if (defaultz.type() == Primitive.Type.ENUMERATION) {
				if (defaultz.enumClass().equals(Enum.class)) throw new PrimitiveParser.InvalidPrimitiveValueException(Primitive.Type.ENUMERATION, "Expected @Default.enumClass()");
				final int ordinal = (int)PrimitiveParser.deserializeValueForPrimitive(new Primitive(defaultz.enumClass()), defaultz.value());
				return (V) defaultz.enumClass().getEnumConstants()[ordinal];
			}
			return (V) PrimitiveParser.deserializeValueForPrimitive(new Primitive(defaultz.type()), defaultz.value());
		}
	}

	interface DeclaredEnum<E extends Enum<E>> {

		Util.InterfaceGenericTypeResolver<DeclaredEnum> __genericInterfaceGenericTypeResolver = new Util.InterfaceGenericTypeResolver<>(DeclaredEnum.class);

		class InvalidEnumKeyException extends EnumUtil.InvalidEnumCriteriaException {
			public InvalidEnumKeyException(Class<? extends Enum<?>> enumClass, String key) {
				super(enumClass, "key", key);
			}
		}

		default String getKey() {
			return Util.getEnumKey((E)this);
		}

//		default E fromKey(String key) throws InvalidEnumKeyException {
//			final Class<E> enumClass = (Class<E>) __genericInterfaceGenericTypeResolver.get(this.getClass())[0];
//			return Util.getEnumFromKey(enumClass, key);
//		}

		class Util {

			private static class InterfaceGenericTypeResolver<I> {

				private final Class<?> interfaceClass;

				private final boolean mandatory;

				private Class<?>[] types;

				public InterfaceGenericTypeResolver(Class<I> interfaceClass) {
					this(interfaceClass, true);
				}
				public InterfaceGenericTypeResolver(Class<I> interfaceClass, boolean mandatory) {
					this.interfaceClass = interfaceClass;
					this.mandatory = mandatory;
				}

				public <C extends I> Class<?>[] get(Class<C> childClass) {
					if (types == null) {
						if (types == null) {
							throw new IllegalStateException("No generic type provided for "+interfaceClass.getName());
						}
						types = GenericTypeResolver.resolveTypeArguments(childClass, interfaceClass);
					}
					return types;
				}
			}

			public static <E extends Enum<E>> String getEnumKey(E e) {
				final Key key = EnumUtil.getField(e).getAnnotation(Key.class);
				if (key != null) return key.value();

				// Search first for annotation on the enum value

				final Field enumField = EnumUtil.getField(e);

				if(enumField.getAnnotation(Key.Enum.LowerCase.class) != null || enumField.getAnnotation(Key.LowerCase.class) != null)
					return e.name().toLowerCase();

				if(enumField.getAnnotation(Key.Enum.UpperCase.class) != null || enumField.getAnnotation(Key.UpperCase.class) != null)
					return e.name().toUpperCase();

				// Then search for annotations on the enum class

				final Class<?> enumClass = e.getClass();

				if(enumClass.getAnnotation(Key.Enum.LowerCase.class) != null)
					return e.name().toLowerCase();

				if(enumClass.getAnnotation(Key.Enum.UpperCase.class) != null)
					return e.name().toUpperCase();

				return null;
			}

			/** If no <code>@Key</code> (or  <code>Key.Enum.UpperCase</code>, etc.. ) information is found on enum or enum's values
			 *  then enum value key are assumed to be enum value <code>name()</code>
			 */
			public static <E extends Enum<E>> E getEnumFromKey(Class<E> enumClass, String key) throws InvalidEnumKeyException {
				for (E value : enumClass.getEnumConstants()) {
					String enumKey = getEnumKey(value);
					if (enumKey == null) enumKey = value.name();
					if (key.equals(enumKey)) return value;
				}
				throw new InvalidEnumKeyException(enumClass, key);
			}
		}
	}


}
