package net.tetrakoopa.bankoid.util;

import net.tetrakoopa.bankoid.model.common.Constraint;

import java.util.regex.Pattern;

public class ContraintsDigester {

	public static class StringConstraint {
		public final Pattern regex;
		public final Integer min;
		public final Integer max;

		public StringConstraint(Pattern regex, Integer min, Integer max) {
			this.regex = regex;
			this.min = min;
			this.max = max;
		}
	}

	public static StringConstraint digestStringConstraint(Constraint[] constraints) {
		boolean regexConstraintDone = false;
		Pattern regex = null;
		Integer min = null;
		Integer max = null;
		for (Constraint constraint : constraints) {
			switch (constraint.type) {
				case REGEX:
					if (!regexConstraintDone) {
						regexConstraintDone = true;
						regex = Pattern.compile(constraint.regex);
					}
					break;
				case MIN:
					if (min != null)
						min = constraint.limit;
					break;
				case MAX:
					if (max != null)
						max = constraint.limit;
					break;
			}

		}
		return new StringConstraint(regex, min, max);
	}
}
