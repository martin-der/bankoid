package net.tetrakoopa.bankoid.util.xml.fwk;

import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public abstract class CommonParser<E> {

	public abstract E parse(XmlPullParser parser) throws ParseException, IOException, XmlPullParserException;

	protected void skip(XmlPullParser parser) throws IOException, XmlPullParserException {
		XmlParserHelper.skip(parser);
	}

	protected void skipRemaining(XmlPullParser parser) throws IOException, XmlPullParserException {
		XmlParserHelper.skipRemaining(parser);
	}

	protected String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
		return XmlParserHelper.readText(parser);
	}

}
