package net.tetrakoopa.bankoid.util.xml.fwk.exception.base;

import org.xmlpull.v1.XmlPullParserException;

public class XmlParseException extends ParseException {

	private final XmlPullParserException cause;

	public XmlParseException(XmlPullParserException cause) {
		super(cause);
		this.cause = cause;
	}
	public XmlParseException(String message, XmlPullParserException cause) {
		super(message, cause);
		this.cause = cause;
	}

	public int getLineNumber() {
		return cause.getLineNumber();
	}

	public int getColumnNumber() {
		return cause.getColumnNumber();
	}
}
