package net.tetrakoopa.bankoid.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.Html;
import android.text.Spanned;
import android.util.TypedValue;

import java.util.Locale;

import androidx.annotation.ColorInt;

public class ResourceHelper {

	public interface Consumer {

		Context getResourcesContext();

		default String resourceText(int id, Object ... params) {
			return ResourceHelper.text(getResourcesContext(), id, params);
		}

		default Spanned resourceTextAsHtml(int id, Object ... params) {
			return ResourceHelper.textAsHtml(getResourcesContext(), id, params);
		}

		default String resourceTextOrRaw(String name, Object ... params) {
			return ResourceHelper.textOrRaw(getResourcesContext(), name, params);
		}
		default Spanned resourceTextOrRawAsHtml(String name, Object ... params) {
			return ResourceHelper.textOrRawAsHtml(getResourcesContext(), name, params);
		}
		default String resourceText(String name, Object ... params) {
			return ResourceHelper.text(getResourcesContext(), name, params);
		}
		default Spanned resourceTextAsHtml(String name, Object ... params) {
			return ResourceHelper.textAsHtml(getResourcesContext(), name, params);
		}
	}

	private static final String STRING_RESOURCE_PREFIX = "@string/";

	public static @ColorInt int getColor(Resources.Theme theme, int id) {
		final TypedValue typedValue = new TypedValue();
		theme.resolveAttribute(id, typedValue, true);
		return typedValue.data;
	}

	public static String text(Context context, int id, Object ... params) {
		return context.getString(id, params);
	}
	public static Spanned textAsHtml(Context context, int id, Object ... params) {
		return Html.fromHtml(text(context, id, params), Html.FROM_HTML_MODE_COMPACT);
	}

	/**
	 * @return the value of the string resource named <code>name</code> if it starts with <code>@string/</code>, otherwise the string <code>name</code> as is.
	 */
	public static String textOrRaw(Context context, String name, Object ... params) {
		return name.startsWith(STRING_RESOURCE_PREFIX)
				? text(context, name, params)
				: name;
	}
	public static Spanned textOrRawAsHtml(Context context, String name, Object ... params) {
		return Html.fromHtml(text(context, name, params), Html.FROM_HTML_MODE_COMPACT);
	}
	public static String text(Context context, String name, Object ... params) {
		final String packageName = context.getPackageName();
		final int resId = context.getResources().getIdentifier(name, "string", packageName);
		if (resId == 0)
			throw new IllegalArgumentException("No string resource named '"+name+"'");
		return text(context, resId, params);
	}
	public static Spanned textAsHtml(Context context, String name, Object ... params) {
		return Html.fromHtml(text(context, name, params), Html.FROM_HTML_MODE_COMPACT);
	}

	public static class Original {

		private static Context getConfigurationContext(Context context) {
			final Configuration config = new Configuration(context.getResources().getConfiguration());
			config.setLocale(Locale.US);
			final Context configurationContext = context.createConfigurationContext(config);
			return configurationContext;
		}

		public static String text(Context context, int id) {
			final Context configurationContext = getConfigurationContext(context);
			final CharSequence chars = configurationContext.getText(id);
			return chars == null ? null : chars.toString();
		}

	}

}
