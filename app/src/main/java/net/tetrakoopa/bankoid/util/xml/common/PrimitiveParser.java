package net.tetrakoopa.bankoid.util.xml.common;

import android.annotation.SuppressLint;

import net.tetrakoopa.bankoid.model.common.Primitive;
import net.tetrakoopa.bankoid.util.EnumUtil;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import org.xmlpull.v1.XmlPullParser;

public class PrimitiveParser {

	public static class TypeExtraInformation {
		public String[] enumerationValues;
		public String enumerationReference;
	}

	public static class InvalidPrimitiveException extends Exception {

		public InvalidPrimitiveException(String message) {
			super(message);
		}

		public InvalidPrimitiveException(String message, Exception cause) {
			super(message, cause);
		}
	}
	public static class InvalidPrimitiveTypeException extends InvalidPrimitiveException {

		public InvalidPrimitiveTypeException(String value) {
			super(buildMessage(value));
		}

		private static String buildMessage(String string) {
			return "'"+string+"' is not a valid Primitive type";
		}
	}

	public static class InvalidPrimitiveValueException extends InvalidPrimitiveException {

		public InvalidPrimitiveValueException(Primitive.Type type, String value) {
			super(buildMessage(type, value));
		}
		public InvalidPrimitiveValueException(Primitive.Type type, String value, Exception cause) {
			super(buildMessage(type, value), cause);
		}

		private static String buildMessage(Primitive.Type type, String string) {
			return "'"+string+"' is not a valid value for "+type.name();
		}

	}



	private static final String REGEX_ENUM_WITH_REFERENCE = "^enum *@ *[a-zA-Z]+([a-zA-Z01-9])* *$";
	private static final String REGEX_ENUM_WITH_VALUES = "^enum *@ *\\[ *([a-zA-Z]+([a-zA-Z01-9])*( *, *[a-zA-Z]+([a-zA-Z01-9])*)+) *\\]$";

	private static void validateValueClassForType(Primitive.Type type, Class<?> valueClass) {
		switch(type) {
			case STRING:
				if (valueClass.equals(String.class))
					return;
				break;
			case BOOLEAN:
				if (valueClass.equals(Boolean.class))
					return;
				break;
			case INTEGER:
				if (valueClass.equals(Integer.class))
					return;
				break;
			case FLOAT:
				if (valueClass.equals(Float.class))
					return;
				break;
			case ENUMERATION:
				if (valueClass.equals(Integer.class))
					return;
				break;
			default:
				throw new InternalError("Unimplemented case "+type.name());
		}
		throw new IllegalArgumentException(valueClass.getName()+" is not a valid value for "+type.name());
	}

	@SuppressLint("NewApi")
	public static Primitive.Type deserializeType(String string, TypeExtraInformation extra) throws EnumUtil.InvalidEnumCriteriaException.Name {
		if (string == null) return null;
		string = string.toLowerCase();
		if (Pattern.matches(REGEX_ENUM_WITH_REFERENCE, string)) {
			if (extra != null) {
				extra.enumerationReference = string.substring(string.indexOf('@')+1).trim();
			}
			return Primitive.Type.ENUMERATION;
		}
		final Matcher matcherEnumWithValues = Pattern.compile(REGEX_ENUM_WITH_VALUES).matcher(string);
		if (matcherEnumWithValues.matches()) {
			if (extra != null) {
				extra.enumerationValues = Arrays.stream(matcherEnumWithValues.group(1).split(",")).map(s -> s.trim()).toArray(String[]::new);
			}
			return Primitive.Type.ENUMERATION;
		}
		return Primitive.Type.fromNameIgnoreCase(string);
	}

	public static Primitive deserializePrimitive(String string) throws InvalidPrimitiveTypeException {
		final TypeExtraInformation extra = new TypeExtraInformation();
		final Primitive.Type type;
		try {
			type = deserializeType(string, extra);
		} catch (EnumUtil.InvalidEnumCriteriaException.Name ex) {
			throw new InvalidPrimitiveTypeException(string);
		}
		if (type == Primitive.Type.ENUMERATION) {
			if (extra.enumerationValues != null)
				return new Primitive(extra.enumerationValues);
			if (extra.enumerationReference != null)
				throw new IllegalStateException("Not implemented : enum declaration with reference");
			throw new InternalError("Missing enum value informations / Not implemented");
		}
		return new Primitive(type);
	}

	public static Object deserializeValueForPrimitive(Primitive primitive, String string) throws InvalidPrimitiveValueException {
		if (primitive == null || string == null) throw new IllegalArgumentException("type must be provided");
		if (string == null) return null;
		final Primitive.Type type = primitive.type;
		switch (type) {
			case STRING: return string;
			case INTEGER:
				try {
					return Integer.parseInt(string);
				} catch (NumberFormatException nfex) {
					throw new InvalidPrimitiveValueException(type, string, nfex);
				}
			case FLOAT:
				try {
					return Float.parseFloat(string);
				} catch (NumberFormatException nfex) {
					throw new InvalidPrimitiveValueException(type, string, nfex);
				}
			case BOOLEAN:
				string = string.toLowerCase();
				if (string.equals("true")) return true;
				if (string.equals("false")) return false;
				throw new InvalidPrimitiveValueException(type, string);
			case ENUMERATION:
				try {
					return primitive.ordinal(string);
				} catch (RuntimeException rex) {
					throw new InvalidPrimitiveValueException(type, string, rex);
				}
			default:
				throw new InternalError("Unimplemented case "+type.name());
		}
	}


}
