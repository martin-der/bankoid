package net.tetrakoopa.bankoid.util;

public interface Continuation {
	void then(boolean success);
}
