package net.tetrakoopa.bankoid.util.xml.fwk.exception.base;

public class InvalidTextContentParseException extends ContractViolationParseException {

	public InvalidTextContentParseException(String message, int lineNumber, int columnNumber) {
		super(message, lineNumber, columnNumber);
	}

	public InvalidTextContentParseException(String message, int lineNumber, int columnNumber, Throwable ex) {
		super(message, lineNumber, columnNumber, ex);
	}
}
