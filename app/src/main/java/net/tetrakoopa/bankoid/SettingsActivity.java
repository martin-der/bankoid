package net.tetrakoopa.bankoid;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.util.TypedValue;
import android.view.MenuItem;
import android.widget.Toast;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.custom.AfterTextChangedTextWatcher;
import net.tetrakoopa.bankoid.model.AccountContent;
import net.tetrakoopa.bankoid.model.bank.Settings;
import net.tetrakoopa.bankoid.model.common.AndroidPreferenceI18NText;
import net.tetrakoopa.bankoid.model.common.Constraint;
import net.tetrakoopa.bankoid.model.common.Primitive;
import net.tetrakoopa.bankoid.service.persistence.AccountPersistenceService;
import net.tetrakoopa.bankoid.service.persistence.CRUD;
import net.tetrakoopa.bankoid.ui.ApplicationTheme;
import net.tetrakoopa.bankoid.ui.CommonActivity;
import net.tetrakoopa.bankoid.ui.dialog.DialogBuilder;
import net.tetrakoopa.bankoid.util.ContraintsDigester;
import net.tetrakoopa.bankoid.util.DeclaredValue;
import net.tetrakoopa.bankoid.util.EnumUtil;
import net.tetrakoopa.bankoid.util.ResourceHelper;
import net.tetrakoopa.bankoid.backend.BackendHandler;
import net.tetrakoopa.bankoid.backend.PreferencesKey;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;
import net.tetrakoopa.bankoid.backend.ui.model.AccountWebViewModel;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.preference.PreferenceScreen;
import androidx.preference.SeekBarPreference;
import androidx.preference.SwitchPreferenceCompat;
import androidx.preference.TwoStatePreference;

public class SettingsActivity extends CommonActivity {

    public interface Group extends DeclaredValue {
        @Key(SettingsActivity.Key.ENABLE_WEB_UI_TWEAKS)
        String UI = "ui";
    }
    public interface Key extends DeclaredValue {

        @Default(type = Primitive.Type.ENUMERATION, value = "LIGHT", enumClass = ApplicationTheme.class)
        String APPLICATION_MAIN_THEME = "application_main_theme";

        @Default(type = Primitive.Type.ENUMERATION, value = "NONE", enumClass = AccountSelectionWhenStarting.class)
        String ACCOUNT_SELECTION_WHEN_STARTING = "account_selection_at_start";

         enum AccountSelectionWhenStarting implements DeclaredEnum<AccountSelectionWhenStarting> {

            NONE, PREVIOUS, PICK;

            public static AccountSelectionWhenStarting fromName(String name) throws EnumUtil.InvalidEnumCriteriaException.Name { return EnumUtil.fromName(AccountSelectionWhenStarting.class, name); }

            public static AccountSelectionWhenStarting fromKey(String key) throws InvalidEnumKeyException { return Util.getEnumFromKey(AccountSelectionWhenStarting.class, key); }
        }

        @Type(Primitive.Type.STRING)
        String ACCOUNT_SELECTED_BY_PICKING = "account_selected_by_picking";
        @Type(Primitive.Type.STRING)
        String ACCOUNT_SELECTED_PREVIOUSLY = "account_selected_previously";

        @Default(type = Primitive.Type.BOOLEAN, value = "false")
        String ENABLE_DEBUG = "enable_debug";
        @Default(type = Primitive.Type.BOOLEAN, value = "true")
        String USE_JAVA_LOGS = "use_java_logs";
        @Default(type = Primitive.Type.BOOLEAN, value = "true")
        String USE_JAVASCRIPT_LOGS = "use_javascript_logs";
        @Default(type = Primitive.Type.BOOLEAN, value = "false")
        String ALLOW_SCREENSHOT = "allow_screenshot";

        @Default(type = Primitive.Type.BOOLEAN, value = "false")
        String ENABLE_WEB_UI_TWEAKS = "enable_web_ui_tweaks";
    }

    private AccountPersistenceService accountPersistenceService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        accountPersistenceService = new AccountPersistenceService(SettingsActivity.this);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment(accountPersistenceService))
                .commit();

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

     }

    public static class SettingsFragment extends PreferenceFragmentCompat implements ResourceHelper.Consumer {

        private final ApplicationData.OnAccountChangeListener accountChangeListener = account -> prepareBackendPreferences();

        private final AccountPersistenceService accountPersistenceService;

        @Override
        public Context getResourcesContext() {
            return getContext();
        }

        public SettingsFragment(AccountPersistenceService accountPersistenceService) {
            this.accountPersistenceService = accountPersistenceService;
        }


        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);

            final ListPreference selectedAccountWhenStarting = findPreference(Key.ACCOUNT_SELECTED_BY_PICKING);
            final ListPreference accountSelectionWhenStarting = findPreference(Key.ACCOUNT_SELECTION_WHEN_STARTING);
            accountSelectionWhenStarting.setOnPreferenceChangeListener((preference, newValue) -> {
                Key.AccountSelectionWhenStarting currentMode;

                try {
                    currentMode = Key.AccountSelectionWhenStarting.fromKey((String)newValue);
                } catch (DeclaredValue.DeclaredEnum.InvalidEnumKeyException e) {
                    logger.log(MessageLogger.LogMessage.Severity.WARNING, SettingsFragment.class, "Invalid account selection mode '"+(String)newValue+"'", e);
                    currentMode = Key.AccountSelectionWhenStarting.NONE;
                }

                final boolean pickMode = currentMode == Key.AccountSelectionWhenStarting.PICK;

                selectedAccountWhenStarting.setEnabled(pickMode);
                if (pickMode) {
                    //getPreferenceScreen().onItemClick( null, null, selectedAccountWhenStarting.getOrder(), 0 );
                }
                return true;
            });

            final Preference applicationTheme = findPreference(Key.APPLICATION_MAIN_THEME);
            applicationTheme.setOnPreferenceChangeListener((preference, newValue) -> {
                final String themeName = (String)newValue;
                final ApplicationTheme theme = ApplicationTheme.fromName(themeName, ApplicationData.DEFAULT_APPLICATION_THEME);
                applicationData.updateTheme(theme);
                final String themeLabel = themeAsText(getContext(), theme);
                preference.setSummary(themeAsText(getContext(), theme));
                DialogBuilder
                        .notification(getContext())
                        .addValidationButton(android.R.string.ok, null)
                        .setMessage("Theme changed to : "+themeLabel)
                        .setTitle("New face !")
                        .show();
                return true;
            });
            final ApplicationTheme currentTheme = applicationData.theme().getValue();
            applicationTheme.setSummary(themeAsText(getContext(), currentTheme));


            final String[] accountNames;
            try (CRUD.DBStream<AccountContent.Account> dbStream = accountPersistenceService.find()){
                accountNames = dbStream.data.map(a -> a.name).toArray(String[]::new);
            }
            selectedAccountWhenStarting.setEntries(accountNames);
            selectedAccountWhenStarting.setEntryValues(accountNames);
            selectedAccountWhenStarting.setEnabled(true);

            prepareBackendPreferences();
        }

        @Override
        public void onStart() {
            super.onStart();
            applicationData.registerOnAccountChangeListener(accountChangeListener);
        }
        @Override
        public void onStop() {
            super.onStop();
            applicationData.unregisterOnAccountChangeListener(accountChangeListener);
        }

        private void prepareBackendPreferences() {
            TypedValue themeTypedValue = new TypedValue();
            getActivity().getTheme().resolveAttribute(R.attr.preferenceTheme, themeTypedValue, true);
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getActivity(), themeTypedValue.resourceId);

            final PreferenceScreen preferenceScreen = getPreferenceScreen();

            final Preference reactivateAllTutorialInformationButton = findPreference("reactivate_all_tutorial_information");
            reactivateAllTutorialInformationButton.setOnPreferenceClickListener(preference -> {
                final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                final SharedPreferences.Editor editor = preferences.edit();
                for (Field field : TutorialConfig.class.getDeclaredFields()) {
                    final TutorialConfig.Key keyAnnotation = field.getAnnotation(TutorialConfig.Key.class);
                    final TutorialConfig.Default defaultAnnotation = field.getAnnotation(TutorialConfig.Default.class);
                    if (keyAnnotation != null) {
                        if (defaultAnnotation != null) {
                            final Primitive.Type type = defaultAnnotation.type();
                            PreferenceHelper.putValueOrLog(editor, keyAnnotation.value(), defaultAnnotation);
                        } else {
                            editor.remove(keyAnnotation.value());
                        }
                    }
                }
                editor.commit();

                Toast.makeText(getContext(), R.string.message_all_tutorial_information_reactivated, Toast.LENGTH_LONG).show();
                return true;
            });

            final ApplicationData applicationData = ApplicationData.instance();
            final PreferenceCategory preferenceCategory = (PreferenceCategory) preferenceScreen.getPreference(1);

            final ApplicationData.Backend<? extends AccountWebViewModel, ? extends BackendHandler<?>> backend = ApplicationData.instance().currentBackend();

            if (backend != null) {

                final Settings settings = applicationData.currentBackend().settingsTemplate;

                final boolean hasUiGroupSettings = settings.getEntries().entrySet().stream().anyMatch(e -> Arrays.stream(e.getValue().groups).anyMatch(Group.UI::equalsIgnoreCase));

                if (hasUiGroupSettings) {
                    SwitchPreferenceCompat checkBoxEnableUIPreferences = new SwitchPreferenceCompat(contextThemeWrapper);
                    checkBoxEnableUIPreferences.setKey(Key.ENABLE_WEB_UI_TWEAKS);
                    checkBoxEnableUIPreferences.setTitle(R.string.ui_use_web_ui_tweaks);
                    preferenceCategory.addPreference(checkBoxEnableUIPreferences);
                }

                for (Map.Entry<String, Settings.Setting> entry : settings.getEntries().entrySet()) {
                    final Settings.Setting setting = entry.getValue();
                    final String key = PreferencesKey.getForBackend(backend.name, entry.getKey());
                    final AndroidPreferenceI18NText text = setting.text;
                    final String title, summary, summaryOn, summaryOff;
                    if (text != null) {
                        title = (text.title != null) ? resourceTextOrRaw(text.title) : key;
                        summary = (text.summary != null) ? resourceTextOrRaw(text.summary) : null;
                        summaryOn = (text.summaryOn != null) ? resourceTextOrRaw(text.summaryOn) : null;
                        summaryOff = (text.summaryOff != null) ? resourceTextOrRaw(text.summaryOff) : null;
                    } else {
                        title = key;
                        summary = null;
                        summaryOn = null;
                        summaryOff = null;
                    }
                    final Preference preference;
                    switch (setting.primitive.type) {
                        case STRING: {
                            EditTextPreference editTextPreference = new EditTextPreference(contextThemeWrapper);
                            preference = editTextPreference;
                            final ContraintsDigester.StringConstraint constraint;
                            try {
                                constraint = ContraintsDigester.digestStringConstraint(setting.constraints);
                            } catch (RuntimeException rex) {
                                Oops.ERROR.log(SettingsActivity.class, "[Backend " + backend.name + "] invalid constraint: " + rex.getMessage(), rex);
                                break;
                            }
                            if (constraint.regex != null || constraint.min != null || constraint.max != null) {
                                editTextPreference.setOnBindEditTextListener(editText -> {
                                    editText.addTextChangedListener(new AfterTextChangedTextWatcher() {
                                        @Override
                                        public void afterTextChanged(Editable editable) {
                                            final String text = editable.toString();
                                            if (constraint.min != null && text.length() < constraint.min) {
                                                // TODO show validation problem
                                            }
                                            if (constraint.max != null && text.length() > constraint.max) {
                                                // TODO show validation problem
                                            }
                                            if (constraint.regex != null && !constraint.regex.matcher(text).matches()) {
                                                // TODO show validation problem
                                            }
                                        }
                                    });
                                });
                            }
                            break;
                        }
                        case BOOLEAN: {
                            SwitchPreferenceCompat checkBoxPreference = new SwitchPreferenceCompat(contextThemeWrapper);
                            preference = checkBoxPreference;
                            break;
                        }
                        case INTEGER: {
                            SeekBarPreference seekBarPreference = new SeekBarPreference(contextThemeWrapper);
                            preference = seekBarPreference;
                            boolean minConstraintDone = false;
                            boolean maxConstraintDone = false;
                            for (Constraint constraint : setting.constraints) {
                                switch (constraint.type) {
                                    case MIN:
                                        if (!minConstraintDone) {
                                            minConstraintDone = true;
                                            seekBarPreference.setMin(constraint.limit);
                                        }
                                        break;
                                    case MAX:
                                        if (!maxConstraintDone) {
                                            maxConstraintDone = true;
                                            seekBarPreference.setMax(constraint.limit);
                                        }
                                        break;
                                }
                            }
                            break;
                        }
                        case ENUMERATION: {
                            Preference eeeePreference = new SeekBarPreference(contextThemeWrapper);
                            preference = eeeePreference;
                            break;
                        }
                        default:
                            preference = null;
                    }
                    if (preference != null) {
                        preference.setKey(key);
                        preference.setTitle(title);
                        if (preference instanceof TwoStatePreference && summaryOff != null && summaryOn != null) {
                            final TwoStatePreference twoStatePreference = (TwoStatePreference)preference;
                            twoStatePreference.setSummaryOn(summaryOn);
                            twoStatePreference.setSummaryOff(summaryOff);
                        }
                        else if (summary != null)
                            preference.setSummary(summary);
                        preferenceCategory.addPreference(preference);
                        if (Arrays.stream(setting.groups).anyMatch(Group.UI::equalsIgnoreCase))
                            preference.setDependency(Key.ENABLE_WEB_UI_TWEAKS);
                    }
                }

                preferenceCategory.setVisible(true);
            } else {
                preferenceCategory.setVisible(false);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private static String themeAsText(Context context, ApplicationTheme theme) {
        return theme == null ? "" : ResourceHelper.text(context, "theme_VALUE_"+theme.name());
    }
}