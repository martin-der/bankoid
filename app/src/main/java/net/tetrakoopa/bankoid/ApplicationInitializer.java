package net.tetrakoopa.bankoid;

import android.content.Context;

import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;

import net.tetrakoopa.bankoid.debug.LogCapable;

import java.util.List;

import androidx.startup.Initializer;

import static java.util.Collections.emptyList;

public class ApplicationInitializer implements Initializer<Void>, LogCapable {

	@Override
	public Void create(Context context) {
		Iconify.with(new FontAwesomeModule());
		ApplicationData.instance().init(context);
		return null;
	}

	@Override
	public List<Class<? extends Initializer<?>>> dependencies() {
		return emptyList();
	}

}
