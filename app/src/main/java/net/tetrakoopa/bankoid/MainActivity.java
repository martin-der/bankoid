package net.tetrakoopa.bankoid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.ui.CommonActivity;
import net.tetrakoopa.bankoid.ui.account.AccountListActivity;
import net.tetrakoopa.bankoid.ui.debug.AccountWebViewDebugActivity;
import net.tetrakoopa.bankoid.ui.debug.LogMessageListActivity;
import net.tetrakoopa.bankoid.util.AssetsReader;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.preference.PreferenceManager;

public class MainActivity extends CommonActivity /*implements NavigationView.OnNavigationItemSelectedListener*/ {

	private AppBarConfiguration mAppBarConfiguration;

	private MenuItem menuActionAccounts;
	private MenuItem menuActionLogs;
	private MenuItem menuActionWebViewInfo;

	private SharedPreferences preferences;

	private ConnectivityManager connectivityManager;
	private final ConnectivityStatusReceiver connectivityStatusReceiver = new ConnectivityStatusReceiver();
	private SharedPreferences.OnSharedPreferenceChangeListener sharedPreferenceChangeListener;

	private final ApplicationData applicationData = ApplicationData.instance();
	private ApplicationData.OnAccountChangeListener accountChangeListener;

	public class ConnectivityStatusReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {


//			connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

			final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

			if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
			} else {
			}
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

		setContentView(R.layout.activity_main);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		DrawerLayout drawer = findViewById(R.id.drawer_layout);

		NavigationView navigationView = findViewById(R.id.nav_view);
		// Passing each menu ID as a set of Ids because each
		// menu should be considered as top level destinations.
		mAppBarConfiguration = new AppBarConfiguration.Builder(
				R.id.nav_account, R.id.nav_about)
				.setDrawerLayout(drawer)
				.build();

//		FragmentManager supportFragmentManager = getFragmentManager();
//		supportFragmentManager.saveFragmentInstanceState();
//		supportFragmentManager.findFragmentById(0);



		MenuItem menuManager = navigationView.getMenu().findItem(R.id.nav_account);
//		menuManager.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_money)
//				//.colorRes(R.color.colorAccent)
//				.actionBarSize());
		try {
			//Drawable managerDrawable = Drawable.createFromResourceStream(getResources(),new TypedValue(), getResources().getAssets().open("icon/piggybank_32.png"), null);
			Drawable managerDrawable = AssetsReader.getDrawableFromAssets(getApplicationContext(),"icon/piggybank_32.png");
			menuManager.setIcon(managerDrawable);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		MenuItem menuAbout = navigationView.getMenu().getItem(1);
		menuAbout.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_heart)
				//.colorRes(R.color.colorAccent)
				.actionBarSize());


		final NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
		NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
		NavigationUI.setupWithNavController(navigationView, navController);

		navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
			@Override
			public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
				final boolean isNavAccount = destination != null && destination.getId() == R.id.nav_account;
				final boolean debugEnabled = preferences.getBoolean(SettingsActivity.Key.ENABLE_DEBUG, false);
				if (menuActionWebViewInfo != null) {
					menuActionWebViewInfo.setVisible(isNavAccount && debugEnabled);
				}
				if (menuActionAccounts != null) {
					menuActionAccounts.setVisible(isNavAccount);
				}
			}
		});

		sharedPreferenceChangeListener = (sharedPreferences, key) -> {
			if (SettingsActivity.Key.ENABLE_DEBUG.equals(key) || SettingsActivity.Key.USE_JAVA_LOGS.equals(key) || SettingsActivity.Key.USE_JAVASCRIPT_LOGS.equals(key)) {
				final boolean debugEnabled = sharedPreferences.getBoolean(SettingsActivity.Key.ENABLE_DEBUG, false);
				final boolean javasLogsEnabled = sharedPreferences.getBoolean(SettingsActivity.Key.USE_JAVA_LOGS, false);
				final boolean javascriptLogsEnabled = sharedPreferences.getBoolean(SettingsActivity.Key.USE_JAVASCRIPT_LOGS, false);
				menuActionLogs.setVisible(debugEnabled && (javasLogsEnabled||javascriptLogsEnabled));
				if (SettingsActivity.Key.ENABLE_DEBUG.equals(key)) {
					final NavDestination currentDestination = navController.getCurrentDestination();
					final boolean isNavAccount = currentDestination != null && currentDestination.getId() == R.id.nav_account;
					menuActionWebViewInfo.setVisible(isNavAccount && debugEnabled);
				}
			}
		};
		preferences.registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);

		accountChangeListener = account -> {
			menuActionWebViewInfo.setEnabled(account != null);

		};
		applicationData.registerOnAccountChangeListener(accountChangeListener);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		preferences.unregisterOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);

		applicationData.unregisterOnAccountChangeListener(accountChangeListener);
	}

	@Override
	public void onResume() {
		super.onResume();

		applicationData.initializationUIMessages().forEach(message -> message.show(this));
		applicationData.clearInitializationUIMessages();

		final IntentFilter connectivityIntentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(connectivityStatusReceiver, connectivityIntentFilter);
	}

	@Override
	public void onPause() {
		super.onResume();
		unregisterReceiver(connectivityStatusReceiver);
	}


//	@Override
//	public boolean onNavigationItemSelected(MenuItem item) {
//
//		int id = item.getItemId();
//
//		// 6 - Show fragment after user clicked on a menu item
//		switch (id){
//			case R.id.activity_main_drawer_news :
//				this.showFragment(FRAGMENT_NEWS);
//				break;
//			case R.id.activity_main_drawer_profile:
//				this.showFragment(FRAGMENT_PROFILE);
//				break;
//			case R.id.activity_main_drawer_settings:
//				this.showFragment(FRAGMENT_PARAMS);
//				break;
//			default:
//				break;
//		}
//
//		this.drawerLayout.closeDrawer(GravityCompat.START);
//
//		return true;
//	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		menuActionAccounts = menu.findItem(R.id.action_accounts);
		menuActionAccounts.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_university)
				.color(Color.WHITE)
				.actionBarSize());

		menuActionLogs = menu.findItem(R.id.action_logs);
		menuActionLogs.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_th_list)
				.color(Color.WHITE)
				.actionBarSize());

		menuActionWebViewInfo = menu.findItem(R.id.action_webview_info);
		menuActionWebViewInfo.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_html5)
				.color(Color.WHITE)
				.actionBarSize());

		final MenuItem menuSettingsLogs = menu.findItem(R.id.action_settings);
		menuSettingsLogs.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_gear)
				.color(Color.WHITE)
				.actionBarSize());

		// Application starts with navAccount selected
		final boolean isNavAccount = true;

		final boolean debugEnabled = preferences.getBoolean(SettingsActivity.Key.ENABLE_DEBUG, false);
		final boolean javaLogsEnabled = preferences.getBoolean(SettingsActivity.Key.USE_JAVA_LOGS, false);
		final boolean javascriptLogsEnabled = preferences.getBoolean(SettingsActivity.Key.USE_JAVASCRIPT_LOGS, false);
		menuActionLogs.setVisible(isNavAccount && debugEnabled && (javascriptLogsEnabled||javaLogsEnabled));
		menuActionWebViewInfo.setVisible(isNavAccount && debugEnabled);
		menuActionWebViewInfo.setEnabled(applicationData.currentAccount() != null);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_accounts:
				startActivity(new Intent(MainActivity.this, AccountListActivity.class));
				return true;
			case R.id.action_settings:
				startActivity(new Intent(MainActivity.this, SettingsActivity.class));
				return true;
			case R.id.action_logs:
				startActivity(new Intent(MainActivity.this, LogMessageListActivity.class));
				return true;
			case R.id.action_webview_info:
				startActivity(new Intent(MainActivity.this, AccountWebViewDebugActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onSupportNavigateUp() {
		NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
		return NavigationUI.navigateUp(navController, mAppBarConfiguration)
				|| super.onSupportNavigateUp();
	}
}