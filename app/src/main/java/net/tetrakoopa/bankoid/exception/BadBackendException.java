package net.tetrakoopa.bankoid.exception;

public class BadBackendException extends Exception {

	public BadBackendException(String message, Throwable cause) {
		super(message, cause);
	}

	public BadBackendException(String message) {
		super(message);
	}

	public BadBackendException(Throwable cause) {
		super(cause);
	}
}
