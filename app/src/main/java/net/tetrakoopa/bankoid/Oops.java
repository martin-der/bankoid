package net.tetrakoopa.bankoid;

import android.content.Context;

import net.tetrakoopa.bankoid.debug.LogCapable;
import net.tetrakoopa.bankoid.ui.dialog.DialogBuilder;
import net.tetrakoopa.bankoid.ui.dialog.TechnicalProblemDialogBuilder;
import net.tetrakoopa.bankoid.util.ResourceHelper;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;

public class Oops implements LogCapable {

	public interface OnClosedListener {
		void onClosed();
	}

	public static abstract class Any {

		public void logAndNotify(Context context, OnClosedListener closedListener, int messageId, Exception cause) {
			logAndNotify(context, closedListener, context.getClass(), messageId, cause);
		}
		public void logAndNotify(Context context, int messageId, Exception cause) {
			logAndNotify(context, null, context.getClass(), messageId, cause);
		}

		public void logAndNotify(Context context, OnClosedListener closedListener, Class<?> clazz, int messageId, Exception cause) {
			final String messageDefault = ResourceHelper.Original.text(context, messageId);
			final long logMessageID = log(clazz, messageDefault, cause);
			notifyUser(context, logMessageID, closedListener, messageId);
		}
		public void logAndNotify(Context context, Class<?> clazz, int message, Exception cause) {
			logAndNotify(context, null, context.getClass(), message, cause);
		}

		public void logAndNotify(Context context, OnClosedListener closedListener, String message, Exception cause) {
			logAndNotify(context, closedListener, context.getClass(), message, cause);
		}
		public void logAndNotify(Context context, String message, Exception cause) {
			logAndNotify(context, (OnClosedListener)null, message, cause);
		}

		public void logAndNotify(Context context, OnClosedListener closedListener, Class<?> clazz, String message, Exception cause) {
			final long logMessageID = log(clazz, message, cause);
			notifyUser(context, logMessageID, closedListener, message);
		}
		public void logAndNotify(Context context, Class<?> clazz, String message, Exception cause) {
			logAndNotify(context, (OnClosedListener)null, clazz, message, cause);
		}

		public abstract long log(Class<?> clazz, String message, Exception cause);

	}

	public static class Error extends Any {

		public long log(Class<?> clazz, String message, Exception cause) {
			return Oops.log(clazz, MessageLogger.LogMessage.Severity.ERROR, message, cause);
		}

	}

	public static class Fatal extends Any {

		public long log(Class<?> clazz, String message, Exception cause) {
			return Oops.log(clazz, MessageLogger.LogMessage.Severity.FATAL, message, cause);
		}

	}

	public static final Any ERROR = new Error();
	public static final Any FATAL = new Fatal();

	private static void notifyUser(Context context, long logMessageId, OnClosedListener onClosedListener, String message) {
		createDialogBuilder(context, onClosedListener)
				.setLogMessage(logMessageId)
				.setMessage(message)
				.show();
	}
	private static void notifyUser(Context context, long logMessageId, OnClosedListener onClosedListener, int messageId) {
		createDialogBuilder(context, onClosedListener)
				.setLogMessage(logMessageId)
				.setMessage(messageId)
				.show();
	}

	private static TechnicalProblemDialogBuilder createDialogBuilder(Context context, OnClosedListener onClosedListener) {
		final TechnicalProblemDialogBuilder builder = DialogBuilder.technicalProblem(context);
		builder.setPositiveButton("OK", null);
		if (onClosedListener != null)
			builder.setOnDismissListener(dialogInterface -> onClosedListener.onClosed());
		return builder;
	}


	private static long log(Class<?> clazz, MessageLogger.LogMessage.Severity severity, String message, Exception cause) {
		return logger.log(severity, clazz, message, cause);
	}

}
