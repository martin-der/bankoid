package net.tetrakoopa.bankoid.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;

import net.tetrakoopa.bankoid.ApplicationData;
import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.debug.LogCapable;
import net.tetrakoopa.bankoid.util.ResourceHelper;

import androidx.appcompat.app.AppCompatActivity;

public abstract class CommonActivity extends AppCompatActivity implements LogCapable, ApplicationData.Aware, ResourceHelper.Consumer {

	private Integer originalTheme;
	private boolean firstObservation;

	private final static String STATE_KEY_FIRST_OBSERVATION = "theme_applied";

	@Override
	public Context getResourcesContext() {
		return this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (originalTheme == null) originalTheme = getOriginalTheme();
		setTheme();
		super.onCreate(savedInstanceState);
//		firstObservation = savedInstanceState == null ? true : savedInstanceState.getBoolean(STATE_KEY_FIRST_OBSERVATION, true);
	}

//	@Override
//	public void onSaveInstanceState(Bundle savedInstanceState) {
//		savedInstanceState.putBoolean(STATE_KEY_FIRST_OBSERVATION, false);
//		super.onSaveInstanceState(savedInstanceState);
//	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onStart() {
		super.onStart();
//		applicationData.theme().observe(CommonActivity.this, applicationTheme -> {
//			if (firstObservation) {
//				firstObservation = false;
//				setTheme();
//				recreate();
//			}
//		});
	}

	@Override
	public void onStop() {
		super.onStop();
		applicationData.theme().removeObservers(this);
	}

	public void setTheme() {
		final int changedTheme = getThemeFor(applicationData.theme().getValue());
		if (changedTheme != originalTheme)
			setTheme(changedTheme);
	}

	private void reTheme() {
		getTheme().applyStyle(getThemeFor(applicationData.theme().getValue()), true);
	}

	@SuppressLint("NewApi")
	private int getOriginalTheme() {
		try {
			return getPackageManager().getActivityInfo(getComponentName(), 0).getThemeResource();
		} catch (PackageManager.NameNotFoundException e) {
			throw new InternalError("Failed to get actual theme", e);
		}
	}

	private int getThemeFor(ApplicationTheme theme) {
		switch (originalTheme) {
			case R.style.AppTheme:
				switch (theme) {
					case LIGHT: return originalTheme;
					case DARK: return R.style.AppTheme_Dark;
				}
				break;
			case R.style.AppTheme_NoActionBar:
				switch (theme) {
					case LIGHT: return originalTheme;
					case DARK: return R.style.AppTheme_Dark_NoActionBar;
				}
				break;
//			case R.style.AppTheme_AppBarOverlay:
//				switch (theme) {
//					case LIGHT: return originalTheme;
//					case DARK: return R.style.AppTheme_Dark_AppBarOverlay;
//				}
//				break;
		}
		return originalTheme;
	}
}
