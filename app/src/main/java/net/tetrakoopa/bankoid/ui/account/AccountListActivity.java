package net.tetrakoopa.bankoid.ui.account;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import net.tetrakoopa.bankoid.ApplicationDataHelper;
import net.tetrakoopa.bankoid.Oops;
import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.SettingsActivity;
import net.tetrakoopa.bankoid.custom.RecyclerViewTouchListener;
import net.tetrakoopa.bankoid.exception.BadBackendException;
import net.tetrakoopa.bankoid.model.AccountContent;
import net.tetrakoopa.bankoid.model.bank.About;
import net.tetrakoopa.bankoid.service.persistence.AccountPersistenceService;
import net.tetrakoopa.bankoid.service.persistence.exception.NoSuchElementException;
import net.tetrakoopa.bankoid.ui.CommonActivity;
import net.tetrakoopa.bankoid.ui.account.create.CreationDialog;
import net.tetrakoopa.bankoid.ui.dialog.DialogBuilder;
import net.tetrakoopa.bankoid.ui.dialog.RegularOneViewDialogBuilder;
import net.tetrakoopa.bankoid.ui.dialog.content.HeaderBodyFooterHolder;
import net.tetrakoopa.bankoid.ui.toast.ToastBuilder;
import net.tetrakoopa.bankoid.util.Continuation;

import java.util.List;

import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;

public class AccountListActivity extends CommonActivity {

	private boolean mTwoPane;

	private MenuItem menuActionSelect;
	private MenuItem menuActionEdit;
	private MenuItem menuActionDelete;

	private Vibrator vibrator;

	private VibrationEffect vibrationForUseFromLongClick;
	private VibrationEffect vibrationForSelectFromLongClick;

	private AccountContent.Account selectedAccount;
	private int selectedAccountPosition;

	private SimpleItemRecyclerViewAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account_list);

		final Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		toolbar.setTitle(getTitle());

		vibrator = (Vibrator)getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
		vibrationForUseFromLongClick = VibrationEffect.createPredefined(VibrationEffect.EFFECT_CLICK);
		vibrationForUseFromLongClick = VibrationEffect.createPredefined(VibrationEffect.EFFECT_CLICK);


		final FloatingActionButton fab = findViewById(R.id.fab_add);
		fab.setOnClickListener(view -> {
			CreationDialog.pickNameAndStart(AccountListActivity.this);
		});
		fab.setImageDrawable(new IconDrawable(this, FontAwesomeIcons.fa_plus)
				.color(Color.WHITE)
				.actionBarSize());

		if (findViewById(R.id.account_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-w900dp).
			// If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;
		}

		final RecyclerView recyclerView = findViewById(R.id.account_list);
		ApplicationDataHelper.loadAccounts(AccountListActivity.this);
		adapter = new SimpleItemRecyclerViewAdapter(this, applicationData.accounts(), mTwoPane);
		recyclerView.setAdapter(adapter);

		recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getApplicationContext(), recyclerView, new RecyclerViewTouchListener.ClickListener() {
			@Override
			public void onClick(View view, int position) {
				AccountListActivity.this.clickAccount(view, position);
			}

			@Override
			public void onLongClick(View view, int position) {
				AccountListActivity.this.longClickAccount(view);
			}
		}));

	}

	public class SimpleItemRecyclerViewAdapter
			extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

		private final AccountListActivity mParentActivity;
		private final List<AccountContent.Account> mValues;
		private final boolean mTwoPane;

		SimpleItemRecyclerViewAdapter(AccountListActivity parent,
									  List<AccountContent.Account> items,
									  boolean twoPane) {
			mValues = items;
			mParentActivity = parent;
			mTwoPane = twoPane;

		}

		@Override
		public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View view = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.account_list_content, parent, false);
			return new ViewHolder(view);
		}

		@Override
		public void onBindViewHolder(final ViewHolder holder, int position) {
			final AccountContent.Account account = mValues.get(position);
			final About about = applicationData.getBackendInfos().get(account.bank);
			if (about != null ) {
				if (about.getLogoDrawable() != null) {
					holder.logo.setImageDrawable(about.getLogoDrawable());
				}
			}
			holder.name.setText(account.name);
			holder.identifier.setText(account.identifier);
			holder.itemView.setTag(account);
			//holder.itemView.setOnClickListener(view -> AccountListActivity.this.clickAccount(view));
			holder.itemView.setActivated(selectedAccount != null && selectedAccount.name.equals(account.name));
//			holder.itemView.
//			holder.itemView.addState(selectedAccount != null && selectedAccount.name.equals(account.name));
		}

		@Override
		public int getItemCount() {
			return mValues.size();
		}

		class ViewHolder extends RecyclerView.ViewHolder {
			final ImageView logo;
			final TextView name;
			final TextView identifier;
			final TextView comment;

			ViewHolder(View view) {
				super(view);
				logo = view.findViewById(R.id.bank_logo);
				name = view.findViewById(R.id.name);
				identifier = view.findViewById(R.id.identifier);
				comment = view.findViewById(R.id.comment);
			}
		}

		public void remove(int position) {
			AccountContent.remove(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.account_list, menu);

		final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		menuActionSelect = menu.findItem(R.id.action_select);
		menuActionSelect.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_bullseye)
				.color(Color.WHITE)
				.actionBarSize());

		menuActionEdit = menu.findItem(R.id.action_edit);
		menuActionEdit.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_pencil)
				.color(Color.WHITE)
				.actionBarSize());

		menuActionDelete = menu.findItem(R.id.action_delete);
		menuActionDelete.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_trash)
				.color(Color.WHITE)
				.actionBarSize());

		updateMenus();

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_select:
				useAccount(selectedAccount, success -> {if (success) finish();});
				return true;
			case R.id.action_edit:
				openAccountDetail(selectedAccount);
				return true;
			case R.id.action_delete:
				DialogBuilder.question(AccountListActivity.this)
						.addDecisionButtons((dialogInterface, i) -> {
							try {
								new AccountPersistenceService(AccountListActivity.this).delete(selectedAccount.id);
							} catch (NoSuchElementException ex) {
								Oops.ERROR.logAndNotify(AccountListActivity.this, R.string.message_error_failed_to_delete_account, ex);
								return;
							}
							selectedAccount = null;
							adapter.remove(selectedAccountPosition);
							updateMenus();
							adapter.notifyItemRemoved(selectedAccountPosition);
							useAccount(null, null);
						})
						.setContent(R.layout.account_header, (holder, content) -> {
							holder.getHeader().setText(R.string.message_account_deletion_confirmation);
							holder.getFooter().setText(R.string.message_account_deletion_warning_about_it);

							((RegularOneViewDialogBuilder.SeverityTintHolder)holder.getNatureHolder("SEVERITY")).markViews(HeaderBodyFooterHolder.ViewMarker.FOOTER);

							final About about = applicationData.getBackendInfos().get(selectedAccount.bank);
							if (about != null ) {
								if (about.getLogoDrawable() != null)
									((ImageView) content.findViewById(R.id.bank_logo)).setImageDrawable(about.getLogoDrawable());
							}
							((TextView)content.findViewById(R.id.name)).setText(selectedAccount.name);
							((TextView)content.findViewById(R.id.identifier)).setText(selectedAccount.identifier);

						})
						.setSeverity(RegularOneViewDialogBuilder.SeverityTintHolder.Severity.WARNING)
						.setTitle(R.string.title_account_deletion)
						.show();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void updateMenus() {
		menuActionSelect.setVisible(selectedAccount != null);
		menuActionEdit.setVisible(selectedAccount != null);
		menuActionDelete.setVisible(selectedAccount != null);
		//TODO: use invalidateOptionsMenu and onCreateOptionsMenu instead ?
	}

	private void useAccount(AccountContent.Account account, Continuation continuation) {
		try {
			applicationData.setCurrentAccount(getApplicationContext(), account);
		} catch (BadBackendException ex) {
			Oops.ERROR.logAndNotify(AccountListActivity.this, () -> continuation.then(false), R.string.message_error_failed_to_select_account, ex);
			return;
		}

		final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AccountListActivity.this);

		if (account != null) {
			preferences
					.edit()
					.putString(SettingsActivity.Key.ACCOUNT_SELECTED_PREVIOUSLY, account.name)
					.commit();
		}

		if (true) {
			final String message = account!=null
					? resourceText(R.string.message_account_selected, account.name)
					: resourceText(R.string.message_no_account_selected);
			ToastBuilder.withIcon(AccountListActivity.this, Toast.LENGTH_LONG)
					.setMessage(message)
					.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_bullseye)
							.color(Color.BLACK)
							.actionBarSize())
					.show();
		}
		if (continuation != null) continuation.then(true);
	}
	private void openAccountDetail(AccountContent.Account account) {
		if (mTwoPane) {
			final Bundle arguments = new Bundle();
			arguments.putString(AccountDetailFragment.ACCOUNT_NAME, String.valueOf(account.id));
			AccountDetailFragment fragment = new AccountDetailFragment();
			fragment.setArguments(arguments);
			this.getSupportFragmentManager().beginTransaction()
					.replace(R.id.account_detail_container, fragment)
					.commit();
		} else {
//			final Bundle arguments = new Bundle();
//			arguments.putString(AccountDetailFragment.ARG_ITEM_ID, String.valueOf(account.id));
			final Context context = AccountListActivity.this;
			Intent intent = new Intent(context, AccountDetailActivity.class);
			intent.putExtra(AccountDetailFragment.ACCOUNT_NAME, account.name);

			context.startActivity(intent);
		}
	}

	private void longClickAccount(View accountView) {
		final AccountContent.Account account = (AccountContent.Account) accountView.getTag();
		vibrator.vibrate(vibrationForUseFromLongClick);
		useAccount(account, success -> {if (success) finish();});
	}
	private void clickAccount(View accountView, int position) {
		final AccountContent.Account account = (AccountContent.Account) accountView.getTag();
		selectedAccount = account;
		selectedAccountPosition = position;
		updateMenus();
		adapter.notifyDataSetChanged();
	}

}