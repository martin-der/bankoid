package net.tetrakoopa.bankoid.ui.account.create;

import net.tetrakoopa.bankoid.model.BankContent;

public interface BankDataManager {
	void onSelectBank(BankContent.Bank bank);
	BankContent.Bank getBank();
}
