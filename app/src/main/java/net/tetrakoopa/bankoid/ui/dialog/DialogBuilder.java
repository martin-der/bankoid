package net.tetrakoopa.bankoid.ui.dialog;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

public final class DialogBuilder {
	private DialogBuilder(){};

	public static <C extends View> QuestionDialogBuilder<C> question(@NonNull Context context, int themeResId) {
		return new QuestionDialogBuilder<>(context, themeResId);
	}
	public static <C extends View> QuestionDialogBuilder<C> question(@NonNull Context context) {
		return new QuestionDialogBuilder<>(context);
	}

	public static NotificationDialogBuilder notification(@NonNull Context context, int themeResId) {
		return new NotificationDialogBuilder(context, themeResId);
	}
	public static NotificationDialogBuilder notification(@NonNull Context context) {
		return new NotificationDialogBuilder(context);
	}

	public static TechnicalProblemDialogBuilder technicalProblem(@NonNull Context context, int themeResId) {
		return new TechnicalProblemDialogBuilder(context, themeResId);
	}
	public static TechnicalProblemDialogBuilder technicalProblem(@NonNull Context context) {
		return new TechnicalProblemDialogBuilder(context);
	}

	public static TutorialDialogBuilder tutorial(@NonNull Context context, int themeResId) {
		return new TutorialDialogBuilder(context, themeResId);
	}
	public static TutorialDialogBuilder tutorial(@NonNull Context context) {
		return new TutorialDialogBuilder(context);
	}
}
