package net.tetrakoopa.bankoid.ui.dialog;

import android.content.Context;
import android.content.DialogInterface;

import net.tetrakoopa.bankoid.ApplicationData;
import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.debug.LogCapable;
import net.tetrakoopa.bankoid.ui.ApplicationTheme;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

public abstract class CommonDialogBuilder<B extends CommonDialogBuilder<B>> extends AlertDialog.Builder implements ApplicationData.Aware, LogCapable {

	protected class Params {
		CharSequence message;
	}

	protected final Params params = new Params();

	public interface OnDialogCreatedListener {
		void onCreated(AlertDialog dialog);
	}

	private DialogInterface.OnShowListener onShowListener;
	private OnDialogCreatedListener onCreatedListener;

	public CommonDialogBuilder(@NonNull Context context) {
		super(context, getStyleForTheme(applicationData.theme().getValue()));
	}

	public CommonDialogBuilder(@NonNull Context context, int themeResId) {
		super(context, themeResId);
	}

	@Override
	public AlertDialog create() {
		final AlertDialog dialog = super.create();
		if (onCreatedListener != null || onShowListener != null)
			dialog.setOnShowListener(dialogInterface -> {
				if (onCreatedListener != null) onCreatedListener.onCreated(dialog);
				if (onShowListener != null) onShowListener.onShow(dialogInterface);
			});
		return dialog;
	}

	public B setOnDialogCreated(OnDialogCreatedListener listener) {
		onCreatedListener = listener;
		return (B)this;
	}

	public B setTitle(@StringRes int titleId) {
		super.setTitle(titleId);
		return (B)this;
	}

	public B setTitle(@Nullable CharSequence title) {
		super.setTitle(title);
		return (B)this;
	}

	private static int getStyleForTheme(ApplicationTheme theme) {
		switch (theme) {
			case LIGHT: return R.style.RegularDialog;
			case DARK: return R.style.RegularDialog_Dark;
		}
		logger.log(MessageLogger.LogMessage.Severity.WARNING, CommonDialogBuilder.class, "No theme found for "+theme.name());
		return R.style.RegularDialog;

	}
}
