package net.tetrakoopa.bankoid.ui.dialog.nature;

import android.view.View;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public abstract class NatureTintHolder<M extends Enum<M> & NatureTintCapable.IdAwareMarker> implements NatureTintCapable.Holder<M> {

	private final HashSet<M> markers = new HashSet<>();

	@Override
	public final void markViews(M... markers) {
		this.markers.clear();
		for (M marker : markers) {
			if (marker != null) this.markers.add(marker);
		}
	}

	@Override
	public final View[] getMarkedViews(View root) {
		final List<View> views = new ArrayList<>();
		for (M marker : this.markers) {
			views.add(root.findViewById(marker.getId()));
		}
		return views.toArray(new View[views.size()]);
	}

}
