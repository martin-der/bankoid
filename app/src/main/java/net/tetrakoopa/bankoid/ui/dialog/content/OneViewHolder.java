package net.tetrakoopa.bankoid.ui.dialog.content;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import net.tetrakoopa.bankoid.ui.dialog.nature.NatureTintCapable;

import java.util.HashMap;
import java.util.Map;

public abstract class OneViewHolder<V extends View> extends Holder<V> implements NatureTintCapable {

	protected final View root;

	protected final V content;

	private final Map<String, Holder<?>> natureTintHolders = new HashMap<>();

	public OneViewHolder(int layoutId, int stubId, int contentLayoutId, LayoutInflater inflater, ViewGroup parent) {
		root = inflater.inflate(layoutId, parent);

		final View maybeViewStub= root.findViewById(stubId);
		final ViewStub viewStub;
		try {
			viewStub = (ViewStub) maybeViewStub;
		} catch (ClassCastException ccex) {
			throw new IllegalArgumentException("Stub id '"+stubId+"'must resolve to a ViewStub : "+ccex.getMessage(), ccex);
		}

		viewStub.setLayoutResource(contentLayoutId);

		final View contentView = viewStub.inflate();
		try {
			content = (V) contentView;
		} catch (ClassCastException ccex) {
			throw new IllegalArgumentException("content view is not the expected type of view : "+ccex.getMessage(), ccex);
		}

	}

	@Override
	public <M extends Enum<M> & NatureTintCapable.IdAwareMarker> Holder<M> getNatureHolder(String nature) {
		return natureTintHolders.containsKey(nature) ? (Holder<M>) natureTintHolders.get(nature) : Holder.NOOP;
	}
	public void addNatureHolder(String name, Holder<?> tintHolder) {
		natureTintHolders.put(name, tintHolder);
	}
	public View getRoot() {
		return root;
	}

	public V getContent() {
		return content;
	}
}
