package net.tetrakoopa.bankoid.ui.debug;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import net.tetrakoopa.bankoid.ui.CommonActivity;
import net.tetrakoopa.bankoid.backend.ui.model.AccountWebViewModel;
import net.tetrakoopa.bankoid.R;

import androidx.appcompat.app.ActionBar;
import androidx.lifecycle.Observer;

public class AccountWebViewDebugActivity extends CommonActivity {

    private AccountWebViewModel accountWebViewModel;

    private IconDrawable iconBooleanTrue;
    private IconDrawable iconBooleanFalse;

    private TextView textUrl;
    private ImageView imageLoggedFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_web_view_debug);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.title_account_webview_debug);

        accountWebViewModel = applicationData.currentBackend().model;

        if (iconBooleanTrue == null)
            iconBooleanTrue = new IconDrawable(getApplicationContext(), FontAwesomeIcons.fa_check)
                   .color(Color.GREEN)
                    .actionBarSize();
        if (iconBooleanFalse == null)
            iconBooleanFalse = new IconDrawable(getApplicationContext(), FontAwesomeIcons.fa_times)
                    .color(Color.RED)
                    .actionBarSize();

        textUrl = findViewById(R.id.text_url);
        updateURL(accountWebViewModel.getUrl().getValue());
        accountWebViewModel.getUrl().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String url) {
                updateURL(url);
            }
        });

        final TextView textIsLoggedFlag = findViewById(R.id.text_logged_flag);
        textIsLoggedFlag.setText("Is logged : ");
        imageLoggedFlag = findViewById(R.id.image_logged_flag);
        updateLoggedFlag(accountWebViewModel.getLogged().getValue());
        accountWebViewModel.getLogged().observe(this, logged -> updateLoggedFlag(logged));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void updateURL(String url) {
        textUrl.setText(url == null ? "" : url);
    }
    private void updateLoggedFlag(boolean logged) {
        updateBoolean(imageLoggedFlag, logged, false);
    }

    private void updateBoolean(ImageView view, Boolean value, boolean defaultValue) {
        if (value == null) value = defaultValue;
        view.setImageDrawable(value?iconBooleanTrue:iconBooleanFalse);
    }

}