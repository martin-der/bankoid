package net.tetrakoopa.bankoid.ui.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.ui.dialog.content.HeaderBodyFooterHolder;

import androidx.annotation.NonNull;

public class QuestionDialogBuilder<C extends View> extends RegularOneViewDialogBuilder<C, HeaderBodyFooterHolder<C>, QuestionDialogBuilder<C>> {

	public QuestionDialogBuilder(@NonNull Context context) {
		super(context);
		initDefault(context);
	}

	public QuestionDialogBuilder(@NonNull Context context, int themeResId) {
		super(context, themeResId);
		initDefault(context);
	}

	private final void initDefault(Context context) {
		setIcon(R.drawable.ic_pig_face);
	}

	public QuestionDialogBuilder<C> addDecisionButtons(DialogInterface.OnClickListener onClickOkListener, DialogInterface.OnClickListener onClickCanceListener) {
		return addDecisionButtons(android.R.string.ok, onClickOkListener, onClickCanceListener);
	}
	public QuestionDialogBuilder<C> addDecisionButtons(DialogInterface.OnClickListener onClickOkListener) {
		return addDecisionButtons(android.R.string.ok, onClickOkListener, null);
	}

	public QuestionDialogBuilder<C> addDecisionButtons(String message, DialogInterface.OnClickListener onClickOkListener, DialogInterface.OnClickListener onClickCanceListener) {
		setPositiveButton(message, onClickOkListener);
		setNegativeButton(android.R.string.cancel, onClickCanceListener);
		return this;
	}
	public QuestionDialogBuilder<C> addDecisionButtons(int resId, DialogInterface.OnClickListener onClickOkListener, DialogInterface.OnClickListener onClickCanceListener) {
		setPositiveButton(resId, onClickOkListener);
		setNegativeButton(android.R.string.cancel, onClickCanceListener);
		return this;
	}
	public QuestionDialogBuilder<C> addDecisionButtons(int resId, DialogInterface.OnClickListener onClickOkListener) {
		return addDecisionButtons(resId, onClickOkListener, null);
	}


}
