package net.tetrakoopa.bankoid.ui.account.create;

import net.tetrakoopa.bankoid.model.bank.Config;

public interface AccountDataManager {

	void onChangeIdentifier(String identifier, boolean valid);
	String getIdentifier();

	String identifierValidationProblems();

	void onChangeAccount(Config.Account type);
	Config.Account getAccount();
}
