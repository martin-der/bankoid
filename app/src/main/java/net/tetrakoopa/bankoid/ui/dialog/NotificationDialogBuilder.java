package net.tetrakoopa.bankoid.ui.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import net.tetrakoopa.bankoid.ui.dialog.content.OneViewHolder;

import androidx.annotation.NonNull;

public class NotificationDialogBuilder<C extends View, H extends OneViewHolder<C>> extends RegularOneViewDialogBuilder<C,H,NotificationDialogBuilder<C,H>> {

	public NotificationDialogBuilder(@NonNull Context context) {
		super(context);
	}

	public NotificationDialogBuilder(@NonNull Context context, int themeResId) {
		super(context, themeResId);
	}

	public NotificationDialogBuilder<C, H> addValidationButton(DialogInterface.OnClickListener onClickListener) {
		return addValidationButton(android.R.string.ok, onClickListener);
	}
	public NotificationDialogBuilder<C, H> addValidationButton(int resId, DialogInterface.OnClickListener onClickListener) {
		setNeutralButton(resId, onClickListener);
		return this;
	}

	public NotificationDialogBuilder<C, H> setMessage(CharSequence text) {
		return super.setMessage(text);
	}

}
