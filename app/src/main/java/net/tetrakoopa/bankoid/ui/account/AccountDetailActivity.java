package net.tetrakoopa.bankoid.ui.account;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import androidx.appcompat.app.ActionBar;

import android.view.MenuItem;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.ui.CommonActivity;

public class AccountDetailActivity extends CommonActivity {

	public static final String ARG_ACCOUNT_NAME = "account_id";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account_detail);
		Toolbar toolbar = findViewById(R.id.detail_toolbar);
		setSupportActionBar(toolbar);

		// Show the Up button in the action bar.
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}

		// savedInstanceState is non-null when there is fragment state
		// saved from previous configurations of this activity
		// (e.g. when rotating the screen from portrait to landscape).
		// In this case, the fragment will automatically be re-added
		// to its container so we don"t need to manually add it.
		// For more information, see the Fragments API guide at:
		//
		// http://developer.android.com/guide/components/fragments.html
		//
		if (savedInstanceState == null) {
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
			AccountDetailFragment fragment = new AccountDetailFragment();
			if (getIntent().hasExtra(ARG_ACCOUNT_NAME)) {
				final String accountName = getIntent().getStringExtra(ARG_ACCOUNT_NAME);
				final Bundle arguments = new Bundle();
				arguments.putString(AccountDetailFragment.ACCOUNT_NAME, accountName);
				fragment.setArguments(arguments);
			}
			getSupportFragmentManager().beginTransaction()
					.add(R.id.account_detail_container, fragment)
					.commit();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {

			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			navigateUpTo(new Intent(this, AccountListActivity.class));

			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}