package net.tetrakoopa.bankoid.ui.dialog.nature;

import android.view.View;

public interface NatureTintCapable {

	interface IdAwareMarker {
		int getId();
	}

	interface Holder<M extends Enum<M> & IdAwareMarker> {

		final class Noop<M extends Enum<M> & IdAwareMarker> implements Holder<M> {

			private static final View[] NO_VIEW = new View[0];

			@Override
			public String getName() {
				return "Noop";
			}

			@Override
			public void markViews(M... markers) { }

			@Override
			public View[] getMarkedViews(View root) { return NO_VIEW; }
		}
		Noop NOOP = new Noop<>();

		String getName();

		void markViews(M... markers);

		View[] getMarkedViews(View root);
	}

	interface Applier {
		void apply(View view);
	}

	<M extends Enum<M> & NatureTintCapable.IdAwareMarker> Holder<M> getNatureHolder(String nature);
}
