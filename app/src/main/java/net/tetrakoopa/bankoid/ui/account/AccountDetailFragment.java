package net.tetrakoopa.bankoid.ui.account;

import android.app.Activity;
import android.os.Bundle;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.tetrakoopa.bankoid.Oops;
import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.model.AccountContent;
import net.tetrakoopa.bankoid.service.persistence.AccountPersistenceService;
import net.tetrakoopa.bankoid.service.persistence.exception.NoSuchElementException;
import net.tetrakoopa.bankoid.service.persistence.exception.NonUniqueResultException;

/**
 * A fragment representing a single Account detail screen.
 * This fragment is either contained in a {@link AccountListActivity}
 * in two-pane mode (on tablets) or a {@link AccountDetailActivity}
 * on handsets.
 */
public class AccountDetailFragment extends Fragment {

    public static final String ACCOUNT_NAME = "account_id";

    private AccountContent.Account mItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ACCOUNT_NAME)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            final String accountName = getArguments().getString(ACCOUNT_NAME);
            //mItem = AccountContent.getItemMap().get(accountName);
            try {
                mItem = new AccountPersistenceService(getContext()).findByName(accountName);
            } catch (NonUniqueResultException | NoSuchElementException ex) {
                Oops.ERROR.logAndNotify(getContext(), AccountDetailFragment.class, R.string.message_error_failed_to_load_account, ex);
                return;
            }

            final Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.name);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.account_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.account_detail)).setText(mItem.identifier);
        }

        return rootView;
    }
}