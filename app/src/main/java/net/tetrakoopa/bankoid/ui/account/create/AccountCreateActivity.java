package net.tetrakoopa.bankoid.ui.account.create;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.shuhart.stepview.StepView;

import net.tetrakoopa.bankoid.ApplicationDataHelper;
import net.tetrakoopa.bankoid.Oops;
import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.model.BankContent;
import net.tetrakoopa.bankoid.model.bank.Config;
import net.tetrakoopa.bankoid.service.business.AccountService;
import net.tetrakoopa.bankoid.service.persistence.AccountPersistenceService;
import net.tetrakoopa.bankoid.service.persistence.exception.NoSuchElementException;
import net.tetrakoopa.bankoid.service.persistence.exception.UniqueConstraintViolationException;
import net.tetrakoopa.bankoid.ui.CommonActivity;
import net.tetrakoopa.bankoid.ui.account.create.fragment.AccountFragment;
import net.tetrakoopa.bankoid.ui.account.create.fragment.BankFragment;
import net.tetrakoopa.bankoid.ui.account.create.fragment.NameFragment;
import net.tetrakoopa.bankoid.ui.account.create.fragment.ValidationFragment;
import net.tetrakoopa.bankoid.ui.dialog.DialogBuilder;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


public class AccountCreateActivity extends CommonActivity {

    public static final String ARG_NAME = "name";
    private boolean notifyAccountCreationSuccessful = true;

    private static class Steps {
        private enum Name {
            BANK, ACCOUNT, VALIDATION
        }
        private static class Step {

            public enum Status {
                TODO, IN_PROGRESS, DONE
            }

            private String title;
            private Status status;

            public Step(String title) {
                this(title, Status.TODO);
            }
            public Step(String title, Status status) {
                this.title = title;
                this.status = status;
            }

            public String getTitle() {
                return title;
            }

            public Status getStatus() {
                return status;
            }
        }

        Step bank;
        Step account;
        Step validation;
    }

    public class Data implements BankDataManager, AccountDataManager {

        public String name;
        public BankContent.Bank bank;
        public String identifier;
        public Config.Account account;

        public void onSetName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public void onSelectBank(BankContent.Bank bank) {
            this.bank = bank;
            AccountCreateActivity.this.next.setEnabled(bank != null);
            AccountCreateActivity.this.bankFragment.notifyDataSetChanged();

        }

        @Override
        public BankContent.Bank getBank() {
            return this.bank;
        }

        @Override
        public void onChangeIdentifier(String identifier, boolean valid) {
            this.identifier = identifier;
            AccountCreateActivity.this.next.setEnabled(identifierValidationProblems() == null);
        }

        @Override
        public String getIdentifier() {
            return this.identifier;
        }

        @Override
        public String identifierValidationProblems() {
            return this.identifier.length()<5 ? "5 char min" : null;
        }

        @Override
        public void onChangeAccount(Config.Account account) {
            this.account = account;
            AccountCreateActivity.this.next.setEnabled(bank != null);
        }

        @Override
        public Config.Account getAccount() {
            return this.account;
        }

    }


    private final Steps step = new Steps();

    private Steps.Name currentStep;

    private StepView steps;
    private Button next;
    private Button previous;

    private final NameFragment nameFragment;
    private final BankFragment bankFragment;
    private final AccountFragment accountFragment;
    private final ValidationFragment validationFragment;

    public final Data data = new Data();

    public AccountCreateActivity() {
        nameFragment = new NameFragment();
        bankFragment = new BankFragment();
        accountFragment = new AccountFragment();
        validationFragment = new ValidationFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_create);

        data.name = getIntent().getStringExtra(ARG_NAME);

        next = findViewById(R.id.next);
        previous = findViewById(R.id.previous);

        final List<Steps.Step> steps = new ArrayList<>();
        steps.add(new Steps.Step("Bank", Steps.Step.Status.IN_PROGRESS));
        steps.add(new Steps.Step("Account"));
        steps.add(new Steps.Step("Validation"));
        //final List<StepBean> stepBeans = steps.stream().map(s -> s.toStepBean()).collect(Collectors.toList());

        step.bank = steps.get(0);
        step.account = steps.get(1);
        step.validation = steps.get(2);

        this.steps = findViewById(R.id.steps);
        this.steps.setSteps(steps.stream().map(s -> s.getTitle()).collect(Collectors.toList()));


        currentStep = Steps.Name.BANK;
        previous.setVisibility(View.INVISIBLE);
        next.setEnabled(false);

        if (savedInstanceState == null) {
            final Bundle arguments = new Bundle();
            arguments.putString(BankFragment.ARG_SELECTION_MODE_ID, BankFragment.SelectionMode.PERSISTENT.name());
            bankFragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content, bankFragment)
                    .commit();
        }

    }

    public void goPreviousStep() {
        if (currentStep != Steps.Name.BANK)
            goPrevious(null);
    }

    private void setStepFragment(Steps.Name step) {
        final FragmentManager manager = getSupportFragmentManager();
        final FragmentTransaction transaction = manager.beginTransaction();

        for (Fragment fragment : manager.getFragments()) {
            transaction.remove(fragment);
        }

        transaction.setCustomAnimations(R.anim.nav_default_enter_anim,R.anim.nav_default_exit_anim,R.anim.nav_default_pop_enter_anim,R.anim.nav_default_pop_exit_anim);

        switch (step)  {
            case BANK:
                final Bundle arguments = new Bundle();
                arguments.putString(BankFragment.ARG_SELECTION_MODE_ID, BankFragment.SelectionMode.PERSISTENT.name());
                bankFragment.setArguments(arguments);
                transaction.add(R.id.content, bankFragment);
                break;
            case ACCOUNT:
                transaction.add(R.id.content, accountFragment);
                break;
            case VALIDATION:
                transaction.add(R.id.content, validationFragment);
                break;
        }

        transaction.commit();
    }

    public void openAccountTypeChoosePopup(View view) {
        accountFragment.openTypeChoosePopup(view);
    }

    public void goPrevious(View view) {
        switch (currentStep)  {
            case BANK:
                break;
            case ACCOUNT:
                currentStep = Steps.Name.BANK;
                steps.go(currentStep.ordinal(), true);
                previous.setVisibility(View.INVISIBLE);
                break;
            case VALIDATION:
                currentStep = Steps.Name.ACCOUNT;
                steps.go(currentStep.ordinal(), true);
                break;
        }
        setStepFragment(currentStep);
        //steps.invalidate();
    }
    public void goNext(View view) {
        switch (currentStep)  {
            case BANK:
                currentStep = Steps.Name.ACCOUNT;
                steps.go(currentStep.ordinal(), true);
                previous.setVisibility(View.VISIBLE);
                break;
            case ACCOUNT:
                currentStep = Steps.Name.VALIDATION;
                steps.go(currentStep.ordinal(), true);
                break;
            case VALIDATION:
                final long id;
                final AccountPersistenceService persistenceService = new AccountPersistenceService(getApplicationContext());
                try {
                    id = persistenceService.insert(data.name, data.bank.identifier, data.account.type, data.identifier );
                } catch(RuntimeException | UniqueConstraintViolationException ex) {
                    Oops.ERROR.logAndNotify(AccountCreateActivity.this, R.string.message_error_failed_to_create_account, ex);
                    break;
                }
                try {
                    new AccountService(this).performPostAccountCreationInitialization(persistenceService.retrieve(id));
                } catch (NoSuchElementException ex) {
                    Oops.ERROR.log(AccountCreateActivity.class, "Failed to load newly created account "+id, ex);
                }
                steps.done(true);
                if (notifyAccountCreationSuccessful) {
                    DialogBuilder.notification(AccountCreateActivity.this)
                            .addValidationButton((dialogInterface, i) -> {
                                ApplicationDataHelper.loadAccounts(AccountCreateActivity.this);
                                AccountCreateActivity.this.finish();
                            })
                            .setTitle(R.string.title_account_creation)
                            .setMessage(resourceText(R.string.message_account_created, data.name))
                            .show();
                } else {
                    ApplicationDataHelper.loadAccounts(AccountCreateActivity.this);
                    this.finish();
                }
                break;
        }
        setStepFragment(currentStep);
    }
}