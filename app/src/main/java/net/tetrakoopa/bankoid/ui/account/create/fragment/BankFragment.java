package net.tetrakoopa.bankoid.ui.account.create.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.custom.TrackSelectionAdapter;
import net.tetrakoopa.bankoid.model.BankContent;
import net.tetrakoopa.bankoid.model.bank.About;
import net.tetrakoopa.bankoid.ui.CommonFragment;
import net.tetrakoopa.bankoid.ui.account.create.AccountCreateActivity;
import net.tetrakoopa.bankoid.ui.account.create.AccountCreationBankViewModel;
import net.tetrakoopa.bankoid.ui.holder.BankHeader;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

public class BankFragment extends CommonFragment {

    public static final String ARG_SELECTION_MODE_ID = "selection-mode";

    public enum SelectionMode {
        NONE, EVENT_ONLY, PERSISTENT
    }

    private SimpleItemRecyclerViewAdapter adapter;
    private AccountCreationBankViewModel mViewModel;

     private SelectionMode selectionMode;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.account_creation_bank_fragment, container, false);
        final RecyclerView listView = rootView.findViewById(R.id.banks);

        setupRecyclerView(listView);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AccountCreationBankViewModel.class);
        selectionMode = SelectionMode.valueOf(getArguments().getString(ARG_SELECTION_MODE_ID, SelectionMode.NONE.name()));
    }

    public static class SimpleItemRecyclerViewAdapter
            extends TrackSelectionAdapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final AccountCreateActivity parentActivity;
        private final Map<String, BankContent.Bank> mValues;
        private final List<Map.Entry<String,BankContent.Bank>> values;

        SimpleItemRecyclerViewAdapter(AccountCreateActivity parent,
                                      Map<String, About> items) {
            mValues = items.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> new BankContent.Bank(e.getKey(), e.getValue())));
            values = mValues.entrySet().stream().collect(Collectors.toList());
            parentActivity = parent;
        }


        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final BankContent.Bank bank = (BankContent.Bank) view.getTag();
                parentActivity.data.onSelectBank(bank);
            }
        };

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.bank_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final BankContent.Bank value = values.get(position).getValue();
            if (value != null ) {
                if (value.about.getLogoDrawable() != null) {
                    holder.bank.logo.setImageDrawable(value.about.getLogoDrawable());
                }
            }
            holder.bank.name.setText(value.about.name);
            holder.bank.identifier.setText(value.about.identifier);
            holder.itemView.setTag(value);
            holder.itemView.setOnClickListener(mOnClickListener);

            final BankContent.Bank selectedBank = parentActivity.data.getBank();
            holder.itemView.setActivated(selectedBank != null && value.identifier.equals(selectedBank.identifier));
        }

        @Override
        protected void onSelectionChange(int oldIndex, int newIndex) {

        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        class ViewHolder extends TrackSelectionAdapter.ViewHolder {
            final BankHeader bank;
            final TextView about;

            ViewHolder(View view) {
                super(view);
                bank = new BankHeader(view);
                about = view.findViewById(R.id.about);
            }
        }
    }
    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        adapter = new SimpleItemRecyclerViewAdapter((AccountCreateActivity)getActivity(), applicationData.getBackendInfos());
        recyclerView.setAdapter(adapter);
    }

}