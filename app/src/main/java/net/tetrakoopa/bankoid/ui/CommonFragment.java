package net.tetrakoopa.bankoid.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;

import net.tetrakoopa.bankoid.ApplicationData;
import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.debug.LogCapable;
import net.tetrakoopa.bankoid.util.ResourceHelper;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public abstract class CommonFragment extends Fragment implements LogCapable, ApplicationData.Aware, ResourceHelper.Consumer {

//	@Override
//	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//		final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.AppTheme_Dark);
//		final LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);
//		return super.onCreateView(localInflater, container, savedInstanceState);
//	}

	private Integer originalTheme = R.style.Theme_AppCompat;

	@Override
	public Context getResourcesContext() {
		return getContext();
	}

	//	public void onActivityCreated(Bundle savedInstanceState) {
//		super.onActivityCreated(savedInstanceState);
//		if (originalTheme == null) originalTheme = getOriginalTheme(getActivity());
//		get
//	}
	protected LayoutInflater getThemedInflater(@NonNull LayoutInflater inflater) {
		final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), getThemeFor(applicationData.theme().getValue()));
		return inflater.cloneInContext(contextThemeWrapper);
	}

	@SuppressLint("NewApi")
	private int getOriginalTheme(Activity activity) {
		try {
			return activity.getPackageManager().getActivityInfo(activity.getComponentName(), 0).getThemeResource();
		} catch (PackageManager.NameNotFoundException e) {
			throw new InternalError("Failed to get actual theme", e);
		}
	}

	private int getThemeFor(ApplicationTheme theme) {
		switch (theme) {
			case LIGHT: return R.style.Theme_AppCompat_Light;
			case DARK: return R.style.Theme_AppCompat;
		}
//		switch (originalTheme) {
//			case R.style.AppTheme:
//				switch (theme) {
//					case LIGHT: return originalTheme;
//					case DARK: return R.style.AppTheme_Dark;
//				}
//				break;
//			case R.style.AppTheme_NoActionBar:
//				switch (theme) {
//					case LIGHT: return originalTheme;
//					case DARK: return R.style.AppTheme_Dark_NoActionBar;
//				}
//				break;
////			case R.style.AppTheme_AppBarOverlay:
////				switch (theme) {
////					case LIGHT: return originalTheme;
////					case DARK: return R.style.AppTheme_Dark_AppBarOverlay;
////				}
////				break;
//		}
		return originalTheme;
	}

}
