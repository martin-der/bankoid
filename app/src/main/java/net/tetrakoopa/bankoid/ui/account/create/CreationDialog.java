package net.tetrakoopa.bankoid.ui.account.create;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.custom.AfterTextChangedTextWatcher;
import net.tetrakoopa.bankoid.model.AccountContent;
import net.tetrakoopa.bankoid.model.common.Constraint;
import net.tetrakoopa.bankoid.service.persistence.AccountPersistenceService;
import net.tetrakoopa.bankoid.service.persistence.CRUD;
import net.tetrakoopa.bankoid.ui.InputValidationUtil;
import net.tetrakoopa.bankoid.ui.dialog.DialogBuilder;
import net.tetrakoopa.bankoid.util.ResourceHelper;

import java.util.concurrent.atomic.AtomicReference;

public class CreationDialog {

	private final static int name_min_length = 5;
	private final static int name_max_length = 30;

	private static final Constraint[] accountNameConstraints = new Constraint[] {
		new Constraint(Constraint.Type.MIN, 3, null),
		new Constraint(Constraint.Type.MAX, 30, null)
	};

	@SuppressLint("NewApi")
	public static void pickNameAndStart(Activity activity) {

		final AtomicReference<Button> positiveButton = new AtomicReference<>();
		final AtomicReference<TextInputEditText> nameEditText = new AtomicReference<>();

		final String[] accountNames;
		try (final CRUD.DBStream<AccountContent.Account> stream = new AccountPersistenceService(activity).find()) {
			accountNames = stream.data.map(a -> a.name).toArray(String[]::new);
		}

		DialogBuilder.question(activity)
				.setTitle(R.string.title_new_account)
				.setMessage(R.string.message_account_creation_enter_name)
				.addDecisionButtons(R.string.action_create, (dialogInterface, i) -> {
					if (i == DialogInterface.BUTTON_POSITIVE) {
						Bundle arguments = new Bundle();
						arguments.putString(AccountCreateActivity.ARG_NAME, nameEditText.get().getText().toString());
						final Intent createAccount = new Intent(activity, AccountCreateActivity.class);
						createAccount.putExtras(arguments);
						activity.startActivity(createAccount);
					}
				}, null)
				.setContent(R.layout.edit_text_with_til, (holder, content) -> {
					final TextInputLayout textInputLayout = (TextInputLayout)content;
					nameEditText.set (textInputLayout.findViewById(R.id.edit_text));
					nameEditText.get().setHint(R.string.hint_account_creation_name);
					InputValidationUtil.prepareInputFilters(accountNameConstraints, nameEditText.get());
					nameEditText.get().addTextChangedListener(new AfterTextChangedTextWatcher() {
						@Override
						public void afterTextChanged(Editable editable) {
							final String text = editable.toString();
							if (text.isEmpty()) {
								positiveButton.get().setEnabled(false);
								textInputLayout.setError(null);
							} else {
								final String message = getValidationMessage(text, activity, accountNames);
								positiveButton.get().setEnabled(message == null);
								textInputLayout.setError(message);
							}
						}
					});

				})
				.setOnDialogCreated(dialog -> {
					positiveButton.set(dialog.getButton(DialogInterface.BUTTON_POSITIVE));
					positiveButton.get().setEnabled(false);
				})
				.show();

	}

	private static String getValidationMessage(String text, Context context, String[] accountNames) {
		if (text.isEmpty()) return null;

		final String formatValidationMessage = InputValidationUtil.getValidationMessage(context, accountNameConstraints, text);
		if (formatValidationMessage != null) return formatValidationMessage;

		for (String accountName : accountNames) {
			if (text.equals(accountName))
				return ResourceHelper.text(context, R.string.message_validation_duplicate_account_name);
		}

		return null;
	}
}
