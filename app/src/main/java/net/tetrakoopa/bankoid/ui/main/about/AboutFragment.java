package net.tetrakoopa.bankoid.ui.main.about;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.ui.CommonFragment;

import androidx.annotation.NonNull;

public class AboutFragment extends CommonFragment {

	private final static String sourceCodeURL = "https://gitlab.com/ouam/bank";
	private final static String financialSupportURL = "https://paypal.com/ouam";
	private final static String financialSupportPaypalLinkText = "paypal/ouam";
	private final static String financialSupportBitcoinAddress = "3J98t1WpEZ73CNmQviecrnyiWrnqRhWNLy";

	private boolean showSupport = true;

	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		//final View root = inflater.inflate(R.layout.fragment_about, container, false);
		final View root = getThemedInflater(inflater).inflate(R.layout.fragment_about, container, false);

		final String applicationName = getContext().getString(R.string.app_name);

		final ImageView logo = root.findViewById(R.id.image_app_logo);
		logo.setImageResource(R.mipmap.ic_launcher);

		final TextView description = root.findViewById(R.id.text_description);
		description.setText(resourceTextAsHtml(R.string.about_application_present, applicationName));

		final TextView noLink = root.findViewById(R.id.text_no_link);
		noLink.setText(resourceText(R.string.about_application_disclaimer_no_link, applicationName));

		final TextView noWarranty = root.findViewById(R.id.text_no_warranty);
		noWarranty.setText(resourceText(R.string.about_application_disclaimer_no_warranty, applicationName));

		final ImageView sourcesIcon = root.findViewById(R.id.icon_sources);
		sourcesIcon.setImageDrawable(new IconDrawable(getContext(), FontAwesomeIcons.fa_keyboard_o)
				.color(R.color.colorPrimary)
				.actionBarSize());

		final TextView sources = root.findViewById(R.id.text_sources);
		sources.setText(resourceTextAsHtml(R.string.about_application_sources, sourceCodeURL));
		sources.setMovementMethod(LinkMovementMethod.getInstance());

		if (showSupport) {
			final ImageView supportIcon = root.findViewById(R.id.icon_support);
			supportIcon.setImageDrawable(new IconDrawable(getContext(), FontAwesomeIcons.fa_beer)
					.color(R.color.colorPrimary)
					.actionBarSize());

			final TextView support = root.findViewById(R.id.text_support);
			support.setText(resourceTextAsHtml(R.string.about_application_support, "<a href=\""+financialSupportURL+"\">"+ financialSupportPaypalLinkText +"</a> or <a href=\"bitcoin:"+financialSupportBitcoinAddress+"?label=BAOW%20Support\">"+"Bitcoin "+financialSupportBitcoinAddress+"</a>"));
			support.setMovementMethod(LinkMovementMethod.getInstance());
		} else {
			final View support = root.findViewById(R.id.support);
			support.setVisibility(View.INVISIBLE);
		}

		return root;
	}

}