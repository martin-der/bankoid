package net.tetrakoopa.bankoid.ui.account.create.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListPopupWindow;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import net.tetrakoopa.bankoid.ApplicationDataHelper;
import net.tetrakoopa.bankoid.Oops;
import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.custom.AfterTextChangedTextWatcher;
import net.tetrakoopa.bankoid.model.BankContent;
import net.tetrakoopa.bankoid.model.bank.Config;
import net.tetrakoopa.bankoid.model.common.Constraint;
import net.tetrakoopa.bankoid.ui.CommonFragment;
import net.tetrakoopa.bankoid.ui.account.create.AccountCreateActivity;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AccountFragment extends CommonFragment {

	private BankContent.Bank bank;

	private Config config;

	private ListPopupWindow typeListPopup;
	private TextView identifierTextView;
	private TextInputLayout identifierTextViewLayout;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.account_creation_account_fragment, container, false);

		identifierTextView = rootView.findViewById(R.id.type);

		identifierTextView = rootView.findViewById(R.id.identifier);
		identifierTextViewLayout = rootView.findViewById(R.id.identifier_til);
		//final Button typeChooseButton = rootView.findViewById(R.id.type_choose);

		return rootView;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		final AccountCreateActivity parentActivity = (AccountCreateActivity)getActivity();

		//mViewModel = ViewModelProviders.of(this).get(AccountCreationBankViewModel.class);
		bank = parentActivity.data.getBank();
		try {
			config = ApplicationDataHelper.loadConfig(bank.identifier, getContext());
		} catch (IOException| ParseException ex) {
			Oops.ERROR.logAndNotify(getContext(), () -> {
				parentActivity.data.onSelectBank(null);
				parentActivity.goPreviousStep();
			}, resourceText(R.string.message_error_unable_to_use_invalid_account_with_badly_configured_backend, bank.identifier), ex);
			return;
		}

		final List<Config.Account> accounts = config.getAccounts();

		if (accounts.size()==0) {
			Oops.ERROR.logAndNotify(getContext(), () -> {
				parentActivity.data.onSelectBank(null);
				parentActivity.goPreviousStep();
			}, resourceText(R.string.message_error_unable_to_use_invalid_account_with_badly_configured_backend, bank.identifier), new IllegalStateException("No account"));
			return;
		}

		if (accounts.size()>1 && accounts.stream().anyMatch(a -> a.type==null || a.type.isEmpty())) {
			Oops.ERROR.logAndNotify(getContext(), () -> {
				parentActivity.data.onSelectBank(null);
				parentActivity.goPreviousStep();
			}, resourceText(R.string.message_error_unable_to_use_invalid_account_with_badly_configured_backend, bank.identifier), new IllegalStateException("Some accounts are unnamed"));
			return;
		}

		identifierTextView.addTextChangedListener(new AfterTextChangedTextWatcher() {
			@Override
			public void afterTextChanged(Editable editable) {
				validateIdentifierInput(editable.toString(), parentActivity.data);
			}
		});

		identifierTextView.setEnabled(false);

		typeListPopup = new ListPopupWindow(getContext());
		ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1);
		adapter.addAll(accounts.stream().map(c -> c.type).collect(Collectors.toList()));
		typeListPopup.setAnimationStyle(android.R.anim.bounce_interpolator);
		typeListPopup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				final Config.Account account = accounts.get(i);
				parentActivity.data.onChangeAccount(account);
				identifierTextView.setEnabled(true);
				typeListPopup.dismiss();
				prepareIdentifierInputConstraints(parentActivity.data.account.identifier);
				validateIdentifierInput(identifierTextView.getText().toString(), parentActivity.data);
			}
		});
		typeListPopup.setAdapter(adapter);

	}

	private void prepareIdentifierInputConstraints(Config.Account.Identifier identifier) {
		for (final Constraint constraint : identifier.constraints) {
			if (constraint.type == Constraint.Type.MAX) {
				identifierTextView.setFilters(new InputFilter[] { new InputFilter.LengthFilter(constraint.limit) });
				break;
			}
		}
	}

	private void validateIdentifierInput(String text, AccountCreateActivity.Data data) {
		String errorMessage = null;
		if (!text.isEmpty()) {
			for (final Constraint constraint : data.account.identifier.constraints) {
				switch (constraint.type) {
					case MIN:
						if (text.length()<constraint.limit) errorMessage = resourceText(R.string.message_validation_text_minimum_length, constraint.limit);
						break;
					case MAX:
						if (text.length()>constraint.limit) errorMessage = resourceText(R.string.message_validation_text_maximum_length, constraint.limit);
						break;
					case REGEX:
						if (!text.matches(constraint.regex)) errorMessage = constraint.description == null ? resourceText(R.string.message_validation_text_unmatched_regex, constraint.regex) : resourceTextOrRaw(constraint.description);
						break;
				}
			}
		}
		data.onChangeIdentifier(text, !text.isEmpty() && errorMessage != null);
		identifierTextViewLayout.setError(errorMessage);
	}

	public void openTypeChoosePopup(View view) {
		typeListPopup.setAnchorView(view);
		typeListPopup.show();
	}

}
