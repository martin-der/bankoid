package net.tetrakoopa.bankoid.ui.main.account;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import net.tetrakoopa.bankoid.ApplicationData;
import net.tetrakoopa.bankoid.Help;
import net.tetrakoopa.bankoid.TutorialConfig;
import net.tetrakoopa.bankoid.backend.SharedPreferencesBackendPreferences;
import net.tetrakoopa.bankoid.ui.CommonFragment;
import net.tetrakoopa.bankoid.backend.BackendHandler;
import net.tetrakoopa.bankoid.custom.PersistableFragment;
import net.tetrakoopa.bankoid.model.AccountContent;
import net.tetrakoopa.bankoid.ui.dialog.DialogBuilder;
import net.tetrakoopa.bankoid.util.SingletonViewModelFactory;
import net.tetrakoopa.bankoid.util.ViaURLClientViewJSExecutor;
import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;
import net.tetrakoopa.bankoid.backend.ui.model.AccountWebViewModel;
import net.tetrakoopa.bankoid.backend.util.ClientViewJSExecutor;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.preference.PreferenceManager;

public class AccountFragment extends CommonFragment implements PersistableFragment {

	private AccountWebViewModel accountWebViewModel;

	private final static String USER_AGENT_STRING = "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Mobile Safari/537.36";

	/**
	 * Add and remove <code>webView</code> in <code>onCreateView</code>/<code>onDestroyView</code> in order to make its content persistent
	 */
	private WebView webView;
	private FloatingActionButton fabLogout;
	private SharedPreferences preferences;
	private ClientViewJSExecutor executor;

	private JSAccountAgent jsAccountAgent;

	private BackendHandler<?> backendHandler;
	private boolean backendHandlerReady;

	private AccountContent.Account runningAccount;

	private Observer<Boolean> loggedObserver;

	private final TutorialConfig tutorialConfig = applicationData.tutorialConfig();

	public final ApplicationData.OnAccountChangeListener accountChangeListener = account -> {
		handleBackendChange(account);
	};

	public AccountFragment() {
		setRetainInstance(true);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		webView = new WebView(getActivity());
		webView.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT));
		prepareWebViewAndDependency();
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		final View root = inflater.inflate(R.layout.fragment_account, container, false);

		((LinearLayout)root).addView(webView, 0);

		preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

		fabLogout = root.findViewById(R.id.fab_logout);
		fabLogout.setImageDrawable(new IconDrawable(getContext(), FontAwesomeIcons.fa_power_off)
				.color(Color.WHITE)
				.actionBarSize());
		fabLogout.setVisibility(View.INVISIBLE);
		fabLogout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (preferences.getBoolean("ui_confirm_logoff", true)) {
					Snackbar.make(view, R.string.message_click_to_confirm_logout, Snackbar.LENGTH_LONG)
							.setAction(R.string.action_logout, view1 -> logout()).show();
				} else {
					logout();
				}
			}
		});

		loggedObserver = logged -> fabLogout.setVisibility(Boolean.TRUE.equals(logged) ? View.VISIBLE:View.INVISIBLE);

		applicationData.registerOnAccountChangeListener(accountChangeListener);

		return root;
	}


	private void prepareWebViewAndDependency() {

		prepareWebView();

		executor = new ViaURLClientViewJSExecutor(webView);

		jsAccountAgent = new JSAccountAgent(executor);

		webView.addJavascriptInterface(new Object() {
			@JavascriptInterface
			public void onError(String errorMessage, String file, String lineNumber){
				int lineNumberAsInt;
				try {
					lineNumberAsInt = Integer.parseInt(lineNumber);
				} catch (NumberFormatException|NullPointerException ex) {
					lineNumberAsInt = 0;
				}
				logger.log(MessageLogger.LogMessage.Severity.ERROR, MessageLogger.LogMessage.Type.WEB_JS, new MessageLogger.LogMessage.Location(file,lineNumberAsInt), "Thrown exception", errorMessage);
			}
		}, "Android");

		webView.addJavascriptInterface(jsAccountAgent, "accountAgent");
	}

	private void prepareWebView() {

		webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		webView.getSettings().setJavaScriptEnabled(true);
		if (android.os.Build.VERSION.SDK_INT >= 21) {
			CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
		} else {
			CookieManager.getInstance().setAcceptCookie(true);
		}
		webView.getSettings().setDomStorageEnabled(true);
		webView.getSettings().setDatabaseEnabled(true);
		webView.getSettings().setUserAgentString(USER_AGENT_STRING);
		//webView.getSettings().setAppCachePath(getContext().getFilesDir().getAbsolutePath() + "/cache");


		final WebViewClient webViewClient = new WebViewClient() {

			boolean loadingFinished = true;
			boolean redirect = false;

			@Override
			public boolean shouldOverrideUrlLoading(
					WebView view, WebResourceRequest request) {
				if (!loadingFinished) {
					redirect = true;
				}

				loadingFinished = false;
				view.loadUrl(request.getUrl().toString());
				return true;
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				loadingFinished = false;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				if (!redirect) {
					loadingFinished = true;

					logger.log(MessageLogger.LogMessage.Severity.DEBUG, MessageLogger.LogMessage.Type.ACTIVITY, "WebView", "URL Changed", url == null ?"<null>" : url);

					if (accountWebViewModel != null) {
						accountWebViewModel.getUrl().postValue(url);
					}

					if (url != null) {
						handleURLChanged(url);
					}
				} else {
					redirect = false;
				}
			}

		};

		webView.setWebChromeClient(new WebChromeClient() {
			@Override
			public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
				logger.log(consoleMessageLevelToSeverity(consoleMessage.messageLevel()), MessageLogger.LogMessage.Type.WEB_JS, new MessageLogger.LogMessage.Location(consoleMessage.sourceId(), consoleMessage.lineNumber()), "Log", consoleMessage.message());
				return super.onConsoleMessage(consoleMessage);
			}
		});

		webView.setWebViewClient(webViewClient);

	}

	@Override
	public void onDestroyView() {
		super.onDestroy();
		applicationData.unregisterOnAccountChangeListener(accountChangeListener);
		((ViewGroup)webView.getParent()).removeView(webView);
	}

	@Override
	public void onStart() {
		super.onStart();
		prepareBackendIfNeeded(applicationData.currentAccount());
	}

	@Override
	public void onStop() {
		super.onStop();
	}


	@Override
	public void onResume() {
		super.onResume();

		tutorialConfig.aboutCreateAccountShown = preferences.getBoolean(Help.Key.ABOUT_CREATE_ACCOUNT_SHOWN, false);

		if (backendHandlerReady) {
			startAccount();
		} else {
			if (!tutorialConfig.aboutCreateAccountShown) {
				DialogBuilder.tutorial(getContext())
					.setTitle(R.string.message_tutorial_first_run_welcome)
					.setMessage(R.string.message_tutorial_no_account_found)
					.setNeutralButton(android.R.string.ok, (dialogInterface, i) -> {
						tutorialConfig.aboutCreateAccountShown = true;
						preferences.edit().putBoolean(Help.Key.ABOUT_CREATE_ACCOUNT_SHOWN, true).commit();
					}).show();
			}
		}
	}

	private AccountContent.Account previousAccount;

	private void prepareBackendIfNeeded(AccountContent.Account account) {
		if (previousAccount == null && account == null) return;
		if (previousAccount != null && account != null) {
			if (account.id == previousAccount.id) return;
		}
		handleBackendChange(account);
	}

	private void handleBackendChange(AccountContent.Account account) {
		previousAccount = account;
		if (account == null) {
			logger.log(MessageLogger.LogMessage.Severity.INFO, AccountFragment.class, "No account selected");
			webView.loadData("", "text/html", null);
			backendHandlerReady = false;
			return;
		}
		logger.log(MessageLogger.LogMessage.Severity.INFO, AccountFragment.class, "Account changed");
		initBackendHandler();
		startAccount();
	}

	private void initBackendHandler() {
		final ApplicationData.Backend backend = applicationData.currentBackend();

		accountWebViewModel = ViewModelProviders.of(this, new SingletonViewModelFactory(backend.model, AccountWebViewModel.class)).get(AccountWebViewModel.class);
		accountWebViewModel.getLogged().postValue(false);
		accountWebViewModel.getUrl().postValue(null);

		backendHandler = applicationData.currentBackend().handler;
		backendHandler.setContextRelatedObjects(getContext(), getActivity().getAssets(), webView, new SharedPreferencesBackendPreferences(backend.name, preferences, backend.settingsTemplate), executor);

		accountWebViewModel.getLogged().observe(getViewLifecycleOwner(), loggedObserver);


		backendHandlerReady = true;
	}

	private void startAccount() {
		final AccountContent.Account currentAccount = applicationData.currentAccount();
		if (runningAccount != currentAccount) {
			runningAccount = currentAccount;

			logger.log(MessageLogger.LogMessage.Severity.INFO, AccountFragment.class, "Start Account Web Manager");

			jsAccountAgent.setAccount(runningAccount.identifier);
			webView.loadUrl(backendHandler.getEntry().toString());
		}
	}

	private void handleURLChanged(String url) {
		if (!backendHandlerReady) return;

		if (url.startsWith("data:text/html")) {
			accountWebViewModel.getLogged().postValue(false);
			return;
		}

		backendHandler.urlChanged(url);
	}

	private void logout() {
		logger.log(MessageLogger.LogMessage.Severity.INFO, AccountFragment.class, "Logout");
		executor.execute("accountAgent.logout");
		//backendHandler.perform(BackendHandler.UserAction.LOGOUT);
	}

	private static MessageLogger.LogMessage.Severity consoleMessageLevelToSeverity(ConsoleMessage.MessageLevel messageLevel) {
		if (messageLevel==null) return null;
		switch (messageLevel) {
			case DEBUG: return MessageLogger.LogMessage.Severity.TRACE;
			case LOG: return MessageLogger.LogMessage.Severity.DEBUG;
			case TIP: return MessageLogger.LogMessage.Severity.INFO;
			case WARNING: return MessageLogger.LogMessage.Severity.WARNING;
			case ERROR: return MessageLogger.LogMessage.Severity.ERROR;
			default:
				//Log.w("")
				return null;
		}
	}

	@Override
	public boolean isPersistent() {
		return true;
	}
}