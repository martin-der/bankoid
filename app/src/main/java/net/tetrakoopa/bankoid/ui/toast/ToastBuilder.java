package net.tetrakoopa.bankoid.ui.toast;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.tetrakoopa.bankoid.R;

public class ToastBuilder {

	private static class Holder {
		final TextView message;
		final ImageView icon;

		private Holder(TextView message, ImageView icon) {
			this.message = message;
			this.icon = icon;
		}
	}

	public static ToastBuilder withIcon(Context context, int duration) {
		final ToastBuilder builder = new ToastBuilder(context);
		final Toast toast = builder.toast;

		toast.setDuration(duration);

		return builder;
	}

	private final Toast toast;

	private final Holder holder;

	public ToastBuilder(Context context) {
		this.toast = new Toast(context);

		final LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		final View root = inflater.inflate(R.layout.toast_with_icon,null);

		final TextView message = root.findViewById(R.id.text);
		final ImageView icon = root.findViewById(R.id.icon);

		holder = new Holder(message, icon);

		toast.setView(root);
	}

	public ToastBuilder setMessage(CharSequence text) {
		holder.message.setText(text);
		return this;
	}
	public ToastBuilder setMessage(int id) {
		holder.message.setText(id);
		return this;
	}

	public ToastBuilder setIcon(Drawable drawable) {
		holder.icon.setImageDrawable(drawable);
		return this;
	}
	public ToastBuilder setIcon(int id) {
		holder.icon.setImageResource(id);
		return this;
	}

	public ToastBuilder create() {
		// NOP
		return this;
	}

	public void show() {
		create();
		toast.show();
	}

}
