package net.tetrakoopa.bankoid.ui;

import net.tetrakoopa.bankoid.util.EnumUtil;

public enum ApplicationTheme {
	LIGHT, DARK;

	public static ApplicationTheme fromName(String name) throws EnumUtil.InvalidEnumCriteriaException.Name {
		return EnumUtil.fromName(ApplicationTheme.class, name);
	}
	public static ApplicationTheme fromName(String name, ApplicationTheme defaultz) {
		return EnumUtil.fromName(ApplicationTheme.class, name, defaultz);
	}
}
