package net.tetrakoopa.bankoid.ui.dialog.content;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.tetrakoopa.bankoid.R;

public class HeaderBodyFooterHolder<V extends View> extends OneViewHolder<V> {

	private final static int layoutId = R.layout.dialog_content_header_body_footer;
	private final static int viewStubId = R.id.dialog_body_stub;

	public enum ViewMarker implements IdAwareMarker {
		HEADER(R.id.dialog_header), FOOTER(R.id.dialog_footer);

		final int id;

		ViewMarker(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
	}

	private TextView header;

	private TextView footer;

	public HeaderBodyFooterHolder(int contentLayoutId, LayoutInflater inflater, ViewGroup parent) {
		super(layoutId, viewStubId, contentLayoutId, inflater, parent);

		header = root.findViewById(R.id.dialog_header);
		footer = root.findViewById(R.id.dialog_footer);

	}

	public View getRoot() {
		return root;
	}

	@Message
	public TextView getHeader() {
		return header;
	}

	public TextView getFooter() {
		return footer;
	}

}
