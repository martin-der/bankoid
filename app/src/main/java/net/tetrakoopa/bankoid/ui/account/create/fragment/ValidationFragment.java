package net.tetrakoopa.bankoid.ui.account.create.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.model.BankContent;
import net.tetrakoopa.bankoid.model.bank.Config;
import net.tetrakoopa.bankoid.ui.CommonFragment;
import net.tetrakoopa.bankoid.ui.account.create.AccountCreateActivity;
import net.tetrakoopa.bankoid.ui.holder.BankHeader;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ValidationFragment extends CommonFragment {

	private BankContent.Bank bank;

	private Config config;

	private TextView nameView;
	private BankHeader bankHeaderView;
	private View typeRow;
	private TextView typeView;
	private TextView identifierView;
	private TextView textView;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.account_creation_validation_fragment, container, false);

		nameView = rootView.findViewById(R.id.name);
		bankHeaderView = new BankHeader(rootView);
		typeRow = rootView.findViewById(R.id.type_row);
		typeView = rootView.findViewById(R.id.type);
		identifierView = rootView.findViewById(R.id.identifier);
		textView = rootView.findViewById(R.id.text);

		return rootView;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		//mViewModel = ViewModelProviders.of(this).get(AccountCreationBankViewModel.class);
		final AccountCreateActivity.Data data = ((AccountCreateActivity)getActivity()).data;

		nameView.setText(data.getName());

		bank = data.getBank();

		bankHeaderView.name.setText(bank.about.name);
		if (bank.about.getLogoDrawable() != null) {
			bankHeaderView.logo.setImageDrawable(bank.about.getLogoDrawable());
		}

		if (data.getAccount()!=null) {
			typeView.setText(data.getAccount().type == null ? "<unknwon" : data.getAccount().type);
			typeRow.setVisibility(View.VISIBLE);
		} else {
			typeRow.setVisibility(View.INVISIBLE);
		}

		identifierView.setText(data.getIdentifier());

		textView.setText(R.string.message_account_creation_read_before_click_next);
	}

}
