package net.tetrakoopa.bankoid.ui.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import net.tetrakoopa.bankoid.R;

public class BankHeader {

	public final ImageView logo;
	public final TextView name;
	public final TextView identifier;

	public BankHeader(View root) {
		logo = root.findViewById(R.id.bank_header_logo);
		name = root.findViewById(R.id.bank_header_name);
		identifier = root.findViewById(R.id.bank_header_identifier);
	}

}
