package net.tetrakoopa.bankoid.ui.debug;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.joanzapata.iconify.IconDrawable;

import net.tetrakoopa.bankoid.model.LogMessageContent;
import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;

import androidx.fragment.app.Fragment;

import static net.tetrakoopa.bankoid.util.JavaLangUtil.exceptionToText;

/**
 * A fragment representing a single LogMessage detail screen.
 * This fragment is either contained in a {@link LogMessageListActivity}
 * in two-pane mode (on tablets) or a {@link LogMessageDetailActivity}
 * on handsets.
 */
public class LogMessageDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "item_id";

    private MessageLogger.LogMessage message;

    AppBarLayout.OnOffsetChangedListener offsetChangedListener = new AppBarLayout.OnOffsetChangedListener() {
        @Override
        public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
            Log.i("LogMessageDetailFrment", "verticalOffset : "+verticalOffset);
            if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                // Collapsed
            } else if (verticalOffset == 0) {
                // Expanded
            } else {
                // Somewhere in between
            }
        }
    };

//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        AppBarLayout mAppBarLayout = getView().findViewById(R.id.tool_bar);
//        mAppBarLayout.addOnOffsetChangedListener(offsetChangedListener);
//    }
//
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            message = LogMessageContent.getItemMap().get(getArguments().getLong(ARG_ITEM_ID));

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {

//                AppBarLayout mAppBarLayout = getView().findViewById(R.id.tool_bar);
//                mAppBarLayout.addOnOffsetChangedListener(offsetChangedListener);

//                appBarLayout.addOnOffsetChangedListener(offsetChangedListener);

//                appBarLayout.setAnimation(new Animation() {
//                });

                appBarLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View view, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        Log.i("LogMessageDetailFragment", "Layout new : "+left+", "+top+", "+right+", "+bottom+" old : "+oldLeft+", "+oldTop+", "+oldRight+", "+oldBottom);
                    }
                });


                appBarLayout.setTitle(message.content);
                if (message.severity != null) {
                    final LogMessageListActivity.SeverityDepiction depiction = LogMessageListActivity.getDepiction(message.severity);
                    ImageView imageView = appBarLayout.findViewById(R.id.severity_icon);
                    // FIXME preload all severity image drawables
                    imageView.setImageDrawable(new IconDrawable(getContext(), depiction.icon)
                            .color(depiction.color)
                            .actionBarSize());
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.logmessage_detail, container, false);

        if (message != null) {
            if (message.location != null)
                ((TextView) rootView.findViewById(R.id.location)).setText(message.location.where+":"+message.location.index);
            ((TextView) rootView.findViewById(R.id.detail)).setText(message.details);

            if (message.type != null) {
                final LogMessageListActivity.TypeDepiction depiction = LogMessageListActivity.getDepiction(message.type);
                final ImageView imageView = rootView.findViewById(R.id.type);
                // FIXME preload all type image drawables
                imageView.setImageDrawable(new IconDrawable(getContext(), depiction.icon)
                        .color(depiction.color)
                        .actionBarSize());
                imageView.setAlpha(0.25f);

            }

            final TextView causeTextView = rootView.findViewById(R.id.exception);
            if (message.cause != null) {
                causeTextView.setText(exceptionToText(message.cause));
            } else {
                causeTextView.setVisibility(View.INVISIBLE);
            }
        }

        return rootView;
    }

 }