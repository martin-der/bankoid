package net.tetrakoopa.bankoid.ui.dialog;

import android.content.Context;
import android.view.View;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.ui.dialog.content.HeaderBodyFooterHolder;
import net.tetrakoopa.bankoid.ui.dialog.content.OneViewHolder;
import net.tetrakoopa.bankoid.ui.dialog.nature.NatureTintCapable;
import net.tetrakoopa.bankoid.ui.dialog.nature.NatureTintHolder;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

public abstract class RegularOneViewDialogBuilder<C extends View, H extends OneViewHolder<C>, B extends RegularOneViewDialogBuilder<C, H, B>> extends OneViewHolderDialogBuilder<C, H, B> {

	private SeverityTintHolder.Severity severity;

	public static class SeverityTintHolder extends NatureTintHolder<HeaderBodyFooterHolder.ViewMarker> implements NatureTintCapable.Holder<HeaderBodyFooterHolder.ViewMarker> {
		public String getName() { return "SEVERITY"; }

		public enum Severity {
			SUCCESS(R.drawable.text_severity_success), NOTIFICATION(R.drawable.text_severity_notice), WARNING(R.drawable.text_severity_warning), ERROR(R.drawable.text_severity_error);

			private final int background;

			Severity(int background) {
				this.background = background;
			}

			public int getBackground() {
				return background;
			}
		}
	}

	public RegularOneViewDialogBuilder(@NonNull Context context) {
		super(context);
		defaultRegularInit(context);
	}

	public RegularOneViewDialogBuilder(@NonNull Context context, int themeResId) {
		super(context, themeResId);
		defaultRegularInit(context);
	}

	public B setSeverity(SeverityTintHolder.Severity severity) {
		this.severity = severity;
		return (B) this;
	}

	private final void defaultRegularInit(Context context) {
		this
			.setIcon(R.drawable.ic_pig_face)
			.setCancelable(true);
	}

	@Override
	protected final void oneViewHolderDialogBuilderCreate(H holder) {
		addNatureHolder(new RegularOneViewDialogBuilder.SeverityTintHolder(), view -> {
			if (severity != null) {
				view.setBackground(ContextCompat.getDrawable(getContext(), severity.getBackground()));
			}
		});
	}
}
