package net.tetrakoopa.bankoid.ui.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import net.tetrakoopa.bankoid.Oops;
import net.tetrakoopa.bankoid.ui.dialog.content.Holder;
import net.tetrakoopa.bankoid.ui.dialog.content.OneViewHolder;
import net.tetrakoopa.bankoid.ui.dialog.nature.NatureTintCapable;
import net.tetrakoopa.bankoid.util.JavaLangUtil;

import org.springframework.core.GenericTypeResolver;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

@SuppressLint("NewApi")
public abstract class OneViewHolderDialogBuilder<C extends View, H extends OneViewHolder<C>, B extends OneViewHolderDialogBuilder<C,H,B>> extends CommonDialogBuilder<B> {

	public interface OnContentCreated<C extends View, H extends OneViewHolder<C>> {
		void contentCreated(H holder, C content);
	}
	
	private final static Class<?>[] holderConstructorParameters = new Class<?>[] { int.class, LayoutInflater.class, ViewGroup.class }; 

	private boolean useCustomContentView = false;

	private final Class<H> holderClass;
	private H holder;

	private Method viewAccessorForMessageRecipient;

	private Map<String, NatureTintCapable.Applier> natureAppliers = new HashMap<>();

	private int contentLayoutId;

	private OnContentCreated<C, H> onContentCreated;

	public OneViewHolderDialogBuilder(@NonNull Context context) {
		super(context);
		this.holderClass = resolveHolderClass(this.getClass());
		this.viewAccessorForMessageRecipient = getViewAccessorForMessageRecipient(holderClass);
	}

	public OneViewHolderDialogBuilder(@NonNull Context context, int themeResId) {
		super(context, themeResId);
		this.holderClass = resolveHolderClass(this.getClass());
		this.viewAccessorForMessageRecipient = getViewAccessorForMessageRecipient(holderClass);
	}

	private static <C extends View, H extends OneViewHolder<C>, B extends OneViewHolderDialogBuilder<C,H,B>, E extends OneViewHolderDialogBuilder<C, H, B>> Class<H> resolveHolderClass(Class<E> e) {
		final Class<?>[] types = GenericTypeResolver.resolveTypeArguments(e, OneViewHolderDialogBuilder.class);
		return (Class<H>) types[1];
	}

	private final void initContent(Context context, int contentLayoutId) {
		final Constructor<H> constructor;
		try {
			constructor = holderClass.getConstructor(holderConstructorParameters);
		} catch (NoSuchMethodException e) {
			throw new IllegalStateException("Holder "+holderClass.getName()+" has no public constructor "+ JavaLangUtil.parametersTypeAsString(holderConstructorParameters), e);
		}
		try {
			holder = constructor.newInstance(contentLayoutId, LayoutInflater.from(context), null);
		} catch (IllegalAccessException|InstantiationException|InvocationTargetException e) {
			throw new InternalError("Failed to instantiate Holder "+holderClass.getName()+": "+e.getMessage(), e);
		}


		setView(holder.getRoot());
	}

	private static Method getViewAccessorForMessageRecipient(Class<?> holderClass) {
		for (Method method : holderClass.getDeclaredMethods()) {
			if (method.getParameterCount()==0 && method.getReturnType().isAssignableFrom(EditText.class) ) {
				final int modifiers = method.getModifiers();
				if (Modifier.isPublic(modifiers) && !Modifier.isStatic(modifiers)) {
					if (method.getAnnotation(Holder.Message.class) != null)
						return method;
				}
			}
		}
		return null;
	}

	public AlertDialog create() {
		if (useCustomContentView) {
			initContent(getContext(), contentLayoutId);
			if (viewAccessorForMessageRecipient != null) {
				if (params.message != null) {
					setTextToTextViewActingAsMessageDisplay(params.message);
				}
			}
			oneViewHolderDialogBuilderCreate(holder);
			if (onContentCreated != null) onContentCreated.contentCreated(holder, holder.getContent());
			for (Map.Entry<String, NatureTintCapable.Applier> applierEntry : natureAppliers.entrySet()) {
				applyNature(applierEntry.getKey(), applierEntry.getValue());
			}
		} else {
			super.setMessage(params.message);
		}
		final AlertDialog dialog = super.create();
		return dialog;
	}

	private void setTextToTextViewActingAsMessageDisplay(CharSequence message) {
		final TextView textView;
		try {
			textView = (TextView) viewAccessorForMessageRecipient.invoke(holder);
		} catch (IllegalAccessException|InvocationTargetException ex) {
			Oops.ERROR.log(OneViewHolderDialogBuilder.class, "Failed to get TextView acting as message display", ex);
			return;
		}
		textView.setText(message);
	}

	protected abstract void oneViewHolderDialogBuilderCreate(H holder);

	protected <M extends Enum<M> & NatureTintCapable.IdAwareMarker> void addNatureHolder(NatureTintCapable.Holder<M> ntHolder, NatureTintCapable.Applier applier) {
		final String name = ntHolder.getName();
		holder.addNatureHolder(name, ntHolder);
		natureAppliers.put(name, applier);
	}
	protected void applyNature(String name, NatureTintCapable.Applier applier) {
		NatureTintCapable.Holder<?> natureHolder = holder.getNatureHolder(name);
		for (View view : natureHolder.getMarkedViews(holder.getRoot())) {
			applier.apply(view);
		}
	}

	public B setContentLayout(int layoutId) {
		this.contentLayoutId = layoutId;
		useCustomContentView = true;
		return (B)this;
	}
	public B setContent(int layoutId, OnContentCreated<C, H> onContentCreated) {
		this.contentLayoutId = layoutId;
		useCustomContentView = true;
		this.onContentCreated = onContentCreated;
		return (B)this;
	}
	public B setOnContentCreatedListener(OnContentCreated<C, H> onContentCreated) {
		this.onContentCreated = onContentCreated;
		return (B)this;
	}

	public B setMessage(int stringId) {
		params.message = getContext().getText(stringId);
		useCustomContentView = false;
		return (B)this;
	}
	public B setMessage(CharSequence text) {
		params.message = text;
		useCustomContentView = false;
		return (B)this;
	}
	
}
