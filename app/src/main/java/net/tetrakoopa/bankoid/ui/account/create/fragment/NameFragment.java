package net.tetrakoopa.bankoid.ui.account.create.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.tetrakoopa.bankoid.ApplicationDataHelper;
import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.model.BankContent;
import net.tetrakoopa.bankoid.model.bank.Config;
import net.tetrakoopa.bankoid.ui.CommonFragment;
import net.tetrakoopa.bankoid.ui.account.create.AccountCreateActivity;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class NameFragment extends CommonFragment {

	private BankContent.Bank bank;

	private Config config;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.account_creation_account_fragment, container, false);

		return rootView;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		//mViewModel = ViewModelProviders.of(this).get(AccountCreationBankViewModel.class);
		bank = ((AccountCreateActivity)getActivity()).data.getBank();
		try {
			config = ApplicationDataHelper.loadConfig(bank.identifier, getContext());
		} catch (IOException| ParseException e) {
			e.printStackTrace();
		}
	}

}
