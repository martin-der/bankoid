package net.tetrakoopa.bankoid.ui.debug;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import net.tetrakoopa.bankoid.model.LogMessageContent;
import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.ui.CommonActivity;
import net.tetrakoopa.bankoid.util.ResourceHelper;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import androidx.appcompat.app.ActionBar;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * An activity representing a list of Log Messages. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link LogMessageDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class LogMessageListActivity extends CommonActivity {

	public static final String ARG_ITEM_ID = "item_id";

	private boolean mTwoPane;

	private MenuItem menuActionClear;
	private MenuItem menuActionFilter;

	private DateFormat dateLogFormat;
	private DateFormat timeLogFormat;
	private RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> logMessagesAdapter;

	public static final class SeverityDepiction {
		public final FontAwesomeIcons icon;
		public final int color;

		private SeverityDepiction(FontAwesomeIcons icon, int color) {
			this.icon = icon;
			this.color = color;
		}
	}

	private static SeverityDepiction SEVERITY_DEPICTION_TRACE;
	private static SeverityDepiction SEVERITY_DEPICTION_DEBUG;
	private static SeverityDepiction SEVERITY_DEPICTION_INFO;
	private static SeverityDepiction SEVERITY_DEPICTION_WARNING;
	private static SeverityDepiction SEVERITY_DEPICTION_ERROR;
	private static SeverityDepiction SEVERITY_DEPICTION_FATAL;

	public static SeverityDepiction getDepiction(MessageLogger.LogMessage.Severity severity) {
		switch (severity) {
			case TRACE: return SEVERITY_DEPICTION_TRACE;
			case DEBUG: return SEVERITY_DEPICTION_DEBUG;
			case INFO: return SEVERITY_DEPICTION_INFO;
			case WARNING: return SEVERITY_DEPICTION_WARNING;
			case ERROR: return SEVERITY_DEPICTION_ERROR;
			case FATAL: return SEVERITY_DEPICTION_FATAL;
		}
		return null;
	}

	public static final class TypeDepiction {
		public final FontAwesomeIcons icon;
		public final int color;

		private TypeDepiction(FontAwesomeIcons icon, int color) {
			this.icon = icon;
			this.color = color;
		}
	}

	private static TypeDepiction TYPE_DEPICTION_ACTIVITY;
	private static TypeDepiction TYPE_DEPICTION_JAVASCRIPT;

	public static TypeDepiction getDepiction(MessageLogger.LogMessage.Type type) {
		switch (type) {
			case WEB_JS: return TYPE_DEPICTION_JAVASCRIPT;
			case ACTIVITY: return TYPE_DEPICTION_ACTIVITY;
		}
		return null;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_logmessage_list);

		final Resources.Theme theme = getTheme();

		SEVERITY_DEPICTION_TRACE = new SeverityDepiction(FontAwesomeIcons.fa_bug, ResourceHelper.getColor(theme, R.attr.color_severity_trace));
		SEVERITY_DEPICTION_DEBUG = new SeverityDepiction(FontAwesomeIcons.fa_bug, ResourceHelper.getColor(theme, R.attr.color_severity_debug));
		SEVERITY_DEPICTION_INFO = new SeverityDepiction(FontAwesomeIcons.fa_info_circle, ResourceHelper.getColor(theme, R.attr.color_severity_info));
		SEVERITY_DEPICTION_WARNING = new SeverityDepiction(FontAwesomeIcons.fa_exclamation_triangle, ResourceHelper.getColor(theme, R.attr.color_severity_warning));
		SEVERITY_DEPICTION_ERROR = new SeverityDepiction(FontAwesomeIcons.fa_exclamation_triangle, ResourceHelper.getColor(theme, R.attr.color_severity_error));
		SEVERITY_DEPICTION_FATAL = new SeverityDepiction(FontAwesomeIcons.fa_bomb, ResourceHelper.getColor(theme, R.attr.color_severity_fatal));

		TYPE_DEPICTION_ACTIVITY = new TypeDepiction(FontAwesomeIcons.fa_android, ResourceHelper.getColor(theme, R.attr.color_message_type_activity));
		TYPE_DEPICTION_JAVASCRIPT = new TypeDepiction(FontAwesomeIcons.fa_html5, ResourceHelper.getColor(theme, R.attr.color_message_type_javascript));

		final ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}

		dateLogFormat = android.text.format.DateFormat.getDateFormat(getBaseContext());
		timeLogFormat = new SimpleDateFormat("HH:mm:ss.SSS");

		if (findViewById(R.id.logmessage_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-w900dp).
			// If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;
		}

		final RecyclerView recyclerView = findViewById(R.id.logmessage_list);
		logMessagesAdapter = new SimpleItemRecyclerViewAdapter(this, LogMessageContent.getItems(), mTwoPane, dateLogFormat, timeLogFormat);
		recyclerView.setAdapter(logMessagesAdapter);

		final long logMessageId = getIntent().getLongExtra(ARG_ITEM_ID, -1);
		if (logMessageId >= 0) {
			Intent intent = new Intent(this, LogMessageDetailActivity.class);
			intent.putExtra(LogMessageDetailFragment.ARG_ITEM_ID, logMessageId);
			startActivity(intent);

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.debug_logs, menu);

		final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		menuActionClear = menu.findItem(R.id.action_clear);
		menuActionClear.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_trash)
				.color(Color.WHITE)
				.actionBarSize());

		menuActionFilter = menu.findItem(R.id.action_filter);
		menuActionFilter.setIcon(new IconDrawable(this, FontAwesomeIcons.fa_filter)
				.color(Color.WHITE)
				.actionBarSize());

		return true;
	}


	public static class SimpleItemRecyclerViewAdapter
			extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

		private final LogMessageListActivity mParentActivity;
		private final List<MessageLogger.LogMessage> mValues;
		private final boolean mTwoPane;

		private final DateFormat timeLogFormat;
		private final DateFormat dateLogFormat;

		private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				MessageLogger.LogMessage item = (MessageLogger.LogMessage) view.getTag();
				if (mTwoPane) {
					Bundle arguments = new Bundle();
					arguments.putString(LogMessageDetailFragment.ARG_ITEM_ID, String.valueOf(item.id));
					LogMessageDetailFragment fragment = new LogMessageDetailFragment();
					fragment.setArguments(arguments);
					mParentActivity.getSupportFragmentManager().beginTransaction()
							.replace(R.id.logmessage_detail_container, fragment)
							.commit();
				} else {
					Context context = view.getContext();
					Intent intent = new Intent(context, LogMessageDetailActivity.class);
					intent.putExtra(LogMessageDetailFragment.ARG_ITEM_ID, item.id);

					context.startActivity(intent);
				}
			}
		};

		SimpleItemRecyclerViewAdapter(LogMessageListActivity parent,
									  List<MessageLogger.LogMessage> items,
									  boolean twoPane, DateFormat dateLogFormat, DateFormat timeLogFormat) {
			mValues = items;
			mParentActivity = parent;
			mTwoPane = twoPane;
			this.dateLogFormat = dateLogFormat;
			this.timeLogFormat = timeLogFormat;
		}

		@Override
		public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View view = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.logmessage_list_content, parent, false);
			return new ViewHolder(view);
		}

		@Override
		public void onBindViewHolder(final ViewHolder holder, int position) {
			final MessageLogger.LogMessage message = mValues.get(position);
			//holder.mIdView.setText(String.valueOf(message.id));
			if (message.severity != null) {
				final SeverityDepiction depiction = getDepiction(message.severity);
				// FIXME preload all severity image drawables
				holder.severity.setImageDrawable(new IconDrawable(mParentActivity.getBaseContext(), depiction.icon)
				.color(depiction.color)
				.actionBarSize());
			}
			if (message.type != null) {
				final TypeDepiction depiction = getDepiction(message.type);
				// FIXME preload all severity image drawables
				holder.type.setImageDrawable(new IconDrawable(mParentActivity.getBaseContext(), depiction.icon)
						.color(depiction.color)
						.actionBarSize());

			}
			holder.date.setText(message.date == null ? "" : dateLogFormat.format(message.date));
			holder.time.setText(message.date == null ? "" : timeLogFormat.format(message.date));

			holder.mContentView.setText(message.content);

			holder.itemView.setTag(mValues.get(position));
			holder.itemView.setOnClickListener(mOnClickListener);
		}

		@Override
		public int getItemCount() {
			return mValues.size();
		}

		class ViewHolder extends RecyclerView.ViewHolder {
			//final TextView mIdView;
			final ImageView severity;
			final TextView date;
			final TextView time;
			final ImageView type;
			final TextView mContentView;

			ViewHolder(View view) {
				super(view);
				//mIdView = (TextView) view.findViewById(R.id.id_text);
				severity = (ImageView) view.findViewById(R.id.severity);
				date = (TextView) view.findViewById(R.id.date);
				time = (TextView) view.findViewById(R.id.time);
				type = (ImageView) view.findViewById(R.id.type);
				mContentView = (TextView) view.findViewById(R.id.content);
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		final int id = item.getItemId();

		if (id == android.R.id.home) {
			this.finish();
			return true;
		}

		switch (item.getItemId()) {
			case R.id.action_clear:
				LogMessageContent.clear();
				logMessagesAdapter.notifyDataSetChanged();
				return true;
			case R.id.action_filter:
				startActivity(new Intent(LogMessageListActivity.this, LogMessageListFilterActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

}