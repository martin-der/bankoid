package net.tetrakoopa.bankoid.ui.dialog;

import android.content.Context;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import androidx.annotation.NonNull;

public class TutorialDialogBuilder extends CommonDialogBuilder<TutorialDialogBuilder> {

	public enum DoNotShowAgainPolicy {
		NOW, USER_REQUEST
	}

	private DoNotShowAgainPolicy doNotShowAgainPolicy;

	public TutorialDialogBuilder(@NonNull Context context) {
		super(context);
		defaultInit(context);
	}

	public TutorialDialogBuilder(@NonNull Context context, int themeResId) {
		super(context, themeResId);
		defaultInit(context);
	}

	private final void defaultInit(Context context) {
		this
			.setIcon(new IconDrawable(context, FontAwesomeIcons.fa_life_ring))
			.setCancelable(false);
	}

	public TutorialDialogBuilder setDoNotShowAgainPolicy(DoNotShowAgainPolicy policy) {
		this.doNotShowAgainPolicy = policy;
		return this;
	}
}
