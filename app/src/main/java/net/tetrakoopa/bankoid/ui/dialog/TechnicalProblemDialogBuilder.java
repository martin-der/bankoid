package net.tetrakoopa.bankoid.ui.dialog;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.View;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.SettingsActivity;
import net.tetrakoopa.bankoid.ui.debug.LogMessageListActivity;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;

public class TechnicalProblemDialogBuilder extends CommonDialogBuilder<TechnicalProblemDialogBuilder> {

	private long logMessageId = -1;

	public TechnicalProblemDialogBuilder(@NonNull Context context) {
		super(context);
		defaultInit(context);
	}

	public TechnicalProblemDialogBuilder(@NonNull Context context, int themeResId) {
		super(context, themeResId);
		defaultInit(context);
	}

	public TechnicalProblemDialogBuilder setLogMessage(long id) {
		logMessageId = id;
		return this;
	}

	private void defaultInit(Context context) {
		this
			.setOnDialogCreated(dialog -> {
				final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
				if (preferences.getBoolean(SettingsActivity.Key.ENABLE_DEBUG, false)) {
					if (logMessageId > 0) {
						try {
							final View icon = dialog.findViewById(android.R.id.icon);
							icon.setOnClickListener(view -> {
								dialog.dismiss();
								Intent intent = new Intent(context, LogMessageListActivity.class);
								intent.putExtra(LogMessageListActivity.ARG_ITEM_ID, logMessageId);
								context.startActivity(intent);
							});
						} catch (RuntimeException rex) {
							Log.e("[DialogError]", "Failed to find icon in layout", rex);
						}
					}
				}
			})
			.setIcon(new IconDrawable(context, FontAwesomeIcons.fa_bomb).color(Color.RED))
			.setTitle(R.string.message_technical_error_occurred)
			.setCancelable(true);
	}
}
