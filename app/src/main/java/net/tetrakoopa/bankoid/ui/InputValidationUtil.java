package net.tetrakoopa.bankoid.ui;

import android.content.Context;
import android.text.InputFilter;
import android.widget.EditText;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.model.common.Constraint;
import net.tetrakoopa.bankoid.util.ResourceHelper;

public class InputValidationUtil {

	public static void prepareInputFilters(Constraint[] constraints, EditText editText) {
		for (final Constraint constraint : constraints) {
			if (constraint.type == Constraint.Type.MAX) {
				editText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(constraint.limit) });
				break;
			}
		}
	}

	public static String getValidationMessage(Context context, Constraint[] constraints, String text) {
		String errorMessage = null;
		if (!text.isEmpty()) {
			for (final Constraint constraint : constraints) {
				switch (constraint.type) {
					case MIN:
						if (text.length()<constraint.limit) errorMessage = ResourceHelper.text(context, R.string.message_validation_text_minimum_length, constraint.limit);
						break;
					case MAX:
						if (text.length()>constraint.limit) errorMessage = ResourceHelper.text(context, R.string.message_validation_text_maximum_length, constraint.limit);
						break;
					case REGEX:
						if (!text.matches(constraint.regex)) errorMessage = constraint.description == null
								? ResourceHelper.text(context, R.string.message_validation_text_unmatched_regex, constraint.regex)
								: ResourceHelper.textOrRaw(context, constraint.description);
						break;
				}
			}
		}
		return errorMessage;
	}

}
