package net.tetrakoopa.bankoid.ui.main.account;

import android.webkit.JavascriptInterface;

import net.tetrakoopa.bankoid.backend.util.ClientViewJSExecutor;

public class JSAccountAgent {

	private String account;

	private final ClientViewJSExecutor executor;

	public JSAccountAgent(ClientViewJSExecutor executor) {
		this.executor = executor;
	}

	public void setAccount(String account) { this.account = account; }

	@JavascriptInterface
	public void setAccount() {
		executor.execute("AccountHelper.setAccountIdentifier", account);
	}

	@JavascriptInterface
	public void logout() {
		executor.execute("AccountHelper.logout");
	}
}
