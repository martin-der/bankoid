package net.tetrakoopa.bankoid.custom;

import android.text.TextWatcher;

public abstract class AfterTextChangedTextWatcher implements TextWatcher {
	@Override
	public final void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

	}

	@Override
	public final void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

	}
}
