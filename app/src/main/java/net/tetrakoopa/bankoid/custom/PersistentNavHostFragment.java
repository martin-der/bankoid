package net.tetrakoopa.bankoid.custom;


import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

public class PersistentNavHostFragment extends NavHostFragment {

	@Override
	public void onCreateNavController(NavController navController) {

		super.onCreateNavController(navController);
		navController.getNavigatorProvider().addNavigator(new PersistentFragmentNavigator(getContext(), getChildFragmentManager(), getId()));
	}
}