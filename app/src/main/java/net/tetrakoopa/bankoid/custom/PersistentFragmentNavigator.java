package net.tetrakoopa.bankoid.custom;

import android.content.Context;
import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.Navigator;
import androidx.navigation.fragment.FragmentNavigator;

@Navigator.Name("persistent_fragment")
public class PersistentFragmentNavigator extends FragmentNavigator {

	private final FragmentManager manager;
	private final Context context;
	private final int containerId;

	private final Map<String, Fragment> fragments = new HashMap<>();

	public PersistentFragmentNavigator(@NonNull Context context, @NonNull FragmentManager manager, int containerId) {
		super(context, manager, containerId);
		this.manager = manager;
		this.context = context;
		this.containerId = containerId;
	}


	public Fragment instantiateFragment(@NonNull Context context,
										@NonNull FragmentManager fragmentManager,
										@NonNull String className, @SuppressWarnings("unused") @Nullable Bundle args) {
		if (fragments.containsKey(className)) return fragments.get(className);
		final Fragment fragment = fragmentManager.getFragmentFactory().instantiate(context.getClassLoader(), className);
		if (fragment instanceof PersistableFragment && ((PersistableFragment)fragment).isPersistent() )
			fragments.put(className, fragment);
		return fragment;
	}



//		@Override
//		public NavDestination navigate(@NonNull Destination destination, @Nullable Bundle args,
//									   @Nullable NavOptions navOptions, @Nullable Navigator.Extras navigatorExtras) {
//			if (manager.isStateSaved()) {
//				Log.i("MainActivity", "Ignoring navigate() call: FragmentManager has already"
//						+ " saved its state");
//				return null;
//			}
//			String className = destination.getClassName();
//			if (className.charAt(0) == '.') {
//				className = context.getPackageName() + className;
//			}
//			final Fragment frag = instantiateFragment(context, manager,
//					className, args);
//			frag.setArguments(args);
//			final FragmentTransaction ft = manager.beginTransaction();
//
//			int enterAnim = navOptions != null ? navOptions.getEnterAnim() : -1;
//			int exitAnim = navOptions != null ? navOptions.getExitAnim() : -1;
//			int popEnterAnim = navOptions != null ? navOptions.getPopEnterAnim() : -1;
//			int popExitAnim = navOptions != null ? navOptions.getPopExitAnim() : -1;
//			if (enterAnim != -1 || exitAnim != -1 || popEnterAnim != -1 || popExitAnim != -1) {
//				enterAnim = enterAnim != -1 ? enterAnim : 0;
//				exitAnim = exitAnim != -1 ? exitAnim : 0;
//				popEnterAnim = popEnterAnim != -1 ? popEnterAnim : 0;
//				popExitAnim = popExitAnim != -1 ? popExitAnim : 0;
//				ft.setCustomAnimations(enterAnim, exitAnim, popEnterAnim, popExitAnim);
//			}
//
//			ft.replace(containerId, frag);
//			ft.setPrimaryNavigationFragment(frag);
//
//			final @IdRes int destId = destination.getId();
//			final boolean initialNavigation = mBackStack.isEmpty();
//			// TODO Build first class singleTop behavior for fragments
//			final boolean isSingleTopReplacement = navOptions != null && !initialNavigation
//					&& navOptions.shouldLaunchSingleTop()
//					&& mBackStack.peekLast() == destId;
//
//			boolean isAdded;
//			if (initialNavigation) {
//				isAdded = true;
//			} else if (isSingleTopReplacement) {
//				// Single Top means we only want one instance on the back stack
//				if (mBackStack.size() > 1) {
//					// If the Fragment to be replaced is on the FragmentManager's
//					// back stack, a simple replace() isn't enough so we
//					// remove it from the back stack and put our replacement
//					// on the back stack in its place
//					manager.popBackStack(
//							generateBackStackName(mBackStack.size(), mBackStack.peekLast()),
//							FragmentManager.POP_BACK_STACK_INCLUSIVE);
//					ft.addToBackStack(generateBackStackName(mBackStack.size(), destId));
//				}
//				isAdded = false;
//			} else {
//				ft.addToBackStack(generateBackStackName(mBackStack.size() + 1, destId));
//				isAdded = true;
//			}
//			if (navigatorExtras instanceof Extras) {
//				Extras extras = (Extras) navigatorExtras;
//				for (Map.Entry<View, String> sharedElement : extras.getSharedElements().entrySet()) {
//					ft.addSharedElement(sharedElement.getKey(), sharedElement.getValue());
//				}
//			}
//			ft.setReorderingAllowed(true);
//			ft.commit();
//			// The commit succeeded, update our view of the world
//			if (isAdded) {
//				mBackStack.add(destId);
//				return destination;
//			} else {
//				return null;
//			}
//		}

//		public NavDestination navigateZZZ(@NonNull Destination destination, @Nullable Bundle args,
//									  @Nullable NavOptions navOptions, @Nullable Navigator.Extras navigatorExtras) {
//			final String tag = String.valueOf(destination.getId());
//			final FragmentTransaction transaction = manager.beginTransaction();
//
//			Fragment currentFragment = manager.getPrimaryNavigationFragment();
//			if (currentFragment != null) {
//				transaction.detach(currentFragment);
//			}
//
//			final Fragment fragment = manager.findFragmentByTag(tag);
//			if (fragment == null) {
//				fragment = destination.createFragment(args);
//				transaction.add(containerId, fragment, tag);
//			} else {
//				transaction.attach(fragment);
//			}
//
//			transaction.setPrimaryNavigationFragment(fragment);
//			transaction.setReorderingAllowed(true);
//			transaction.commit();
//
//			return dispatchOnNavigatorNavigated(destination.getId(), BACK_STACK_DESTINATION_ADDED);
//		}
}
