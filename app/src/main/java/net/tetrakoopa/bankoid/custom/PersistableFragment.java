package net.tetrakoopa.bankoid.custom;


public interface PersistableFragment {
	boolean isPersistent();
}
