package net.tetrakoopa.bankoid.model.common;

import net.tetrakoopa.bankoid.util.EnumUtil;

public class Constraint {

	public enum Type {
		REGEX, MIN, MAX;

		/** @see net.tetrakoopa.bankoid.util.EnumUtil.fromName(Class, String) */
		public static Type fromName(String name) throws EnumUtil.InvalidEnumCriteriaException.Name {
			return EnumUtil.fromName(Type.class, name);
		}

		/** @see net.tetrakoopa.bankoid.util.EnumUtil.fromNameIgnoreCase(Class, String) */
		public static Type fromNameIgnoreCase(String name) throws EnumUtil.InvalidEnumCriteriaException.Name {
			return EnumUtil.fromNameIgnoreCase(Type.class, name);
		}

	}

	public final Type type;

	public final int limit;

	public final String regex;

	public final String description;

	public Constraint(String regex, String description) {
		this.type = Type.REGEX;
		this.description = description;
		this.regex = regex;
		this.limit = 0;
	}

	public Constraint(Type type, int limit, String description) {
		if (type != Type.MIN && type != Type.MAX) throw new IllegalArgumentException("Type must be "+Type.MAX+" or "+Type.MIN);
		this.type = type;
		this.description = description;
		this.limit = limit;
		this.regex = null;
	}

}
