package net.tetrakoopa.bankoid.model;

import android.util.Log;

import net.tetrakoopa.bankoid.backend.debug.MessageLogger.LogMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LogMessageContent {

	private static final List<LogMessage> ITEMS = new ArrayList<>();
	private static final List<LogMessage> ITEMS_READONLY = Collections.unmodifiableList(ITEMS);

	private static final Map<Long, LogMessage> ITEM_MAP = new HashMap<>();
	private static final Map<Long, LogMessage> ITEM_MAP_READONLY = Collections.unmodifiableMap(ITEM_MAP);

	private static boolean useExceptionExactLocation = true;

	public static List<LogMessage> getItems() {
		return ITEMS_READONLY;
	}
	public static Map<Long, LogMessage> getItemMap() {
		return ITEM_MAP_READONLY;
	}

	public static long addMessage(LogMessage message) {
		androidLog(message);
		addItem(message);
		return message.id;
	}

	private static void addItem(LogMessage item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.id, item);
	}
	public static void clear() {
		ITEMS.clear();
		ITEM_MAP.clear();
	}

	private static void androidLog(LogMessage message) {
		final String tag = "BANKOID";
		switch (message.severity) {
			case TRACE: case DEBUG: Log.d(tag, message.content, message.cause); return;
			case INFO: Log.i(tag, message.content, message.cause); return;
			case WARNING: Log.w(tag, message.content, message.cause); return;
			case ERROR: case FATAL: Log.e(tag, message.content, message.cause); return;
		}
		Log.e(tag, message.content, message.cause);
	}
}