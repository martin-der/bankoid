package net.tetrakoopa.bankoid.model.bank;

import net.tetrakoopa.bankoid.backend.BackendHandler;
import net.tetrakoopa.bankoid.model.common.Constraint;
import net.tetrakoopa.bankoid.backend.ui.model.AccountWebViewModel;

import java.util.ArrayList;
import java.util.List;

public class Config {

	public static class Clazz<M extends AccountWebViewModel, H extends  BackendHandler<M>> {

		private final Class<M> model;
		private final Class<H> handler;

		public Clazz(Class<H> handler, Class<M> model) {
			this.model = model;
			this.handler = handler;
		}

		public Class<M> getModel() {
			return model;
		}

		public Class<H> getHandler() {
			return handler;
		}
	}

	public static class Account {

		public static class Identifier {

			public final Constraint[] constraints;

			public Identifier(Constraint[] constraints) {
				this.constraints = constraints;
			}

		}

		public final String type;

		public final Identifier identifier;

		public Account(String type, Identifier identifier) {
			this.type = type;
			this.identifier = identifier;
		}


	}

	private final Clazz clazz;

	private final Settings settings;

	private final List<Account> accounts = new ArrayList<>();

	public Config(Clazz clazz, Settings settings) {
		this.clazz = clazz;
		this.settings = settings;
	}

	public Clazz getClazz() {
		return clazz;
	}

	public Settings getSettings() {
		return settings;
	}

	public List<Account> getAccounts() {
		return accounts;
	}
}
