package net.tetrakoopa.bankoid.model.common;

public class AndroidPreferenceI18NText {

	public final String title;
	public final String summary;
	public final String summaryOn;
	public final String summaryOff;

	public AndroidPreferenceI18NText(String title, String summary, String summaryOn, String summaryOff) {
		this.title = title;
		this.summary = summary;
		this.summaryOn = summaryOn;
		this.summaryOff = summaryOff;
	}
}
