package net.tetrakoopa.bankoid.model.bank;

import android.graphics.drawable.Drawable;

import net.tetrakoopa.bankoid.util.xml.common.DrawableParser;

public class About {

	public static class Logo extends DrawableParser.DeserializedDrawable {
		public Logo(DrawableParser.DrawableDescription description) {
			super(description);
		}
	}

	public final String name;

	public final String identifier;

	public final String description;

	public final Logo logo;

	public About(String name, String identifier, String description, Logo logo) {
		this.name = name;
		this.identifier = identifier;
		this.description = description;
		this.logo = logo;
	}

	public Drawable getLogoDrawable() {
		return logo == null ? null : logo.getDrawable();
	}
}
