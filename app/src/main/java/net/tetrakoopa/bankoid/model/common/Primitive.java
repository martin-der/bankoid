package net.tetrakoopa.bankoid.model.common;

import android.annotation.SuppressLint;

import net.tetrakoopa.bankoid.util.EnumUtil;

import java.util.Arrays;

@SuppressLint("NewApi")
public class Primitive {

	public enum Type {

		STRING, BOOLEAN, INTEGER, FLOAT, ENUMERATION;

		/**
		 * @throws IllegalArgumentException if no <code>Type</code> named <code>name</code> exist
		 */
		public static Type fromName(String name) throws EnumUtil.InvalidEnumCriteriaException.Name {
			return EnumUtil.fromName(Type.class, name);
		}
		public static Type fromNameIgnoreCase(String name) throws EnumUtil.InvalidEnumCriteriaException.Name {
			return EnumUtil.fromNameIgnoreCase(Type.class, name);
		}
	}

	public final Type type;

	public final String[] enumerationValues;

	/**
	 * Constructor for all primitives but ENUMERATION
	 */
	public Primitive(Type type) {
		if (type == Type.ENUMERATION) throw new IllegalArgumentException(Type.ENUMERATION+" type requires a list a enumeration names");
		this.type = type;
		this.enumerationValues = null;
	}

	/**
	 * Constructor for ENUMERATION
	 */
	public Primitive(String[] enumerationValues) {
		this.type = Type.ENUMERATION;
		this.enumerationValues = Arrays.copyOf(enumerationValues, enumerationValues.length);
	}
	/**
	 * Constructor for ENUMERATION
	 */
	public Primitive(Class<? extends Enum> enumerationClass) {
		this(Arrays.stream(enumerationClass.getEnumConstants()).map(e -> e.name()).toArray(String[]::new));
	}

	public Integer ordinal(String string) {
		if (type != Type.ENUMERATION) throw new IllegalStateException("primitive must be of type ENUMERATION");
		if (string == null) return null;
		int ordinal = 0;
		for (String value : enumerationValues) {
			if (value.equals(string)) return ordinal;
			ordinal++;
		}
		throw new IllegalArgumentException("No such Enumeration with name = '"+string+"'");
	}


}
