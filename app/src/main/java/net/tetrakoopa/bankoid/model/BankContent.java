package net.tetrakoopa.bankoid.model;

import net.tetrakoopa.bankoid.model.bank.About;

public class BankContent {

	public static class Bank {

		public final String identifier;

		public final About about;

		public Bank(String identifier, About about) {
			this.identifier = identifier;
			this.about = about;
		}
	}
}
