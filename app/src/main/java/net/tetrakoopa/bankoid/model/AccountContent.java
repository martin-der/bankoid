package net.tetrakoopa.bankoid.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AccountContent {

	private static final List<Account> ITEMS = new ArrayList<>();
	private static final List<Account> ITEMS_READONLY = Collections.unmodifiableList(ITEMS);

	public static List<Account> getItems() {
		return ITEMS_READONLY;
	}

	public static void clear() { ITEMS.clear(); }

	public static void add(Account account) {
		ITEMS.add(account);
	}

	public static Account remove(int position) {
		return ITEMS.remove(position);
	}

	public static class Account {

		public long id;
		public final String name;
		public final String bank;
		public final String type;
		public final String identifier;

		public Account(long id, String name, String bank, String type, String identifier) {
			this.id = id;
			this.name = name;
			this.bank = bank;
			this.type = type;
			this.identifier = identifier;
		}

		@Override
		public String toString() {
			return "["+bank+"] "+name+"#"+id+":"+identifier;
		}
	}
}