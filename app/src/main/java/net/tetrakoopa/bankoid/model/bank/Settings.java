package net.tetrakoopa.bankoid.model.bank;

import net.tetrakoopa.bankoid.model.common.AndroidPreferenceI18NText;
import net.tetrakoopa.bankoid.model.common.Constraint;
import net.tetrakoopa.bankoid.model.common.Primitive;

import java.util.LinkedHashMap;
import java.util.Map;

public class Settings {

	public static class Setting {

		private final static Constraint[] NO_CONSTRAINT = new Constraint[0];
		private final static String[] NO_GROUP = new String[0];

		public final static String VALID_NAME_REGEX = "[a-zA-Z]+(-+[a-zA-Z01-9]+)*";

		public final String dependency;
		
		public final Primitive primitive;

		public final Constraint[] constraints;

		public final Object defaultz;

		public final String[] groups;

		public final AndroidPreferenceI18NText text;

		public Setting(Primitive primitive, Constraint[] constraints, String[] groups, String dependency, Object defaultz, AndroidPreferenceI18NText text) {
			if (primitive == null || defaultz == null) throw new IllegalArgumentException("type and default value must be provided");
			this.constraints = constraints == null ? NO_CONSTRAINT : constraints;
			this.groups = groups == null ? NO_GROUP : groups;
			this.primitive = primitive;
			this.dependency = dependency;
			this.defaultz = defaultz;
			this.text = text;
		}
	}

	private final Map<String, Setting> entries = new LinkedHashMap<>();

	public Map<String, Setting> getEntries() {
		return entries;
	}
}
