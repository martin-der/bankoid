package net.tetrakoopa.bankoid;

import android.content.Context;
import android.content.SharedPreferences;

import net.tetrakoopa.bankoid.R;
import net.tetrakoopa.bankoid.ui.ApplicationTheme;
import net.tetrakoopa.bankoid.ui.dialog.DialogBuilder;
import net.tetrakoopa.bankoid.backend.BackendHandler;
import net.tetrakoopa.bankoid.debug.LogCapable;
import net.tetrakoopa.bankoid.exception.BadBackendException;
import net.tetrakoopa.bankoid.model.AccountContent;
import net.tetrakoopa.bankoid.model.bank.About;
import net.tetrakoopa.bankoid.model.bank.Config;
import net.tetrakoopa.bankoid.model.bank.Settings;
import net.tetrakoopa.bankoid.backend.ui.model.AccountWebViewModel;
import net.tetrakoopa.bankoid.util.xml.AboutParser;
import net.tetrakoopa.bankoid.util.xml.fwk.exception.base.ParseException;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.preference.PreferenceManager;

import static net.tetrakoopa.bankoid.util.JavaLangUtil.parametersTypeAsString;

public class ApplicationData implements LogCapable {

	public interface Aware {
		ApplicationData applicationData = ApplicationData.instance();
	}

	private final static String VALID_BACKEND_FOLDER_NAME_REGEX = "[a-zA-Z]+(-+[a-zA-Z01-9]+)*";

	private final MutableLiveData<ApplicationTheme> theme = new MutableLiveData<>();
	public static final ApplicationTheme DEFAULT_APPLICATION_THEME = ApplicationTheme.LIGHT;
	public void updateTheme(ApplicationTheme theme) {
		this.theme.postValue(theme);
	}


	public LiveData<ApplicationTheme> theme() {
		return theme;
	}

	private static ApplicationData _instance = new ApplicationData();
	public static ApplicationData instance() { return _instance; }

	public interface OnAccountChangeListener {
		void onAccountChange(AccountContent.Account account);
	}
	private final Set<OnAccountChangeListener> accountChangeListeners = new HashSet<>();

	public void registerOnAccountChangeListener(OnAccountChangeListener listener) { accountChangeListeners.add(listener); }
	public void unregisterOnAccountChangeListener(OnAccountChangeListener listener) { accountChangeListeners.remove(listener); }

	private Backend<? extends AccountWebViewModel, ? extends BackendHandler<?>> _currentBackend;

	public Backend<? extends AccountWebViewModel, ? extends BackendHandler<?>> currentBackend() {
		return _currentBackend;
	}

	private Settings referenceSettings;
	public Settings referenceSettings() { return referenceSettings; };

	private AccountContent.Account _currentAccount;

	public AccountContent.Account currentAccount() {
		return _currentAccount;
	}

	public List<AccountContent.Account> accounts() {
		return Collections.unmodifiableList(AccountContent.getItems());
	}

	private final List<String> backendNames = new ArrayList<>();

	private final Map<String, About> backendInfos = new HashMap<>();

	public Map<String, About> getBackendInfos() {
		return backendInfos;
	}

	private final TutorialConfig tutorialConfig = new TutorialConfig();
	public TutorialConfig tutorialConfig() {
		return tutorialConfig;
	}

	private List<InitializationUIMessage> initializationUIMessages = new ArrayList<>();
	public List<InitializationUIMessage> initializationUIMessages() {
		return Collections.unmodifiableList(initializationUIMessages);
	}
	private final InitializationUIMessage.Pusher initializationUIMessagesPusher = new InitializationUIMessage.Pusher(initializationUIMessages);
	public void clearInitializationUIMessages() { initializationUIMessages.clear(); }

	public interface InitializationUIMessage {

		class Pusher {

			private final List<InitializationUIMessage> messages;

			private Pusher(List<InitializationUIMessage> messages) {
				this.messages = messages;
			}

			public void push(InitializationUIMessage message) {
				messages.add(message);
			}
		}

		void show(Context context);
	}

	public static class Backend<M extends AccountWebViewModel, H extends BackendHandler<M>> {

		public final String name;

		public final M model;
		public final H handler;

		public final Settings settingsTemplate;

		public Backend(String name, M model, H handler, Settings settingsTemplate) {
			this.name = name;
			this.model = model;
			this.handler = handler;
			this.settingsTemplate = settingsTemplate;
		}
	}

	public void init(Context context) {

		final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

		ApplicationDataHelper.doFistInitialization(sharedPreferences);

		theme.setValue(ApplicationTheme.fromName(sharedPreferences.getString(SettingsActivity.Key.APPLICATION_MAIN_THEME, DEFAULT_APPLICATION_THEME.name()), DEFAULT_APPLICATION_THEME));

		final Pattern folderNameValidation = Pattern.compile(VALID_BACKEND_FOLDER_NAME_REGEX);

		final String[] folders;
		try {
			folders = context.getAssets().list("backend");
		} catch (IOException ex) {
			logger.log(MessageLogger.LogMessage.Severity.FATAL, ApplicationData.class, "Failed to list backends from asset folder", ex);
			return;
		}

		final AboutParser aboutParser = new AboutParser();
		for (String folder: folders) {
			try {
				if (!folderNameValidation.matcher(folder).matches()) {
					logger.log(MessageLogger.LogMessage.Severity.ERROR, ApplicationData.class, "Backend folder name '"+folder+"' does not match '"+VALID_BACKEND_FOLDER_NAME_REGEX+"'");
					continue;
				}
				final About about = ApplicationDataHelper.loadAbout(folder, context, aboutParser);
				backendInfos.put(folder, about);
			} catch (ParseException|IOException ex) {
				logger.log(MessageLogger.LogMessage.Severity.ERROR, ApplicationData.class, "Failed to get '"+folder+"' backend about info", ex);
				continue;
			}
		}

		if (backendInfos.size()==0) {
			logger.log(MessageLogger.LogMessage.Severity.ERROR, ApplicationData.class, "No backend available");
		} else {
			backendNames.addAll(backendInfos.keySet());
		}

		try {
			referenceSettings = ApplicationDataHelper.loadReferenceSettings(context);
		} catch (IOException|ParseException ex) {
			logger.log(MessageLogger.LogMessage.Severity.FATAL, ApplicationData.class, "Failed to read reference settings", ex);
		}

		ApplicationDataHelper.loadStartingAccount(context, sharedPreferences, initializationUIMessagesPusher);
	}

	public void setCurrentAccount(Context context, AccountContent.Account account) throws BadBackendException {
		if (account != null)
			setCurrentBackend(context, account.bank);
		else
			_currentBackend = null;
		_currentAccount = account;
		for (OnAccountChangeListener listener : accountChangeListeners) {
			listener.onAccountChange(account);
		}
	}
	private void setCurrentBackend(Context context, String name) throws BadBackendException {
		_currentBackend = loadBackendAndWireIt(context, name);
	}

	private <M extends AccountWebViewModel, H extends BackendHandler<M>> Backend<M, H> loadBackendAndWireIt(Context context, String name) throws BadBackendException {
		final Backend<M, H> backend = loadBackend(context, name);
		backend.handler.setLogger(logger);
		backend.handler.setLoginFailureDialogProvider((c) -> DialogBuilder.notification(c)
				.setTitle(R.string.message_logging_in_failed_title)
				.setMessage(R.string.message_logging_in_failed_explanation)
				.setNeutralButton(android.R.string.ok, null)
				.show());
		return backend;
	}

	private <M extends AccountWebViewModel, H extends BackendHandler<M>> Backend<M, H> loadBackend(Context context, String name) throws BadBackendException {
		final Config.Clazz<M, H> clazz;
		final Settings settings;
		try {
			final Config config = ApplicationDataHelper.loadConfig(name, context);
			clazz = config.getClazz();
			settings = config.getSettings();
		} catch (ParseException e) {
			throw new BadBackendException("Failed to read config file: "+e.getMessage(), e);
		} catch (IOException e) {
			throw new BadBackendException("Failed to read config: "+e.getMessage(), e);
		}

		final M model;
		try {
			model = clazz.getModel().newInstance();
		} catch (IllegalAccessException e) {
			throw new RuntimeException("Failed to create backend model: "+e.getMessage(), e);
		} catch (InstantiationException e) {
			throw new BadBackendException("Failed to create backend model (Does the model have a public constructor?): "+e.getMessage(), e);
		}

		final Class<?>[] constructorParametersType = new Class<?>[] { String.class, model.getClass() };

		final H handler;
		try {
			final Constructor<H> constructor = clazz.getHandler().getConstructor(String.class, model.getClass());
			handler = constructor.newInstance(name, model);
		} catch (IllegalAccessException e) {
			throw new RuntimeException("Failed to create backend handler"+e.getMessage(), e);
		} catch (InstantiationException e) {
			throw new RuntimeException("Failed to create backend handler: "+e.getMessage(), e);
		} catch (NoSuchMethodException e) {
			throw new BadBackendException("Failed to create backend handler (Does the model have a public constructor with "+parametersTypeAsString(constructorParametersType)+"?): +e.getMessage()", e);
		} catch (InvocationTargetException e) {
			throw new BadBackendException("Failed to create backend handler: Constructor throw an exception: "+e.getMessage(), e);
		}
		return new Backend<>(name, model, handler, settings);
	}

}
