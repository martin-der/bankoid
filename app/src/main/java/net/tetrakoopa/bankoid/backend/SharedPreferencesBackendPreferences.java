package net.tetrakoopa.bankoid.backend;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import net.tetrakoopa.bankoid.SettingsActivity;
import net.tetrakoopa.bankoid.debug.LogCapable;
import net.tetrakoopa.bankoid.model.bank.Settings;
import net.tetrakoopa.bankoid.model.common.Primitive;
import net.tetrakoopa.bankoid.util.DeclaredValue;
import net.tetrakoopa.bankoid.util.JavaLangUtil;
import net.tetrakoopa.bankoid.util.xml.common.PrimitiveParser;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;

import org.springframework.util.ReflectionUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@SuppressLint("NewApi")
public class SharedPreferencesBackendPreferences implements BackendPreferences, LogCapable {

	private final SharedPreferences preferences;
	
	private final Map<String, Settings.Setting> settings;

	private final String backend;

	private final String backendPrefixKey;

	private final Set<OnPreferenceChangeListener> listeners = new HashSet<>();

	private static class Group {
		final String key;
		final Object defaultz;

		private Group(String key, Object defaultz) {
			this.key = key;
			this.defaultz = defaultz;
		}
	}

	private static final Map<String, Group> groupsDefaults = new HashMap<>();

	private final MessageLogger.LogMessage.Location backendLogMessageLocation;

	static {
		ReflectionUtils.doWithLocalFields(SettingsActivity.Group.class, groupField -> {
			final DeclaredValue.Key keyAnnotation = groupField.getAnnotation(DeclaredValue.Key.class);
			if (keyAnnotation != null) {
				ReflectionUtils.doWithLocalFields(SettingsActivity.Key.class, keyField -> {
					if (keyAnnotation.value().equals(keyField.get(null))) {
						final boolean value;
						final DeclaredValue.Default defaultz = keyField.getAnnotation(DeclaredValue.Default.class);
						// group type should always be BOOLEAN anyway
						// but let's be careful
						if (defaultz.type() == Primitive.Type.BOOLEAN) {
							if (defaultz != null) {
								try {
									value = DeclaredValue.Helper.getValue(defaultz);
								} catch (PrimitiveParser.InvalidPrimitiveValueException ex) {
									throw new InternalError(ex);
								}
								groupsDefaults.put((String)groupField.get(null), new Group(keyAnnotation.value(), value));
							}
						}
					}
				});
			}
		});
	}


	public SharedPreferencesBackendPreferences(String backend, SharedPreferences preferences, Settings settings) {
		this.backend = backend;
		this.backendPrefixKey = PreferencesKey.getForBackend(backend, "");
		this.backendLogMessageLocation = new MessageLogger.LogMessage.Location("Backend '"+backend+"'");
		this.preferences = preferences;
		this.settings = settings.getEntries();
	}

	@Override
	public void registerOnPreferenceChangeListener(OnPreferenceChangeListener listener) {
		listeners.add(listener);
	}

	@Override
	public void unregisterOnPreferenceChangeListener(OnPreferenceChangeListener listener) {
		listeners.remove(listener);
	}

	public void notifyPreferenceChange(final String key) {
		if (listeners.isEmpty()) return;

		if (key == SettingsActivity.Key.ENABLE_WEB_UI_TWEAKS) {
			final List<Map.Entry<String, Settings.Setting>> uiSettings = settings.entrySet().stream().filter(e -> Arrays.stream(e.getValue().groups).anyMatch(SettingsActivity.Group.UI::equalsIgnoreCase)).collect(Collectors.toList());
			if (!uiSettings.isEmpty()) {
				final String[] keys = uiSettings.stream().map(Map.Entry::getKey).toArray(String[]::new);
				listeners.forEach(listener -> listener.onPreferenceChanged(this, keys));
			}
			return ;
		}

		if (!key.startsWith(backendPrefixKey)) return;

		final String unPrefixedKey = key.substring(backendPrefixKey.length());

		if (!settings.containsKey(unPrefixedKey)) return;

		listeners.forEach(listener -> listener.onPreferenceChanged(this, unPrefixedKey));
	}

	/**
	 * @return <code>null</code> if the preference is not allowed in backend scope
	 */
	private Settings.Setting getSetting(String key) {
		final Settings.Setting setting = settings.get(key);
		if (setting == null) {
			logger.log(MessageLogger.LogMessage.Severity.WARNING, MessageLogger.LogMessage.Type.ACTIVITY, backendLogMessageLocation, "Backend forbidden preference request", "Backend '"+backend+"' requested a key not allowed '"+key+"'");
			return null;
		}
		return setting;
	}
	private String getCuratedKey(String key) {
		return backendPrefixKey+key;
	}

	private boolean allowedByGroups(Settings.Setting setting) {
		boolean allowedByGroups = true;
		for (final String groupName : setting.groups) {
			if (groupsDefaults.containsKey(groupName)) {
				final Group group = groupsDefaults.get(groupName);
				if (! preferences.getBoolean(group.key, (boolean)group.defaultz)) {
					allowedByGroups = false;
					break;
				}
			}
		}
		return allowedByGroups;
	}

	private interface GenericPreferenceGetter<T> {
		T get(String key, T defaultValue);
	}

	private <T> T getValue(Class<T> type, String key, T overAllDefaultValue, GenericPreferenceGetter<T> getter) {
		final Settings.Setting setting = getSetting(key);
		if (setting == null) return overAllDefaultValue;

		final Object rawDefaultValue = setting.defaultz;
		final Class<?> typeToBoxed = type.isPrimitive() ? JavaLangUtil.boxedClass(type) : type;
		if (! typeToBoxed.isAssignableFrom(rawDefaultValue.getClass())) {
			logger.log(MessageLogger.LogMessage.Severity.WARNING, MessageLogger.LogMessage.Type.ACTIVITY, backendLogMessageLocation, "Backend bad preference request", "backend '"+backend+"' requested property '"+ key +" as "+type+" whereas default value is of type "+ setting.primitive.type.name());
			return overAllDefaultValue;
		}
		final T defaultValue = (T) rawDefaultValue;

		if (!allowedByGroups(setting)) return defaultValue;

		final String curatedKey = getCuratedKey(key);
		return getter.get(curatedKey, defaultValue);
	}

	@Override
	public boolean getBoolean(String key) {
		return getValue(Boolean.TYPE, key, false, preferences::getBoolean);
	}

	@Override
	public float getFloat(String key) {
		return getValue(Float.TYPE, key, 0f, preferences::getFloat);
	}

	@Override
	public int getInt(String key) {
		return getValue(Integer.TYPE, key, 0, preferences::getInt);
	}

	@Override
	public long getLong(String key) {
		return getValue(Long.TYPE, key, 0l, preferences::getLong);
	}

	@Override
	public String getString(String key) {
		return getValue(String.class, key, null, preferences::getString);
	}

	@Override
	public Set<String> getStringSet(String key) {
		return getValue(Set.class, key, null, preferences::getStringSet);
	}

}
