package net.tetrakoopa.bankoid.backend;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.widget.Toast;

public class WebBackendProviderConnection implements ServiceConnection {

	public interface ConnectionStatusListener {
		void onConnected(ComponentName name, IWebBackend backend);
		void onDisconnected(ComponentName name);
	}

	private final Context context;

	private IWebBackend webBackendService;

	private ComponentName name;

	private final ConnectionStatusListener listener;

	public WebBackendProviderConnection(Context context, ConnectionStatusListener listener) {
		this.context = context;
		this.listener = listener;
	}

	@Override
	public final void onServiceConnected(ComponentName name, IBinder bound) {
		this.webBackendService = IWebBackend.Stub.asInterface(bound);
		this.name = name;
		listener.onConnected(name, webBackendService);
	}

	@Override
	public final void onServiceDisconnected(ComponentName name) {
		webBackendService = null;
		listener.onDisconnected(name);
	}

	public static class Helper<C extends ServiceConnection> {

		private final Context context;
		private C connection;

		public Helper(Context context) {
			this.context = context;
		}

		protected C instantiateConnection(ConnectionStatusListener listener) {
			return (C) new WebBackendProviderConnection(context, listener);
		}

		protected String serviceName() {
//			return "net.tetrakoopa.bankoid.backend.WebBackendProviderService";
			return "net.tetrakoopa.bankoid.backend.provider.webbackendreference.WebBackendReferenceService";
		}

		public C initService(ConnectionStatusListener listener) {

			connection = instantiateConnection(listener);
			final Intent intent = new Intent();
			intent.setClassName(context, serviceName());
			if(!context.bindService(intent, connection, Context.BIND_AUTO_CREATE)) {
				Toast.makeText(context, "Bind Service Failed!", Toast.LENGTH_SHORT).show();
			}
			return connection;
		}

		public void releaseService() {
			context.unbindService(connection);
			connection = null;
		}

	}
}

