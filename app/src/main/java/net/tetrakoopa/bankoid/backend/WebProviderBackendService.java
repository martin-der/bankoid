package net.tetrakoopa.bankoid.backend;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class WebProviderBackendService extends Service  {

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		final IWebBackend webBackend = IWebBackend.Stub.asInterface(null);
		return webBackend.asBinder();
	}
}
