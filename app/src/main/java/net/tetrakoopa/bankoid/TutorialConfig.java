package net.tetrakoopa.bankoid;

import net.tetrakoopa.bankoid.model.common.Primitive;
import net.tetrakoopa.bankoid.util.DeclaredValue;

public class TutorialConfig implements DeclaredValue {

	@Key(Help.Key.ABOUT_CREATE_ACCOUNT_SHOWN)
	@Default(value="false", type = Primitive.Type.BOOLEAN)
	public boolean aboutCreateAccountShown;
}
