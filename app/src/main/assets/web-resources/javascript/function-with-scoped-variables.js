var C = function(ctx, funcBody){
        var newBody = [];

        for(var k in ctx){
            var i =  "var "+k + " = ctx['"+k+"'];";
            newBody.push(i);
        }
        var res = "return function(t){ " +funcBody+ " }";
        newBody.push(res);
        var F = new Function("ctx", newBody.join('\n'));
        return F(ctx);
}
var newFunction = C({"foo":10, "bar":100}, "return foo+bar*t")

Function.prototype.applyVars = function(scope, scope_variables, params) {
  if (scope_variables) {
    var variable, defVars = [];
    for (variable in scope_variables) {
      if (scope_variables.hasOwnProperty(variable)) {
        defVars.push(variable + '=scope_variables["' + variable + '"]');
      }
    }
    eval('var ' + defVars.join(',') + ';');
    return eval('(' + this + ').apply(scope, params);');
  }
  return this.apply(scope, params);
}
