
var Net_TetraKoopa_AndroidOverWeb_Tool = {

	ImageEditor : {

		editImage : function (image, editionOperator) {

			var canvas = document.createElement('canvas');

			canvas.width = image.width; canvas.height = image.height;
			var context = canvas.getContext("2d");
			context.drawImage(image,0,0,canvas.width,canvas.height);
			var idt = context.getImageData(0,0,canvas.width,canvas.height);

			editionOperator(canvas);

			context.putImageData(idt, 0,0);  // 0,0 is xy coordinates
			img.src = canvas.toDataURL();
		}
	}

};

// usage
/*
Net_TetraKoopa_AndroidOverWeb_Tool.editImage(document.query('img#this-image', function (canvas) {
	getPixel(idt, 852);  // returns array [red, green, blue, alpha]
	getPixelXY(idt, 1,1); // same pixel using x,y
	
	setPixelXY(idt, 1,1, 0,0,0,255); // a black pixel
}))
*/


