
var Net_TetraKoopa_AndroidOverWeb_Common = {

	_doInternalSetup : function () {
//		try {
			AccountHelper.Selector = Net_TetraKoopa_AndroidOverWeb_Common.Selector;
//		} catch (error) {
//			throw "Failed to perfom internal setup : "+error
//		}
	},

	Selector : {
		get : function(selector) {
			return document.querySelector(selector);
		},
		getOrFail : function(selector, title) {
			var element = Net_TetraKoopa_AndroidOverWeb_Common.Selector.get(selector);
			if (element == null) {
				if (title != null)
					throw "No such element \""+title+"\" for selector \""+selector+"\"";
				else
					throw "No such element for selector \""+selector+"\"";
			}
			return element;
		}
	}
};
