## Safe

* put js methods/variables into namespace
* settings
  * open `Starting Account` when `Start account mode` is set to `Pick` => revert to previous `Starting Account` option if cancelled  

## Improvement

* detect when/if network is available

* filter message log

## Backend development

* Add variables to function context ( c.f. `function-with-scoped-variables.js` )

* Change backend as AAR to backend as plugin ( Service ? )

## UX

* **\[In Progress]** add a preference to deactivate all web tweaks with one checkbox

* theme
  * change on the fly ( or force application to restart )
  * some parts are no themed
