package net.tetrakoopa.bankoid.backend.provider.labanquepostale;

import net.tetrakoopa.bankoid.backend.BackendHandler;
import net.tetrakoopa.bankoid.backend.ResourceProvider;
import net.tetrakoopa.bankoid.backend.debug.MessageLogger;

import java.net.MalformedURLException;
import java.net.URL;

public class LaBanquePostaleBackendHandler extends BackendHandler<LaBanquePostaleAccountWebViewModel> implements ResourceProvider {

	private final static String bankAccountProtocolAndHost = "https://m.labanquepostale.fr";
	private final static String bankAccountLoginUrlStart = bankAccountProtocolAndHost + "/wsost/OstBrokerWeb/pagehandler";
	private final static String bankAccountLoginUrl = bankAccountLoginUrlStart + "?TAM_OP=login&ERROR_CODE=0x00000000&URL=%2Fws_qh5%2Fbad%2Fmobile%2Fsite.mobi%2Fsmartphone%2Findex.html%3Forigin%3Dmobipph%26codeMedia%3D9228";
	private final static String bankAccountManagerUrl = bankAccountProtocolAndHost + "/ws_qh5/bad/mobile/site.mobi/smartphone/index.jsp?origin=mobipph&codeMedia=9228";
	private final static String bankAccountManagerUrl2 = bankAccountProtocolAndHost + "/ws_qh5/bad/mobile/site.mobi/smartphone/index.jsp?origin=mobipph&codeMedia=9228#/dashboard";
	private final static String invalidLoginUrl = "https://transverse.labanquepostale.fr/xo_/messages/message.html?param=0x132120c8&v=2&origin=mobipph";

	private final URL entry;

	public LaBanquePostaleBackendHandler(String resourcesFolderName, LaBanquePostaleAccountWebViewModel accountModel) {
		super(resourcesFolderName, accountModel);
		try {
			entry = new URL(bankAccountLoginUrl);
		} catch (MalformedURLException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public URL getEntry() {
		return entry;
	}

	@Override
	public boolean onURLChanged(String string, URL url) {

		if (string.startsWith(bankAccountLoginUrlStart)) {
			accountModel.getLogged().postValue(false);
			injectMainJS();

			getLogger().log(MessageLogger.LogMessage.Severity.INFO, LaBanquePostaleBackendHandler.class, "Main page loaded");

			if (preferences.getBoolean(preferencesKey.cleanUI.HIDE_SAVE_ACCOUNT_IDENTIFIER))
				executeJSFunctionForThis("AccountHelper", "AccountHelper.CleanUI.hideSaveAccountIdentifierCheckbox");
			executeJSFunction("accountAgent.setAccount");

			return true;
		}

		if (string.startsWith("https://transverse.labanquepostale.fr/xo_/messages/message.html?param=0x132120c8")) {
			showLoginFailure();
			navigateTo(bankAccountLoginUrl);
			return true;
		}

		if (url.getRef() != null && url.getRef().equals("/dashboard")) {
			accountModel.getLogged().postValue(true);
		} else {
			accountModel.getLogged().postValue(false);
		}

		injectMainJSIfNeeded();

		return true;
	}

	@Override
	public void onUserAction(UserAction action) {
		executeJSFunction("accountAgent.logout");
	}

}
