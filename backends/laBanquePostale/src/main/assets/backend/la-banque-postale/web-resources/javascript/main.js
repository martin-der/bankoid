
var AccountHelper = {

	UIElement : {
		accountIdentifierInput : "input[id='val_cel_identifiant']",
		saveAccountIdentifier : {
			checkBox : "input[id='saveId']",
			label: "label[for='saveId']"
		},
		logoutButton : "button[data-ad-on-click='scope.openPopinDeconnexion'][aria-label='Déconnexion']",
		logoutEffectiveButton : "button[data-ad-on-click='scope.openPopinDeconnexion'][aria-label='me déconnecter']"
	},

	CleanUI : {

		hideSaveAccountIdentifierCheckbox : function() {
			var saveNumberCheckbox = this.Selector.get(this.UIElement.saveAccountIdentifier.checkBox);
			if (saveNumberCheckbox != null) {
				saveNumberCheckbox.style.display = 'none';
				var saveIdentifierLabel = this.Selector.get(this.UIElement.saveAccountIdentifier.label);
				if (saveIdentifierLabel != null) saveIdentifierLabel.style.display = 'none';
			}
		},

		hideLogoutButton : function() {
			var logoutButton = this.Selector.get(this.UIElement.logoutButton);
			if (logoutButton != null) {
				saveNumberCheckbox.style.visibility = 'hidden';
			}
		}

	},

	setAccountIdentifier : function(value) {
		var accountInput = this.Selector.getOrFail(this.UIElement.accountIdentifierInput, "Account Number");
		accountInput.value = value;
	},
	login : function() {

	},
	logout : function() {
		var logoutEffectiveButton = this.Selector.getOrFail(this.UIElement.logoutEffectiveButton, "Logout Button");
		logoutEffectiveButton.click();
	}
};