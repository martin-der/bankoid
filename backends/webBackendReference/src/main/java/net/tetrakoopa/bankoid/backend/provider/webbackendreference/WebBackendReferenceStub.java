package net.tetrakoopa.bankoid.backend.provider.webbackendreference;

import android.os.RemoteException;

import net.tetrakoopa.bankoid.backend.IWebBackend;
import net.tetrakoopa.bankoid.backend.Info;

public class WebBackendReferenceStub extends IWebBackend.Stub {

	@Override
	public String name() throws RemoteException {
		return "Web-Backend-Reference";
	}

	@Override
	public Info info() throws RemoteException {
		return null;
	}
}
