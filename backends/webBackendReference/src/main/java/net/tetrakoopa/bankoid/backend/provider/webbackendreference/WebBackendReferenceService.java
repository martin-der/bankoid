package net.tetrakoopa.bankoid.backend.provider.webbackendreference;

import net.tetrakoopa.bankoid.backend.BackendService;

public class WebBackendReferenceService extends BackendService {

	protected WebBackendReferenceService() {
		super(new WebBackendReferenceStub());
	}
}
