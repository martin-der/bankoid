Bankoid
=======

An android application which is a simple wrapper around a web bank account manager.

Please be aware **this application is in an alpha state**.
Most important features are implemented but few things are [still buggy](./TODO.md) : LIGHT/DARK theme, account reopening when starting.

## Features

* Multi-account ( ...even though only one type of account is available )

## Development

Adding a backend provider[^backend_provider] for another type of account is [~~fairly easy~~ fairly undocumented](./backends/webBackendReference/README.md).

## Future
The possibility of adding backend providers[^backend_provider] via plugins would be useful.

A plugin way of adding new backend provider[^backend_provider] would be useful.

[^backend_provider]: How to describe access a web account manager.
  It provides a start URL, and reacts to some triggers to guess the state of the page ( login done, login error )